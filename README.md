<div style="text-align: center;"> 
    <h1> Wally </h1>
<h2>A Virtual Payment Platform</h2>
</div>

# Description

Wally is an application designed to simulate the workings of a virtual payment platform whereby a user can create a set
of virtual debit cards, wallets and transfer money from one account to another. The application comes with a set of
extra features such as an overdraft, savings accounts, currency exchange with live update of the current exchange rates,
email verification, payment categories and more. Wally has both MVC and REST controllers. For the purposes of simplicity
we will refer to 'wallet' and 'account' as one and the same.

# Table of contents

1. [Introduction](#introduction)
2. [Technologies](#technologies)
3. [Features](#features)
   1. [Core](#core)
   2. [Overdraft](#overdraft)
   3. [Savings](#savings)
   4. [Currencies](#currencies)
   5. [Email Verification](#email-verification)
   6. [User Photos](#user-photos)
   7. [Payment Categories](#payment-categories)
4. [Code Snippets](#code-snippets)
   1. [Mock Bank Api](#mock-bank-api)
   2. [Automated Currency Update](#automated-currency-update)
5. [Installation Guide](#installation-guide)
6. [User Manual](#user-manual)
    1. [REST](#rest)
       1. [Swagger](#swagger)
       2. [Postman](#postman)
    2. [MVC](#mvc)
7. [Tests](#tests)
8. [Limitations](#limitations)
9. [Authors and acknowledgment](#authors-and-acknowledgment)
10. [Project status](#project-status)

## Introduction

The project is serving the purpose of the authors' skills testament after the completion
of the Telerik Alpha Academy course. The full project description document is added to the git folder
for reference purposes. Any deviations not listed in the limitations section have been 
discussed with the client/lecturers. Wally was designed within 3 weeks at the end of 
which it received the <b>Best Project</b> award by <b>Experian PLC</b> and <b>Cognyte Software Ltd.</b>
Afterwards, there have been some complementary works, which have improved the 
functionality and appearance, all of which can be traced through the git commit history.   

## Technologies

Wally is built in <b>Java</b> using the <b>Spring Boot</b> framework and the <b>Gradle Build Tool</b>.
Underneath lies a <b>MariaDB, SQL</b> database which is being accessed through <b>Hibernate</b>. 
The front end is based on proprietary template and the content is being displayed through 
<b>thymeleaf</b>. The fine-tuning and the special transitions are made with <b>CSS, HTML</b> and 
<b> JavaScript</b>. The tests are all done using <b> Mockito</b> and <b>JUnit5</b>. Furthermore,
the REST controllers can be viewed through <b>Swagger</b> and there is an attached JSON 
file for a quick <b>Postman</b> import. Please refer to the table of contents for 
the instruction set.
## Features
The payment platform has a set of necessary 'core' features and optional ones.
Please refer to the original documentation for more details on the original criteria.

Due to the fact that this particular mechanic was left out of the description, it was assumed 
that the bank has an account and a wallet with admin privileges. The main reason for that
is the necessity to fund overdrafts, deposit savings and payback user deposits with 
accrued interest.

<b>NB!</b> All update pages come pre-filled. Due to the length of the readMe, the photos are not attached.

![](project-photos/wally-landing-page.png)

### Core
<b>Visitors</b><br>
All users can see the general features listed on our landing page. However, in order to use them, 
the users have to register themselves first by providing a username, password, email and a phone 
number. 

Then they have to go to their e-mail and click on the 'verify' link to activate their account.

<b>Users/Customers</b><br>
Once activated, the user can log in. After that customers ought to register a debit/credit card.

![](project-photos/my-wallets.png)

When the card is created, then users can open their personal wallets. There they can top up
their accounts and send money to other wallets. Moreover, a mock bank api has been created that 
approves, denies and sends pending statuses all of which are processed according to the
condition given by the bank. In the back end, Wally has a scheduled service which at every minute
picks up all transactions that are pending and re-sends the status confirmation request to the bank.

A bonus feature of the core functionality is that users can also view their total, per currency 
transaction summary of their past month's activity as well as the overall value of each wallet.
At the bottom of the page they can see their paginated transactions for the past month.

<b>Admins</b><br>
Admins can view all transactions and users and can sort, search and filter through them.
All the aforementioned features are paginated by a dedicated pagination helper. 

![](project-photos/transactions-overview.png)

Through the 'Users' view, the admin can block and unblock each user. 

![](project-photos/users-overview.png)

Blocking a user prevents him/her from making transactions other than top up.

### Overdraft
Users can turn on or off their overdraft feature on a specific wallet from 'My Wallets'.
The funds will be automatically deducted when needed and repaid when the account is financed.

The following photos show some of the main features of the Overdrafts.
![](project-photos/overdrafts-focus.png)
![](project-photos/overdrafts-overview.png)
When the user chooses to pay back the overdraft, they are redirected to the card 
top up page and if they choose to pay back all overdrafts, they are redirected
to the all wallets section.
![](project-photos/overdrafts-new-update.png)
### Savings
![](project-photos/savings-circled.png)
![](project-photos/savings-overview.png)
![](project-photos/savings-new-deposit.png)
![](project-photos/savings-new-savings-plan.png)
### Currencies
![](project-photos/currencies-circled.png)
![](project-photos/currencies-overview.png)
It is important to note that all currencies are updated by an external api to a 
forex automatically. Due to the limited free updates/queries, the feature is
commented/stopped. Please refer to the code snipped section to see how to
activate the feature.
![](project-photos/currencies-new.png)
### Email Verification
### User Photos
### Payment Categories
The payment categories have been designed in order to differentiate the different payments from one
another. Furthermore, a set of core categories has been set up with every new account registration.
This takes the job away from the customer and limits the amount of similar and or duplicate entries 
in the database. Nevertheless, the user can still create new categories or modify the current ones.
![](project-photos/categories-selected.png)
![](project-photos/categories-overview.png)
![](project-photos/categories-new.png)

## Code Snippets
### Mock Bank Api
![](project-photos/mock-bank-code.png)
### Automated Currency Update
Please remove '//' in front of '@Scheduled...' in order to activate and please 
put back the '//' within less than a minute of operation. The queries are 
limited and might run out.
![](project-photos/currency-update-1.png)
![](project-photos/currency-update-2.png)
## Installation Guide
## User Manual
### REST
####Swagger
Once the application is running, please open the following 
<a href='http://localhost:8080/swagger-ui.html'> link</a>. Now you can see the full documentation of 
the application's controllers.
####Postman
In order to enter the current pre-set api queries, the JSON text from 
('...wally\api.queries\forPostmanImport') has to be copied (it can be opened through Intellij)
and then pasted in Postman in the empty box as shown on the second image below.
![](project-photos/postman-first-step.png)
![](project-photos/postman-second-step.png)
### MVC
## Tests
## Limitations
1. The bank uses double instead of BigDecimal for all the transaction calculations. For the purpose
of the project, speed was used as a priority instead of accuracy.
2. The pre-set user categories are picked on the basis of ID. Categories with ID 1-11 are the ones
which are picked by default.
3. The bank must always have money to give for overdrafts or savings.
4. There is no transfer record for overdraft bank financing. If there is, it would be visible 
by the customer and might cause confusion.
5. Generic service methods are overloaded with and without authorization respectively. The developer
must know when to use which and should not skip authorization check when it's due.
6. Past overdrafts, savings and transactions will accumulate and there is no mechanism of archiving them.
7. Wally has only one wallet in one currency. Typically, the bank will have a wallet in every currency.
8. Entering wrong CVV more than 3 times doesn't stop user from trying again. This is the user's bank
task typically.
9. Head, header, footer and script thymeleaf fragments can stay on one page but are divided into four
different ones in order to increase visibility.
10. When a user tops up their wallet, he/she will see a transaction from the same wallet money is
sent to. This is due to the fact that with every debit/credit card registered, Wally needs to register
the foreign bank account where the user card is linked to and there is no such feature right now.
11. Pending transactions are processed at a later date. However, if the transaction is from a 
different currency, the exchange rate used for conversion will be the one from the 
first payment attempt.
12. If the system is down at midnight, customer deposits won't be paid out.
13. Interest is accrued monthly for both overdrafts and savings.
14. When making transfers, there has to be a simultaneous update of both the account from which
money is taken from and the one money is transferred to.

## Authors and acknowledgment
Written by Veselina Georgieva and Zahary Ninov. <br>
We would like to thank the full Telerik Academy Team for their constant support and the time, which they
passionately devoted on teaching us the skills necessary to write this projects. We also want
to thank our mentors who guided us in the process of the software development course and 
navigating us through the pitfalls of our ambitious visions. Thank you all 
for providing us with a stepping stone for our new career!

## Project status
The project is still in the process of development at the time of writing. Please check our backlog for 
further details on known issues and missing but necessary features.

[comment]: <> (## Add your files)

[comment]: <> (- [ ] [Create]&#40;https://gitlab.com/-/experiment/new_project_readme_content:e8b5f2de9c9708ec646b2efc79ca1b68?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file&#41;)

[comment]: <> (  or [upload]&#40;https://gitlab.com/-/experiment/new_project_readme_content:e8b5f2de9c9708ec646b2efc79ca1b68?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file&#41;)

[comment]: <> (  files)

[comment]: <> (- [ ] [Add files using the command line]&#40;https://gitlab.com/-/experiment/new_project_readme_content:e8b5f2de9c9708ec646b2efc79ca1b68?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line&#41;)

[comment]: <> (  or push an existing Git repository with the following command:)

[comment]: <> (```)

[comment]: <> (cd existing_repo)

[comment]: <> (git remote add origin https://gitlab.com/virtual-wallet/virtual-wallet.git)

[comment]: <> (git branch -M main)

[comment]: <> (git push -uf origin main)

[comment]: <> (```)

[comment]: <> (## Integrate with your tools)

[comment]: <> (- [ ] [Set up project integrations]&#40;https://gitlab.com/-/experiment/new_project_readme_content:e8b5f2de9c9708ec646b2efc79ca1b68?https://docs.gitlab.com/ee/user/project/integrations/&#41;)

[comment]: <> (## Collaborate with your team)

[comment]: <> (- [ ] [Invite team members and collaborators]&#40;https://gitlab.com/-/experiment/new_project_readme_content:e8b5f2de9c9708ec646b2efc79ca1b68?https://docs.gitlab.com/ee/user/project/members/&#41;)

[comment]: <> (- [ ] [Create a new merge request]&#40;https://gitlab.com/-/experiment/new_project_readme_content:e8b5f2de9c9708ec646b2efc79ca1b68?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html&#41;)

[comment]: <> (- [ ] [Automatically close issues from merge requests]&#40;https://gitlab.com/-/experiment/new_project_readme_content:e8b5f2de9c9708ec646b2efc79ca1b68?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically&#41;)

[comment]: <> (- [ ] [Automatically merge when pipeline succeeds]&#40;https://gitlab.com/-/experiment/new_project_readme_content:e8b5f2de9c9708ec646b2efc79ca1b68?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html&#41;)

[comment]: <> (## Test and Deploy)

[comment]: <> (Use the built-in continuous integration in GitLab.)

[comment]: <> (- [ ] [Get started with GitLab CI/CD]&#40;https://gitlab.com/-/experiment/new_project_readme_content:e8b5f2de9c9708ec646b2efc79ca1b68?https://docs.gitlab.com/ee/ci/quick_start/index.html&#41;)

[comment]: <> (- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing&#40;SAST&#41;]&#40;https://gitlab.com/-/experiment/new_project_readme_content:e8b5f2de9c9708ec646b2efc79ca1b68?https://docs.gitlab.com/ee/user/application_security/sast/&#41;)

[comment]: <> (- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy]&#40;https://gitlab.com/-/experiment/new_project_readme_content:e8b5f2de9c9708ec646b2efc79ca1b68?https://docs.gitlab.com/ee/topics/autodevops/requirements.html&#41;)

[comment]: <> (- [ ] [Use pull-based deployments for improved Kubernetes management]&#40;https://gitlab.com/-/experiment/new_project_readme_content:e8b5f2de9c9708ec646b2efc79ca1b68?https://docs.gitlab.com/ee/user/clusters/agent/&#41;)

[comment]: <> (***)

[comment]: <> (# Editing this README)

[comment]: <> (When you're ready to make this README your own, just edit this file and use the handy template below &#40;or feel free to structure it however you want - this is just a starting point!&#41;.  Thank you to [makeareadme.com]&#40;https://gitlab.com/-/experiment/new_project_readme_content:e8b5f2de9c9708ec646b2efc79ca1b68?https://www.makeareadme.com/&#41; for this template.)

[comment]: <> (## Suggestions for a good README)

[comment]: <> (Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.)

[comment]: <> (## Name)

[comment]: <> (Choose a self-explaining name for your project.)

[comment]: <> (## Description)

[comment]: <> (Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.)

[comment]: <> (## Badges)

[comment]: <> (On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.)

[comment]: <> (## Visuals)

[comment]: <> (Depending on what you are making, it can be a good idea to include screenshots or even a video &#40;you'll frequently see GIFs rather than actual videos&#41;. Tools like ttygif can help, but check out Asciinema for a more sophisticated method.)

[comment]: <> (## Installation)

[comment]: <> (Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.)

[comment]: <> (## Usage)

[comment]: <> (Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.)

[comment]: <> (## Support)

[comment]: <> (Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.)

[comment]: <> (## Roadmap)

[comment]: <> (If you have ideas for releases in the future, it is a good idea to list them in the README.)

[comment]: <> (## Contributing)

[comment]: <> (State if you are open to contributions and what your requirements are for accepting them.)

[comment]: <> (For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.)

[comment]: <> (You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.)

[comment]: <> (## Authors and acknowledgment)

[comment]: <> (Show your appreciation to those who have contributed to the project.)

[comment]: <> (## License)

[comment]: <> (For open source projects, say how it is licensed.)

[comment]: <> (## Project status)

[comment]: <> (If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.)

