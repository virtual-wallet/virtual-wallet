package com.telerikacademy.wally.helpers;

import com.telerikacademy.wally.models.SavingsRecord;

import java.time.LocalDate;

import static com.telerikacademy.wally.helpers.SavingsFeatureHelper.createMockSavingsFeature;
import static com.telerikacademy.wally.helpers.WalletHelper.createMockWallet;

public class SavingsRecordHelper {

    public static SavingsRecord createMockSavingsRecord() {
        SavingsRecord mockSavingsRecord = new SavingsRecord();
        LocalDate pastDate = LocalDate.now().minusMonths(6);

        mockSavingsRecord.setId(1);
        mockSavingsRecord.setSavingsFeature(createMockSavingsFeature());
        mockSavingsRecord.setAer(2.5);
        mockSavingsRecord.setBaseAmount(1000.0);
        mockSavingsRecord.setStartDate(java.sql.Date.valueOf(pastDate));
        mockSavingsRecord.setDurationMonths(6);
        mockSavingsRecord.setWallet(createMockWallet());
        return mockSavingsRecord;
    }

}
