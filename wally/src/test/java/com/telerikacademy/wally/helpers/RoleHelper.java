package com.telerikacademy.wally.helpers;

import com.telerikacademy.wally.models.Role;

public class RoleHelper {

    public static Role createMockRole() {
        Role mockRole = new Role();
        mockRole.setId(2);
        mockRole.setName("Admin");
        return mockRole;
    }
}
