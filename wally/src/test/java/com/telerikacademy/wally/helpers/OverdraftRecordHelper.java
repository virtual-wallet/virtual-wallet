package com.telerikacademy.wally.helpers;

import com.telerikacademy.wally.models.OverdraftRecord;

import java.time.LocalDate;

import static com.telerikacademy.wally.helpers.OverdraftFeatureHelper.createMockOverdraftFeature;
import static com.telerikacademy.wally.helpers.WalletHelper.createMockWallet;

public class OverdraftRecordHelper {

    public static OverdraftRecord createMockOverdraftRecord() {
        OverdraftRecord mockOverdraftRecord = new OverdraftRecord();
        LocalDate pastDate = LocalDate.now().minusMonths(3);

        mockOverdraftRecord.setId(1);
        mockOverdraftRecord.setOverdraftFeature(createMockOverdraftFeature());
        mockOverdraftRecord.setUnpaidMonths(2);
        mockOverdraftRecord.setPaid(false);
        mockOverdraftRecord.setDateStarted(java.sql.Date.valueOf(pastDate));
        mockOverdraftRecord.setWallet(createMockWallet());
        return mockOverdraftRecord;
    }

}
