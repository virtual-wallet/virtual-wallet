package com.telerikacademy.wally.helpers;

import com.telerikacademy.wally.models.Category;

public class CategoryHelper {

    public static Category createMockCategory() {
        Category mockCategory = new Category();
        mockCategory.setId(1);
        mockCategory.setName("MockCategory");
        return mockCategory;
    }
}
