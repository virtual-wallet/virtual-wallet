package com.telerikacademy.wally.helpers;

import com.telerikacademy.wally.models.dtos.TransactionFilterSortDto;
import com.telerikacademy.wally.models.enums.contracts.SortOptions;

import static com.telerikacademy.wally.models.enums.TransactionSortOptions.DATE_DESC;

public class TransactionFilterSortDtoHelper {

    public static TransactionFilterSortDto createMockTransactionFilterSortDto() {
        TransactionFilterSortDto mockTransactionFilterSortDto = new TransactionFilterSortDto();

        mockTransactionFilterSortDto.setStatus("Completed");
        mockTransactionFilterSortDto.setSort(DATE_DESC.getPreview());
        return mockTransactionFilterSortDto;
    }
}
