package com.telerikacademy.wally.helpers;

import com.telerikacademy.wally.models.CardType;

public class CardTypeHelper {

    public static CardType createMockCardType() {
        CardType mockCardType = new CardType();
        mockCardType.setId(1);
        mockCardType.setType("MockCardType");
        return mockCardType;
    }
}
