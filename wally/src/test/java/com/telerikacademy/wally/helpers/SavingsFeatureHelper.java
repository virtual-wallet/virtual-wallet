package com.telerikacademy.wally.helpers;

import com.telerikacademy.wally.models.SavingsFeature;

public class SavingsFeatureHelper {

    public static SavingsFeature createMockSavingsFeature() {
        SavingsFeature mockSavingsFeature = new SavingsFeature();
        mockSavingsFeature.setId(1);
        mockSavingsFeature.setName("MockSavingsFeature");
        mockSavingsFeature.setAer(2.5);
        mockSavingsFeature.setDuration(6);
        return mockSavingsFeature;
    }
}
