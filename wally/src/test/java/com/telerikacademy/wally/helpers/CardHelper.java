package com.telerikacademy.wally.helpers;

import com.telerikacademy.wally.models.Card;

import java.time.LocalDate;

import static com.telerikacademy.wally.helpers.CardTypeHelper.createMockCardType;
import static com.telerikacademy.wally.helpers.CurrencyHelper.createMockCurrency;
import static com.telerikacademy.wally.helpers.UserHelper.createMockUser;

public class CardHelper {
    public static Card createMockCard() {
        Card mockCard = new Card();
        LocalDate futureDate =LocalDate.now().plusYears(3);

        mockCard.setId(1);
        mockCard.setCardNumber("123456789");
        mockCard.setCardType(createMockCardType());
        mockCard.setCvv(123);
        mockCard.setExpireDate(java.sql.Date.valueOf(futureDate));;
        mockCard.setCurrency(createMockCurrency());
        mockCard.setUser(createMockUser());
        return mockCard;
    }



}
