package com.telerikacademy.wally.helpers;

import com.telerikacademy.wally.models.Transaction;

import java.time.LocalDate;

import static com.telerikacademy.wally.helpers.CategoryHelper.createMockCategory;
import static com.telerikacademy.wally.helpers.StatusHelper.createMockStatus;
import static com.telerikacademy.wally.helpers.UserHelper.createMockUser;
import static com.telerikacademy.wally.helpers.WalletHelper.createMockWallet;

public class TransactionHelper {

    public static Transaction createMockTransaction() {
        Transaction mockTransaction = new Transaction();
        LocalDate pastDate =LocalDate.now().minusMonths(6);

        mockTransaction.setTransactionId(1);
        mockTransaction.setTransactionTime(java.sql.Date.valueOf(pastDate));
        mockTransaction.setExchangeRate(1.5);
        mockTransaction.setTransferValue(500);
        mockTransaction.setCategory(createMockCategory());
        mockTransaction.setStatus(createMockStatus());
        mockTransaction.setFromWallet(createMockWallet());
        mockTransaction.setToWallet(createMockWallet());
        mockTransaction.setFromUser(createMockUser());
        return mockTransaction;
    }

}
