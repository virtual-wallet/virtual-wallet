package com.telerikacademy.wally.helpers;

import com.telerikacademy.wally.models.Wallet;

import static com.telerikacademy.wally.helpers.CurrencyHelper.createMockCurrency;
import static com.telerikacademy.wally.helpers.UserHelper.createMockUser;

public class WalletHelper {
    
    public static Wallet createMockWallet() {
        Wallet mockWallet = new Wallet();
        mockWallet.setWalletId(1);
        mockWallet.setBalance(10000);
        mockWallet.setOwner(createMockUser());
        mockWallet.setCurrency(createMockCurrency());
        mockWallet.setHasOverdraftEnabled(true);
        return mockWallet;
    }

    public static Wallet createMockPayeeWallet() {
        Wallet mockWallet = new Wallet();
        mockWallet.setWalletId(2);
        mockWallet.setBalance(5000);
        mockWallet.setOwner(createMockUser());
        mockWallet.setCurrency(createMockCurrency());
        mockWallet.setHasOverdraftEnabled(true);
        return mockWallet;
    }
    
}
