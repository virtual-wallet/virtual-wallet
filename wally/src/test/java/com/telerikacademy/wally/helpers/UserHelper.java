package com.telerikacademy.wally.helpers;

import com.telerikacademy.wally.models.User;

import java.util.Set;

import static com.telerikacademy.wally.helpers.CategoryHelper.createMockCategory;
import static com.telerikacademy.wally.helpers.RoleHelper.createMockRole;

public class UserHelper {

    public static User createMockUser() {
        User mockUser = new User();

        mockUser.setUserId(1);
        mockUser.setUsername("MockUser");
        mockUser.setBlocked(false);
        mockUser.setEnabled(true);
        mockUser.setCategoriesSet(Set.of(createMockCategory()));
        mockUser.setEmail("MockEmail");
        mockUser.setPhone("0886123456");
        mockUser.setRoleSet(Set.of(createMockRole()));
        mockUser.setVerificationCode("MockCode");
        mockUser.setPassword("MockPassword123#$!");
        return mockUser;
    }
    public static User createMockPayeeUser() {
        User mockUser = new User();

        mockUser.setUserId(2);
        mockUser.setUsername("MockPayeeUser");
        mockUser.setBlocked(false);
        mockUser.setEnabled(true);
        mockUser.setCategoriesSet(Set.of(createMockCategory()));
        mockUser.setEmail("MockEmail");
        mockUser.setPhone("0886123456");
        mockUser.setRoleSet(Set.of(createMockRole()));
        mockUser.setVerificationCode("MockCode");
        mockUser.setPassword("MockPassword123#$!");
        return mockUser;
    }


}
