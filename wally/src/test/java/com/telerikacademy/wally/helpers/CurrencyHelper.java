package com.telerikacademy.wally.helpers;

import com.telerikacademy.wally.models.Currency;

public class CurrencyHelper {

    public static Currency createMockCurrency() {
        Currency mockCurrency = new Currency();
        mockCurrency.setId(1);
        mockCurrency.setCurrencyName("MockCurrency");
        mockCurrency.setSymbol("£");
        mockCurrency.setIso("GBP");
        mockCurrency.setExchangeRate(0.835);
        return mockCurrency;
    }
}
