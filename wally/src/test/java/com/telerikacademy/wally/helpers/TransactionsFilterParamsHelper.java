package com.telerikacademy.wally.helpers;

import com.telerikacademy.wally.models.enums.contracts.SortOptions;
import com.telerikacademy.wally.models.params.TransactionsFilterParams;

import java.time.LocalDate;
import java.util.Date;

import static com.telerikacademy.wally.models.enums.TransactionSortOptions.*;

public class TransactionsFilterParamsHelper {

    public static TransactionsFilterParams createMockTransactionFilterParams() {
        TransactionsFilterParams transactionsFilterParams = new TransactionsFilterParams();
        SortOptions sortOption = DATE_DESC;
        LocalDate pastDate = LocalDate.now().minusMonths(1);

        transactionsFilterParams.setSortOptions(sortOption);
        transactionsFilterParams.setToUsername("MockUser");
        transactionsFilterParams.setFromUsername("MockUser2");
        transactionsFilterParams.setDateFrom(java.sql.Date.valueOf(pastDate));
        transactionsFilterParams.setDateTo(new Date());
        transactionsFilterParams.setIsCredited(false);
        transactionsFilterParams.setSortOptions(sortOption);
        transactionsFilterParams.setStatus(2);
        transactionsFilterParams.setCategory(1);
        transactionsFilterParams.setFromWalletId(1);
        transactionsFilterParams.setToWalletId(2);
        return transactionsFilterParams;
    }

}
