package com.telerikacademy.wally.helpers;

import com.telerikacademy.wally.models.OverdraftFeature;

public class OverdraftFeatureHelper {

    public static OverdraftFeature createMockOverdraftFeature() {
        OverdraftFeature mockOverdraftFeature = new OverdraftFeature();
        mockOverdraftFeature.setId(1);
        mockOverdraftFeature.setName("MockOverdraftFeature");
        mockOverdraftFeature.setApr(12.5);
        mockOverdraftFeature.setMaxMonthsWithoutPayment(6);
        return mockOverdraftFeature;
    }
}
