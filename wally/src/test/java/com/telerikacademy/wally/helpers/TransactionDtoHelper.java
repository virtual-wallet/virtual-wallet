package com.telerikacademy.wally.helpers;

import com.telerikacademy.wally.models.dtos.TransactionDto;

public class TransactionDtoHelper {

    public static TransactionDto createMockTransactionDto() {
        TransactionDto transactionDto = new TransactionDto();

        transactionDto.setExchangeRate(1);
        transactionDto.setCategoryId(1);
        transactionDto.setStatusId(2);
        transactionDto.setValue(1000);
        transactionDto.setFromWalletId(1);
        transactionDto.setToWalletId(2);
        transactionDto.setFromUserId(1);
        return transactionDto;
    }
}
