package com.telerikacademy.wally.helpers;

import com.telerikacademy.wally.models.Status;

public class StatusHelper {

    public static Status createMockStatus() {
        Status mockStatus = new Status();
        mockStatus.setId(1);
        mockStatus.setName("MockStatus");
        return mockStatus;
    }
}
