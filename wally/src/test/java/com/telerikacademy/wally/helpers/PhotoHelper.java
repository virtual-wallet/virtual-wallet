package com.telerikacademy.wally.helpers;

import com.telerikacademy.wally.models.Photo;

import static com.telerikacademy.wally.helpers.UserHelper.createMockUser;

public class PhotoHelper {

    public static Photo createMockPhoto() {
        Photo mockPhoto = new Photo();
        mockPhoto.setId(1);
        mockPhoto.setPhoto("MockPhoto");
        mockPhoto.setUser(createMockUser());
        return mockPhoto;
    }
}
