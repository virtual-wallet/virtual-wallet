package com.telerikacademy.wally.services;

import com.telerikacademy.wally.config.ExternalApiUrlConfig;
import com.telerikacademy.wally.models.*;
import com.telerikacademy.wally.models.dtos.TransactionDto;
import com.telerikacademy.wally.services.contracts.*;
import com.telerikacademy.wally.utils.mappers.TransactionMapper;
import com.telerikacademy.wally.utils.mappers.OverdraftMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.wally.helpers.CurrencyHelper.createMockCurrency;
import static com.telerikacademy.wally.helpers.OverdraftRecordHelper.createMockOverdraftRecord;
import static com.telerikacademy.wally.helpers.TransactionDtoHelper.createMockTransactionDto;
import static com.telerikacademy.wally.helpers.TransactionHelper.createMockTransaction;
import static com.telerikacademy.wally.helpers.UserHelper.createMockUser;
import static com.telerikacademy.wally.helpers.WalletHelper.createMockWallet;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class OverdraftManagerTests {

    @Mock
    WalletService walletService;
    @Mock
    UserService userService;
    @Mock
    OverdraftRecordService overdraftRecordService;
    @Mock
    OverdraftMapper overdraftMapper;
    @Mock
    OverdraftFeatureService overdraftFeatureService;
    @Mock
    ExternalApiUrlConfig externalApiUrlConfig;
    @Mock
    TransactionService transactionService;
    @Mock
    TransactionMapper transactionMapper;

    @InjectMocks
    OverdraftManagerImpl service;

//    @Test
//    public void accrueApr_should_changeBalance_and_callRepository() {
//        List<OverdraftRecord> mockListRecords = new ArrayList<>();
//        OverdraftRecord mockOverdraftRecord1 = createMockOverdraftRecord();
//        OverdraftRecord mockOverdraftRecord2 = createMockOverdraftRecord();
//        mockOverdraftRecord2.setId(2);
//        mockListRecords.add(mockOverdraftRecord1);
//        mockListRecords.add(mockOverdraftRecord2);
//        Wallet wallet = mockOverdraftRecord1.getWallet();
//
//
//        User admin = createMockUser();
//
//        service.accrueApr(mockListRecords, admin);
//
//        Mockito.verify(walletService, Mockito.times(2)).update(wallet,admin);
//    }

    @Test
    public void manageUserOverdrafts_should_changeOverdraftRecords_UserBlocked_deptInWallet_and_callRepository() {
        List<OverdraftRecord> mockListRecords = new ArrayList<>();
        OverdraftRecord mockOverdraftRecord1 = createMockOverdraftRecord();
        OverdraftRecord mockOverdraftRecord2 = createMockOverdraftRecord();
        OverdraftRecord mockOverdraftRecord3 = createMockOverdraftRecord();
        OverdraftRecord mockOverdraftRecord4 = createMockOverdraftRecord();
        OverdraftRecord mockOverdraftRecord5 = createMockOverdraftRecord();
        mockOverdraftRecord5.setUnpaidMonths(8);
        mockOverdraftRecord3.setUnpaidMonths(5);

        mockOverdraftRecord3.getWallet().setBalance(-1000);
        mockOverdraftRecord2.getWallet().setBalance(-1000);
        mockOverdraftRecord1.getWallet().setBalance(-1000);

        Wallet wallet = mockOverdraftRecord1.getWallet();

        User blockedUser = createMockUser();
        blockedUser.setBlocked(true);
        mockOverdraftRecord5.getWallet().setOwner(blockedUser);

        mockListRecords.add(mockOverdraftRecord3);
        mockListRecords.add(mockOverdraftRecord2);
        mockListRecords.add(mockOverdraftRecord5);
        mockListRecords.add(mockOverdraftRecord4);

        User admin = createMockUser();
        List<Wallet> walletsInDept = List.of(mockOverdraftRecord3.getWallet(),
                mockOverdraftRecord2.getWallet(),
                mockOverdraftRecord1.getWallet());

        List<OverdraftRecord> updatedRecordsOfLastMonthDebtors = List.of(mockOverdraftRecord2, mockOverdraftRecord3);
        List<OverdraftRecord> currentRecordsOfDebtors = List.of(mockOverdraftRecord1,
                mockOverdraftRecord2,
                mockOverdraftRecord3);

        Mockito.lenient().when(overdraftRecordService.getAllByField("isPaid", false))
                .thenReturn(mockListRecords)
                .thenReturn(updatedRecordsOfLastMonthDebtors)
                .thenReturn(currentRecordsOfDebtors);
        Mockito.lenient().when(userService.getById(externalApiUrlConfig.getBankMainAcc()))
                .thenReturn(admin);
        when(walletService.getAllInDept()).thenReturn(walletsInDept);

        service.manageUserOverdrafts();

        Mockito.verify(walletService, Mockito.times(3)).update(mockOverdraftRecord1.getWallet(), admin);
        Mockito.verify(overdraftRecordService, Mockito.times(1)).update(mockOverdraftRecord2, admin);
        Mockito.verify(overdraftRecordService, Mockito.times(1)).update(mockOverdraftRecord4, admin);
        Mockito.verify(overdraftRecordService, Mockito.times(1)).update(mockOverdraftRecord5, admin);
        Mockito.verify(userService, Mockito.times(2)).update(mockOverdraftRecord4.getWallet().getOwner(), admin);

    }

    @Test
    public void financeUserPayment_should_Update_userWallet() {
        Wallet payerWallet = createMockWallet();
        payerWallet.setBalance(-1000);
        Wallet payeeWallet = createMockWallet();
        payerWallet.setWalletId(2);
        Wallet bankWallet = createMockWallet();
        bankWallet.setWalletId(3);
        Transaction transaction = createMockTransaction();

        when(walletService.getById(2)).thenReturn(payeeWallet);
        when(externalApiUrlConfig.getBankMainAcc()).thenReturn(3);
        when(walletService.getById(3)).thenReturn(bankWallet);

        service.financeUserPayment(transaction, payerWallet, payerWallet.getBalance());

        Mockito.verify(walletService, Mockito.times(1)).update(bankWallet);
        Mockito.verify(walletService, Mockito.times(1)).update(payeeWallet);
        Mockito.verify(walletService, Mockito.times(1)).update(payerWallet);
    }

    @Test
    public void paybackUserOverdraft() {
        TransactionDto transactionDto = createMockTransactionDto();
        Wallet payerWallet = createMockWallet();
        payerWallet.setBalance(-1000);
        Wallet payeeWallet = createMockWallet();
        payerWallet.setWalletId(2);
        Wallet bankWallet = createMockWallet();
        bankWallet.setWalletId(3);

        when(externalApiUrlConfig.getBankUserId()).thenReturn(String.valueOf(3));
        when(userService.getById(3)).thenReturn(bankWallet.getOwner());
        when(externalApiUrlConfig.getBankMainAcc()).thenReturn(3);
        when(walletService.getById(3)).thenReturn(bankWallet);

        service.paybackUserOverdraftToBankOnly(payeeWallet, 500);

        Mockito.verify(walletService, Mockito.times(1)).update(bankWallet, bankWallet.getOwner());
    }

    @Test
    public void paybackUserOverdraft_differentCurrency_payFullDept() {
        TransactionDto transactionDto = createMockTransactionDto();
        Wallet payeeWallet = createMockWallet();
        payeeWallet.setBalance(-1000);
        payeeWallet.setWalletId(2);
        Wallet bankWallet = createMockWallet();
        bankWallet.setWalletId(3);
        Currency currency = createMockCurrency();
        currency.setId(3);
        bankWallet.setCurrency(currency);

        when(externalApiUrlConfig.getBankUserId()).thenReturn(String.valueOf(3));
        when(userService.getById(3)).thenReturn(bankWallet.getOwner());
        when(externalApiUrlConfig.getBankMainAcc()).thenReturn(3);
        when(walletService.getById(3)).thenReturn(bankWallet);

        service.paybackUserOverdraftToBankOnly(payeeWallet, 50);

        Mockito.verify(walletService, Mockito.times(1)).update(bankWallet, bankWallet.getOwner());
    }

}
