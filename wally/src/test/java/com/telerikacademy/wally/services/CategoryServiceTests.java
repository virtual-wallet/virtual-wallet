package com.telerikacademy.wally.services;


import com.telerikacademy.wally.exceptions.EntityNotFoundException;
import com.telerikacademy.wally.models.Category;
import com.telerikacademy.wally.models.User;
import com.telerikacademy.wally.repositories.contracts.CategoryRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import static com.telerikacademy.wally.helpers.CategoryHelper.createMockCategory;
import static com.telerikacademy.wally.helpers.UserHelper.createMockPayeeUser;
import static com.telerikacademy.wally.helpers.UserHelper.createMockUser;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CategoryServiceTests {

    @Mock
    CategoryRepository mockRepository;

    @InjectMocks
    CategoryServiceImpl service;

    @Test
    void getBaseCategories_should_callRepository() {
        // Arrange
        when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());
        // Act
        service.getBaseCategories();
        // Assert
        Mockito.verify(mockRepository, Mockito.times(1)).getAll();
    }

    @Test
    void getOrCreateCategoryIfExists_should_callRepository_Once_if_Exists() {
        Category category = createMockCategory();
        User user = createMockUser();
        when(mockRepository.getByField("name", category.getName())).thenReturn(category);

                service.getOrCreateCategoryIfExists(category, user);

        Mockito.verify(mockRepository, Mockito.times(1)).getByField("name", category.getName());
    }

    @Test
    void getOrCreateCategoryIfExists_should_callRepository_Trice_if_Does_Not_Exist() {
        Category category = createMockCategory();
        User user = createMockUser();

        when(mockRepository.getByField("name", category.getName()))
                .thenThrow(EntityNotFoundException.class)
                .thenReturn(category);

        service.getOrCreateCategoryIfExists(category, user);

        Mockito.verify(mockRepository, Mockito.times(2)).getByField("name", category.getName());
        Mockito.verify(mockRepository, Mockito.times(1)).create(category);
    }

    @Test
    void deleteIfNotUsed_should_CallRepository_If_Category_NotUsed(){
        Category category = createMockCategory();
        User user = createMockUser();
        category.setUserSet(new HashSet<>());

        service.deleteIfNotUsed(category,user);

        Mockito.verify(mockRepository, Mockito.times(1)).delete(category.getId());

    }


}
