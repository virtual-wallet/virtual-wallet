package com.telerikacademy.wally.services;

import com.telerikacademy.wally.config.ExternalApiUrlConfig;
import com.telerikacademy.wally.exceptions.PaymentIssueException;
import com.telerikacademy.wally.models.*;
import com.telerikacademy.wally.models.dtos.TransactionDto;
import com.telerikacademy.wally.services.contracts.*;
import com.telerikacademy.wally.utils.mappers.TransactionMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.telerikacademy.wally.helpers.CurrencyHelper.createMockCurrency;
import static com.telerikacademy.wally.helpers.TransactionDtoHelper.createMockTransactionDto;
import static com.telerikacademy.wally.helpers.TransactionHelper.createMockTransaction;
import static com.telerikacademy.wally.helpers.UserHelper.createMockUser;
import static com.telerikacademy.wally.helpers.WalletHelper.createMockPayeeWallet;
import static com.telerikacademy.wally.helpers.WalletHelper.createMockWallet;
import static org.hamcrest.Matchers.matchesPattern;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@ExtendWith(MockitoExtension.class)
public class PaymentManagerTests {

    @Mock
    WalletService walletService;
    @Mock
    TransactionService transactionService;
    @Mock
    ExternalApiUrlConfig externalApiUrlConfig;
    @Mock
    TransactionMapper transactionMapper;
    @Mock
    StatusService statusService;
    @Mock
    UserService userService;
    @Mock
    OverdraftManager overdraftManager;

    @InjectMocks
    PaymentManagerImpl service;

    @Test
    public void setCardPaymentIn_should_createTransaction_and_Make_payment_IfStatus_Completed() {
        Wallet wallet = createMockWallet();
        Transaction transaction = createMockTransaction();
        TransactionDto dto = createMockTransactionDto();
        User user = createMockUser();

        when(walletService.getById(anyInt())).thenReturn(wallet);
        when(transactionMapper.fromDtoExternalCard(dto, wallet, user)).thenReturn(transaction);
        doNothing().when(transactionService).create(transaction);

        service.setCardPaymentIn(anyInt(), dto, user);

        Mockito.verify(walletService, Mockito.times(1))
                .executeCardToWalletTransaction(wallet, dto.getValue(), dto.getExchangeRate());

    }

    @Test
    public void setWalletToWalletPayment_makesPayment() {
        Wallet payerWallet = createMockWallet();
        Wallet payeeWallet = createMockPayeeWallet();
        TransactionDto dto = createMockTransactionDto();
        Transaction transaction = createMockTransaction();
        User admin = createMockUser();

        when(walletService.getById(anyInt()))
                .thenReturn(payerWallet)
                .thenReturn(payeeWallet);
        when(transactionMapper.fromDtoWalletToWallet(dto, admin)).thenReturn(transaction);

        service.setInternalPayment(transaction, admin);

//        Mockito.verify(walletService, Mockito.times(1))
//                .update(payeeWallet, payeeWallet.getOwner());
//        Mockito.verify(walletService, Mockito.times(1))
//                .update(payerWallet, payerWallet.getOwner());
        Mockito.verify(transactionService, Mockito.times(1))
                .create(transaction);
    }

    @Test
    public void setWalletToWalletPayment_makesPayment_withOverdraftPayment() {
        Wallet payerWallet = createMockWallet();
        Wallet payeeWallet = createMockPayeeWallet();
        payeeWallet.setBalance(-100);
        TransactionDto dto = createMockTransactionDto();
        Transaction transaction = createMockTransaction();
        User admin = createMockUser();

        when(walletService.getById(anyInt()))
                .thenReturn(payerWallet)
                .thenReturn(payeeWallet);
        when(transactionMapper.fromDtoWalletToWallet(dto, admin)).thenReturn(transaction);

        service.setInternalPayment(transaction, admin);

//        Mockito.verify(walletService, Mockito.times(1))
//                .update(payeeWallet, payeeWallet.getOwner());
//        Mockito.verify(walletService, Mockito.times(1))
//                .update(payerWallet, payerWallet.getOwner());
        Mockito.verify(transactionService, Mockito.times(1))
                .create(transaction);
    }

    @Test
    public void setWalletToWalletPayment_withOverdraft() {
        Wallet payerWallet = createMockWallet();
        payerWallet.setBalance(-100);
        Wallet payeeWallet = createMockPayeeWallet();
        TransactionDto dto = createMockTransactionDto();
        Transaction transaction = createMockTransaction();
        User admin = createMockUser();

        when(walletService.getById(anyInt()))
                .thenReturn(payerWallet)
                .thenReturn(payeeWallet);
        when(transactionMapper.fromDtoWalletToWallet(dto, admin)).thenReturn(transaction);

        service.setInternalPayment(transaction, admin);

        Mockito.verify(transactionService, Mockito.times(1))
                .create(transaction);
    }

    @Test
    public void setWalletToWalletPayment_throws_when_UserDoesNotHaveOverdraftEnabled_or_haveMoney() {
        Wallet payerWallet = createMockWallet();
        payerWallet.setBalance(0);
        payerWallet.setHasOverdraftEnabled(false);
        Wallet payeeWallet = createMockPayeeWallet();
        TransactionDto dto = createMockTransactionDto();
        Transaction transaction = createMockTransaction();
        User admin = createMockUser();
        when(walletService.getById(anyInt()))
                .thenReturn(payerWallet)
                .thenReturn(payeeWallet);

        Assertions.assertThrows(PaymentIssueException.class,
                () -> service.setInternalPayment(transaction, admin));
    }

    @Test
    public void setWalletToWalletPayment_MakesPayment_with_differentCurrencies() {
        Wallet payerWallet = createMockWallet();
        Wallet payeeWallet = createMockPayeeWallet();
        Currency currency = createMockCurrency();
        currency.setId(3);
        payeeWallet.setCurrency(currency);
        TransactionDto dto = createMockTransactionDto();
        Transaction transaction = createMockTransaction();
        User admin = createMockUser();

        when(walletService.getById(anyInt()))
                .thenReturn(payerWallet)
                .thenReturn(payeeWallet);
        when(transactionMapper.fromDtoWalletToWallet(dto, admin)).thenReturn(transaction);

        service.setInternalPayment(transaction, admin);

//        Mockito.verify(walletService, Mockito.times(1))
//                .update(payeeWallet, payeeWallet.getOwner());
//        Mockito.verify(walletService, Mockito.times(1))
//                .update(payerWallet, payerWallet.getOwner());
        Mockito.verify(transactionService, Mockito.times(1))
                .create(transaction);
    }

//    @Test
//    public void checkPendingReceivedTransactionStatuses_Updates_pendingTransactions() {
//        User admin = createMockUser();
//        Transaction transaction = createMockTransaction();
//        List<Transaction> transactions = List.of(transaction);
//        Status status = createMockStatus();
//        Status statusCompleted = createMockStatus();
//        statusCompleted.setId(2);
//        status.setId(3);
//        transaction.setStatus(status);
//        String url = "http://localhost:8080/api/mock-bank/check-status";
//        MockBankDto dto = new MockBankDto();
//        ResponseEntity response = new ResponseEntity<>("approved", HttpStatus.OK);
//        RestTemplate restTemplate = Mockito.spy(RestTemplate.class);
//
//        MockRestServiceServer mockServer = MockRestServiceServer.createServer(restTemplate);
//        MockRestServiceServer.bindTo(restTemplate).ignoreExpectOrder(true).build();
//
//        when(externalApiUrlConfig.getBankUserId()).thenReturn(String.valueOf(1));
//        when(userService.getById(1)).thenReturn(admin);
//        when(transactionService.getAllByField("status_id", STATUS_PENDING)).thenReturn(transactions);
//        when(externalApiUrlConfig.getMockBankStatusCheckUrl()).thenReturn("http://localhost:8080/api/mock-bank/check-status");
//        when(restTemplate.getForEntity(eq(url), eq(String.class))).thenReturn(response);
////        doReturn(response).when(createExternalTransferRequestQuery(any(), url));
//        when(statusService.getById(STATUS_COMPLETED)).thenReturn(statusCompleted);
//
//        service.checkPendingReceivedTransactionStatuses();
//
//        Mockito.verify(walletService, Mockito.times(1)).executeCardToWalletTransaction(
//                transaction.getToWallet().getWalletId(), transaction.getTransferValue());
//        Mockito.verify(transactionService, Mockito.times(1)).update(transaction, admin);
//    }
}
