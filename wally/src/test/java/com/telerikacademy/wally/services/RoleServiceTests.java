package com.telerikacademy.wally.services;

import com.telerikacademy.wally.exceptions.DuplicateEntityException;
import com.telerikacademy.wally.exceptions.UnauthorizedOperationException;
import com.telerikacademy.wally.models.Category;
import com.telerikacademy.wally.models.Role;
import com.telerikacademy.wally.models.User;
import com.telerikacademy.wally.repositories.contracts.RoleRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.telerikacademy.wally.helpers.CategoryHelper.createMockCategory;
import static com.telerikacademy.wally.helpers.RoleHelper.createMockRole;
import static com.telerikacademy.wally.helpers.UserHelper.createMockUser;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class RoleServiceTests {

    @Mock
    RoleRepository mockRepository;

    @InjectMocks
    RoleServiceImpl service;

    @Test
    void getAll_should_callRepository() {
        // Arrange
        when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());
        // Act
        service.getAll();
        // Assert
        Mockito.verify(mockRepository, Mockito.times(1)).getAll();
    }

    @Test
    void getAll_should_Authorize_and_CallRepository() {
        // Arrange
        User admin = createMockUser();
        when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());
        // Act
        service.getAll(admin);
        // Assert
        Mockito.verify(mockRepository, Mockito.times(1)).getAll();
    }


    @Test
    public void getById_Should_ReturnRole_When_MatchExists() {
        // Arrange
        Role mockRole = new Role(2, "MockRoleName");
        when(mockRepository.getById(2))
                .thenReturn(mockRole);
        // Act
        Role result = service.getById(2);
        // Assert
        Assertions.assertEquals(2, result.getId());
        Assertions.assertEquals("MockRoleName", result.getName());
        Mockito.verify(mockRepository, Mockito.times(1)).getById(2);
    }

    @Test
    public void getById_Should_Authorize_and_ReturnRole_When_MatchExists() {
        // Arrange
        Role mockRole = new Role(2, "MockRoleName");
        User admin = createMockUser();
        when(mockRepository.getById(2))
                .thenReturn(mockRole);
        // Act
        Role result = service.getById(2, admin);
        // Assert
        Assertions.assertEquals(2, result.getId());
        Assertions.assertEquals("MockRoleName", result.getName());
        Mockito.verify(mockRepository, Mockito.times(1)).getById(2);
    }

    @Test
    public void getById_Should_Throw_when_userNotAdmin() {
        // Arrange
        Role mockRole = new Role(2, "MockRoleName");
        User nonAdmin = createMockUser();
        nonAdmin.setRoleSet(Set.of(mockRole));
        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getById(2, nonAdmin));
    }

    @Test
    public void getAllById_Should_ReturnListRole_When_MatchExists() {
        // Arrange
        Role mockRole1 = new Role(2, "MockRoleName");
        Role mockRole2 = new Role(2, "MockRoleName");
        List<Role> roles = List.of(mockRole2, mockRole1);
        when(mockRepository.getAllById(2))
                .thenReturn(roles);
        // Act
        List<Role> result = service.getAllById(2);
        // Assert
        Assertions.assertEquals(2, result.size());
        Mockito.verify(mockRepository, Mockito.times(1)).getAllById(2);
    }

    @Test
    public void getAllById_Should_Authorize_and_ReturnListRole_When_MatchExists() {
        // Arrange
        Role mockRole1 = new Role(2, "MockRoleName");
        Role mockRole2 = new Role(2, "MockRoleName");
        User admin = createMockUser();
        List<Role> roles = List.of(mockRole2, mockRole1);
        when(mockRepository.getAllById(2))
                .thenReturn(roles);
        // Act
        List<Role> result = service.getAllById(2, admin);
        // Assert
        Assertions.assertEquals(2, result.size());
        Mockito.verify(mockRepository, Mockito.times(1)).getAllById(2);
    }

    @Test
    public void getAllById_Should_Throw_when_userNotAdmin() {
        // Arrange
        Role mockRole = new Role(2, "MockRoleName");
        User nonAdmin = createMockUser();
        nonAdmin.setRoleSet(Set.of(mockRole));

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getAllById(2, nonAdmin));
    }

    @Test
    public void getByField_Should_ReturnRole_When_MatchExists() {
        // Arrange
        Role mockRole = new Role(2, "MockRoleName");
        when(mockRepository.getByField("roleName", "MockRoleName"))
                .thenReturn(mockRole);
        // Act
        Role result = service.getByField("roleName", "MockRoleName");
        // Assert
        Assertions.assertEquals(2, result.getId());
        Assertions.assertEquals("MockRoleName", result.getName());
        Mockito.verify(mockRepository, Mockito.times(1))
                .getByField("roleName", "MockRoleName");
    }

    @Test
    public void getByField_Should_Authorize_and_ReturnRole_When_MatchExists() {
        // Arrange
        Role mockRole = new Role(2, "MockRoleName");
        User admin = createMockUser();
        when(mockRepository.getByField("roleName", "MockRoleName"))
                .thenReturn(mockRole);
        // Act
        Role result = service.getByField("roleName", "MockRoleName", admin);
        // Assert
        Assertions.assertEquals(2, result.getId());
        Assertions.assertEquals("MockRoleName", result.getName());
        Mockito.verify(mockRepository, Mockito.times(1))
                .getByField("roleName", "MockRoleName");
    }

    @Test
    public void getByField_Should_Throw_when_userNotAdmin() {
        // Arrange
        Role mockRole = new Role(2, "MockRoleName");
        User nonAdmin = createMockUser();
        nonAdmin.setRoleSet(Set.of(mockRole));

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getByField("roleName", "MockRoleName", nonAdmin));
    }

    @Test
    public void getAllByField_Should_ReturnListRole_When_MatchExists() {
        // Arrange
        Role mockRole1 = new Role(2, "MockRoleName");
        Role mockRole2 = new Role(2, "MockRoleName");
        List<Role> roles = List.of(mockRole2, mockRole1);
        when(mockRepository.getAllByField("roleName", "MockRoleName"))
                .thenReturn(roles);
        // Act
        List<Role> result = service.getAllByField("roleName", "MockRoleName");
        // Assert
        Assertions.assertEquals(2, result.size());
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAllByField("roleName", "MockRoleName");
    }

    @Test
    public void getAllByField_Should_Authorize_and_ReturnListRole_When_MatchExists() {
        // Arrange
        Role mockRole1 = new Role(2, "MockRoleName");
        Role mockRole2 = new Role(2, "MockRoleName");
        List<Role> roles = List.of(mockRole2, mockRole1);
        User admin = createMockUser();
        when(mockRepository.getAllByField("roleName", "MockRoleName"))
                .thenReturn(roles);
        // Act
        List<Role> result = service.getAllByField("roleName", "MockRoleName", admin);
        // Assert
        Assertions.assertEquals(2, result.size());
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAllByField("roleName", "MockRoleName");
    }

    @Test
    public void getAllByField_Should_Throw_when_userNotAdmin() {
        // Arrange
        Role mockRole = new Role(2, "MockRoleName");
        User nonAdmin = createMockUser();
        nonAdmin.setRoleSet(Set.of(mockRole));

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getAllByField("roleName", "MockRoleName", nonAdmin));
    }

    @Test
    void deleteIfNotUsed_should_CallRepository() {
        Role role = createMockRole();
        User user = createMockUser();

        service.delete(role.getId(), user);

        Mockito.verify(mockRepository, Mockito.times(1)).delete(role.getId());
    }

    @Test
    void update_should_CallRepository() {
        Role role = createMockRole();
        User user = createMockUser();

        service.update(role, user);

        Mockito.verify(mockRepository, Mockito.times(1)).update(role);
    }

    @Test
    void create_should_CallRepository() {
        Role role = createMockRole();
        User user = createMockUser();

        service.create(role, user);

        Mockito.verify(mockRepository, Mockito.times(1)).create(role);
    }

    @Test
    void create_should_Throw_when_RoleExists() {
        Role role = createMockRole();
        User user = createMockUser();
        when(mockRepository.getAll()).thenReturn(List.of(role));

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.create(role, user));
    }

}
