package com.telerikacademy.wally.services;

import com.telerikacademy.wally.models.Transaction;
import com.telerikacademy.wally.models.User;
import com.telerikacademy.wally.models.Wallet;
import com.telerikacademy.wally.models.dtos.TransactionDto;
import com.telerikacademy.wally.models.params.TransactionsFilterParams;
import com.telerikacademy.wally.repositories.contracts.WalletRepository;
import com.telerikacademy.wally.services.contracts.TransactionService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageRequest;

import java.util.List;
import java.util.Optional;

import static com.telerikacademy.wally.helpers.TransactionDtoHelper.createMockTransactionDto;
import static com.telerikacademy.wally.helpers.TransactionHelper.createMockTransaction;
import static com.telerikacademy.wally.helpers.TransactionsFilterParamsHelper.createMockTransactionFilterParams;
import static com.telerikacademy.wally.helpers.UserHelper.createMockUser;
import static com.telerikacademy.wally.helpers.WalletHelper.createMockWallet;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class WalletServiceTests {

    @Mock
    WalletRepository walletRepository;
    @Mock
    TransactionService transactionService;

    @InjectMocks
    WalletServiceImpl service;

    @Test
    public void getLastMonthTransactionsSummary_should_callRepository() {
        int walletId = 1;
        User user = createMockUser();
        TransactionsFilterParams incomingTransactionsFilterParams = createMockTransactionFilterParams();
        TransactionsFilterParams outgoingTransactionsFilterParams = createMockTransactionFilterParams();
        Transaction transaction1 = createMockTransaction();
        Transaction transaction2 = createMockTransaction();
        List<Transaction> transactions = List.of(transaction1, transaction2);

        when(transactionService.searchAndSort(outgoingTransactionsFilterParams)).thenReturn(transactions);
        when(transactionService.searchAndSort(incomingTransactionsFilterParams)).thenReturn(transactions);

        service.getLastMonthTransactionsSummary(Optional.of(walletId), user, incomingTransactionsFilterParams, outgoingTransactionsFilterParams);

    }

//    @Test
//    public void executeWalletToWalletTransaction_Should_CallRepository() {
//        Transaction transaction = createMockTransaction();
//        Wallet payerWallet = transaction.getFromWallet();
//        Wallet payeeWallet = transaction.getToWallet();
//        service.executeCardToWalletTransaction(transaction);
//
//        verify(walletRepository, Mockito.times(1)).update(payeeWallet);
//        verify(walletRepository, Mockito.times(1)).update(payerWallet);
//    }

    @Test
    public void getUserTotalBalance_should_returnTotalBalance() {
        Wallet wallet1 = createMockWallet();
        Wallet wallet2 = createMockWallet();
        List<Wallet> wallets = List.of(wallet1, wallet2);

        service.getUserTotalBalance(wallets);
    }

    @Test
    public void getWalletTotalBalance_should_returnTotalBalance() {
        Wallet wallet1 = createMockWallet();

        service.getWalletTotalBalance(wallet1);
    }

    @Test
    public void executeCardToWalletTransaction_should_callRepository() {
        Wallet userWallet = createMockWallet();
        double transactionValue = 500;
        TransactionDto dto = createMockTransactionDto();

        when(walletRepository.getById(1)).thenReturn(userWallet);

        service.executeCardToWalletTransaction(userWallet,transactionValue, dto.getExchangeRate());

        verify(walletRepository, Mockito.times(1)).update(userWallet);
    }
    @Test
    public void getAllInDept_Should_callRepository(){
        service.getAllInDept();

        verify(walletRepository, Mockito.times(1)).getAllInDept();
    }
    @Test
    public void getPaginated_should_callRepository() {
        service.getPaginated(PageRequest.of(1,5));

        verify(walletRepository, Mockito.times(1)).getAll();
    }
}
