package com.telerikacademy.wally.services;

import com.telerikacademy.wally.exceptions.DuplicateEntityException;
import com.telerikacademy.wally.exceptions.EntityNotFoundException;
import com.telerikacademy.wally.exceptions.UnauthorizedOperationException;
import com.telerikacademy.wally.models.Category;
import com.telerikacademy.wally.models.Role;
import com.telerikacademy.wally.models.User;
import com.telerikacademy.wally.repositories.contracts.UserRepository;
import com.telerikacademy.wally.services.contracts.CategoryService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.mail.javamail.JavaMailSender;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.telerikacademy.wally.helpers.CategoryHelper.createMockCategory;
import static com.telerikacademy.wally.helpers.RoleHelper.createMockRole;
import static com.telerikacademy.wally.helpers.UserHelper.createMockPayeeUser;
import static com.telerikacademy.wally.helpers.UserHelper.createMockUser;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserServiceTests {

    @Mock
    UserRepository userRepository;
    @Mock
    CategoryService categoryService;
    @Mock
    JavaMailSender mailSender;

    @InjectMocks
    UserServiceImpl service;

    @Test
    public void create_verifiesUniqueness_and_CreatesUser() {
        User user = createMockUser();
        User anotherUser = createMockPayeeUser();
        List<User> users = List.of(anotherUser);
        when(userRepository.getByField(any(), any()))
                .thenReturn(user)
                .thenReturn(user)
                .thenReturn(user);
        when(userRepository.getAll())
                .thenReturn(users)
                .thenReturn(users)
                .thenReturn(users);

        service.create(user);

        Mockito.verify(userRepository, Mockito.times(1)).create(user);
    }

    @Test
    public void create_throws_DuplicateEntityException_whenUserExists() {
        User user = createMockUser();
        List<User> users = List.of(user);
        when(userRepository.getByField(any(), any()))
                .thenReturn(user)
                .thenReturn(user)
                .thenReturn(user);
        when(userRepository.getAll())
                .thenReturn(users)
                .thenReturn(users)
                .thenReturn(users);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.create(user));
    }

//    @Test
//    public void sendVerificationEmail_reachesMailSender() throws MessagingException, UnsupportedEncodingException {
//        User user = createMockUser();
//        String url = "some_url";
//        MimeMessage message = Mockito.spy(mailSender.createMimeMessage());
//
//        service.sendVerificationEmail(user, url);
//
//        Mockito.verify(mailSender, Mockito.times(1)).send(message);
//    }

    @Test
    public void update_Should_Call_Repository() {
        User user = createMockPayeeUser();
        User userToAuthenticate = createMockUser();

        service.update(user, userToAuthenticate);
        Mockito.verify(userRepository, Mockito.times(1)).update(user);
    }

    @Test
    public void update_Should_Throw_when_User_notAdmin_or_Customer() {
        User user = createMockPayeeUser();
        User userToAuthenticate = createMockUser();
        Role role = createMockRole();
        role.setName("nonAdmin");
        userToAuthenticate.setRoleSet(Set.of(role));

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.update(user, userToAuthenticate));
    }

    @Test
    public void delete_Should_Call_Repository() {
        User user = createMockPayeeUser();
        User userToAuthenticate = createMockUser();

        service.delete(user.getUserId(), userToAuthenticate);
        Mockito.verify(userRepository, Mockito.times(1)).delete(user.getUserId());
    }

    @Test
    public void delete_Should_Throw_when_User_notAdmin_or_Customer() {
        User user = createMockPayeeUser();
        User userToAuthenticate = createMockUser();
        Role role = createMockRole();
        role.setName("nonAdmin");
        userToAuthenticate.setRoleSet(Set.of(role));

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(user.getUserId(), userToAuthenticate));
    }

    @Test
    public void findPaginated_Should_Throw_when_User_notAdmin_or_Customer() {
        User user = createMockPayeeUser();
        User userToAuthenticate = createMockUser();
        List<User> users = List.of(user, userToAuthenticate);
        Pageable pageable = PageRequest.of(1, 5);
        when(service.getAll(userToAuthenticate)).thenReturn(users);

        service.findPaginated(pageable, userToAuthenticate);

        Mockito.verify(userRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void addOrModifyCategoryToUserCategories_should_update_userCategories() {
        User userToAuthenticate = createMockUser();
        Category category = createMockCategory();

        service.addOrModifyCategoryToUserCategories(category, userToAuthenticate);
        Mockito.verify(userRepository, Mockito.times(1)).update(userToAuthenticate);
    }

    @Test
    public void removeUserCategories_should_update_userCategories() {
        User userToAuthenticate = createMockUser();
        Category category = createMockCategory();

        service.deleteUserCategory(category, userToAuthenticate);
        Mockito.verify(userRepository, Mockito.times(1)).update(userToAuthenticate);
        Mockito.verify(categoryService, Mockito.times(1)).deleteIfNotUsed(category,userToAuthenticate);
    }

    @Test
    public void verify_Should_Not_Update_userEnabled_and_callRepository_when_userEnabled(){
        User userToAuthenticate = createMockUser();
        String verificationCode = "code";

        when(userRepository.findByVerificationCode(verificationCode)).thenReturn(userToAuthenticate);


        service.verify(verificationCode);
        Mockito.verify(userRepository, Mockito.times(0)).update(userToAuthenticate);
    }

    @Test
    public void verify_Should_Update_userEnabled_and_callRepository(){
        User userToAuthenticate = createMockUser();
        userToAuthenticate.setEnabled(false);
        String verificationCode = "code";

        when(userRepository.findByVerificationCode(verificationCode)).thenReturn(userToAuthenticate);

        service.verify(verificationCode);
        Mockito.verify(userRepository, Mockito.times(1)).update(userToAuthenticate);
    }
}
