package com.telerikacademy.wally.services;

import com.telerikacademy.wally.config.ExternalApiUrlConfig;
import com.telerikacademy.wally.models.Transaction;
import com.telerikacademy.wally.models.User;
import com.telerikacademy.wally.models.dtos.TransactionFilterSortDto;
import com.telerikacademy.wally.models.params.TransactionsFilterParams;
import com.telerikacademy.wally.repositories.contracts.TransactionRepository;
import com.telerikacademy.wally.services.contracts.StatusService;
import com.telerikacademy.wally.services.contracts.WalletService;
import com.telerikacademy.wally.utils.mappers.TransactionMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.telerikacademy.wally.helpers.TransactionHelper.createMockTransaction;
import static com.telerikacademy.wally.helpers.UserHelper.createMockUser;
import static com.telerikacademy.wally.models.enums.TransactionSortOptions.DATE_DESC;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TransactionServiceTests {

    @Mock
    TransactionRepository mockRepository;
    @Mock
    ExternalApiUrlConfig externalApiUrlConfig;
    @Mock
    StatusService statusService;
    @Mock
    TransactionMapper transactionMapper;
    @Mock
    WalletService walletService;

    @InjectMocks
    TransactionServiceImpl service;

    @Test
    void searchAndSort_should_callRepository() {
        // Arrange
        TransactionsFilterParams transactionsFilterParams = new TransactionsFilterParams();
        when(mockRepository.searchAndSort(transactionsFilterParams))
                .thenReturn(new ArrayList<>());
        // Act
        List<Transaction> result = service.searchAndSort(transactionsFilterParams);
        // Assert
        Mockito.verify(mockRepository, Mockito.times(1)).searchAndSort(transactionsFilterParams);
    }

    @Test
    void findPaginated_withTransactionParams_should_callRepository() {
        // Arrange
        Pageable pageable = PageRequest.of(2, 10);
        User user = createMockUser();
        TransactionsFilterParams transactionsFilterParams = new TransactionsFilterParams();
        when(mockRepository.searchAndSort(transactionsFilterParams))
                .thenReturn(new ArrayList<>());
        // Act
        Page<Transaction> result = service.findPaginated(pageable, transactionsFilterParams, user);
        // Assert
        Mockito.verify(mockRepository, Mockito.times(1)).searchAndSort(transactionsFilterParams);
    }

    @Test
    void findPaginated_should_callRepository() {
        // Arrange
        Pageable pageable = PageRequest.of(2, 10);
        User user = createMockUser();
        TransactionsFilterParams transactionsFilterParams = new TransactionsFilterParams();
        when(service.getAll())
                .thenReturn(new ArrayList<>());
        // Act
        Page<Transaction> result = service.findPaginated(pageable, user);
        // Assert
        Mockito.verify(mockRepository, Mockito.times(1)).getAll();
    }

    @Test
    void create_should_callRepository() {
        // Arrange
        Transaction transaction = createMockTransaction();
        // Act
        service.create(transaction);
        // Assert
        Mockito.verify(mockRepository, Mockito.times(1)).create(transaction);
    }

    @Test
    void loadUserPastTransactions_should_callRepository() {
        // Arrange
        int page = 1;
        int size = 10;
        User user = createMockUser();
        Optional<Integer> walletId = Optional.of(1);
        TransactionsFilterParams transactionsFilterParams = new TransactionsFilterParams();
        TransactionFilterSortDto dto = new TransactionFilterSortDto();
        Transaction transaction1 = createMockTransaction();
        Transaction transaction2 = createMockTransaction();
        List<Transaction> transactions =List.of(transaction1, transaction2);

        when(transactionMapper.mapIncomingUserPaymentsToFilterParams(user, walletId, DATE_DESC))
                .thenReturn(transactionsFilterParams)
                .thenReturn(transactionsFilterParams);
        Mockito.lenient().when(mockRepository.searchAndSort(transactionsFilterParams))
                .thenReturn(transactions)
                .thenReturn(transactions);

        // Act
        Page<Transaction> result = service.loadUserPastTransactions(page,size,user,walletId);
        // Assert
        Mockito.verify(mockRepository, Mockito.times(1)).searchAndSort(transactionsFilterParams);
    }


}
