package com.telerikacademy.wally.services;

import com.telerikacademy.wally.models.Card;
import com.telerikacademy.wally.models.Wallet;
import com.telerikacademy.wally.repositories.contracts.CurrencyRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.telerikacademy.wally.helpers.CardHelper.createMockCard;
import static com.telerikacademy.wally.helpers.WalletHelper.createMockWallet;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class CurrencyServiceTests {

    @Mock
    CurrencyRepository currencyRepository;

    @InjectMocks
    CurrencyServiceImpl service;

//    @Test
//    public void calculateExchangeRate_Should_calculateRate() {
//        Wallet wallet1 = createMockWallet();
//        Wallet wallet2 = createMockWallet();
//
//        verify(service.calculateExchangeRate(wallet1,wallet2)).doubleValue();
//    }
//
//    @Test
//    public void calculateExchangeRate_Should_calculateRate_with_Card_and_Wallet() {
//        Card payeeCard = createMockCard();
//        Wallet wallet2 = createMockWallet();
//
//        verify(service.calculateExchangeRate(payeeCard,wallet2)).doubleValue();
//    }
}
