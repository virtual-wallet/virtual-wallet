package com.telerikacademy.wally.services;

import com.telerikacademy.wally.config.ExternalApiUrlConfig;
import com.telerikacademy.wally.models.SavingsRecord;
import com.telerikacademy.wally.services.contracts.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static com.telerikacademy.wally.helpers.SavingsRecordHelper.createMockSavingsRecord;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class SavingsManagerTests {

    @Mock SavingsFeatureService savingsFeatureService;
    @Mock SavingsRecordService savingsRecordService;
    @Mock WalletService walletService;
    @Mock ExternalApiUrlConfig externalApiUrlConfig;
    @Mock TransactionService transactionService;
    @Mock PaymentManager paymentManager;

    @InjectMocks
    SavingsManagerImpl service;

//    @Test
//    public void paybackCustomerDeposits_should_callRepository(){
//        SavingsRecord savingsRecord1= createMockSavingsRecord();
//        SavingsRecord savingsRecord2 = createMockSavingsRecord();
//        List<SavingsRecord> recordList = List.of(savingsRecord1,savingsRecord2);
//
//        when(savingsRecordService.getAll()).thenReturn(recordList);
//
//        service.paybackCustomerDeposits();
//
//        Mockito.verify(walletService, Mockito.times(2))
//                .update(savingsRecord1.getWallet(), savingsRecord1.getWallet().getOwner());
//    }

}
