package com.telerikacademy.wally.services;

import com.telerikacademy.wally.exceptions.DuplicateEntityException;
import com.telerikacademy.wally.exceptions.InvalidDataInput;
import com.telerikacademy.wally.exceptions.UnauthorizedOperationException;
import com.telerikacademy.wally.models.Card;
import com.telerikacademy.wally.models.Role;
import com.telerikacademy.wally.models.User;
import com.telerikacademy.wally.repositories.contracts.CardRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import static com.telerikacademy.wally.helpers.CardHelper.createMockCard;
import static com.telerikacademy.wally.helpers.RoleHelper.createMockRole;
import static com.telerikacademy.wally.helpers.UserHelper.createMockPayeeUser;
import static com.telerikacademy.wally.helpers.UserHelper.createMockUser;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CardServiceHelpers {

    @Mock
    CardRepository repository;

    @InjectMocks
    CardServiceImpl service;

    @Test
    public void update_Should_Call_Repository() {
        User userToAuthenticate = createMockUser();
        Card card = createMockCard();

        service.update(card, userToAuthenticate);
        Mockito.verify(repository, Mockito.times(1)).update(card);
    }

    @Test
    public void update_Should_Throw_when_User_notAdmin_or_Customer() {
        User user = createMockPayeeUser();
        User userToAuthenticate = createMockUser();
        Role role = createMockRole();
        role.setName("nonAdmin");
        userToAuthenticate.setRoleSet(Set.of(role));
        Card card = createMockCard();
        card.setUser(user);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.update(card, userToAuthenticate));
    }

    @Test
    public void delete_Should_Call_Repository() {
        User userToAuthenticate = createMockUser();
        Card card = createMockCard();

        service.delete(card.getId(), userToAuthenticate.getUserId(),userToAuthenticate);
            Mockito.verify(repository, Mockito.times(1)).delete(card.getId());
    }

    @Test
    public void delete_Should_Throw_when_User_notAdmin_or_Customer() {
        User user = createMockPayeeUser();
        User userToAuthenticate = createMockUser();
        Role role = createMockRole();
        role.setName("nonAdmin");
        userToAuthenticate.setRoleSet(Set.of(role));
        user.setRoleSet(Set.of(role));
        Card card = createMockCard();
        card.setUser(user);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(card.getId(),userToAuthenticate.getUserId(), user));
    }

    @Test
    public void getAllById_Should_Call_Repository() {
        User userToAuthenticate = createMockUser();
        Card card = createMockCard();

        service.getAllById(card.getId(), userToAuthenticate);
        Mockito.verify(repository, Mockito.times(1)).getAllById(card.getId());
    }

    @Test
    public void getAllById_Should_Throw_when_User_notAdmin_or_Customer() {
        User user = createMockPayeeUser();
        User userToAuthenticate = createMockUser();
        Role role = createMockRole();
        role.setName("nonAdmin");
        userToAuthenticate.setRoleSet(Set.of(role));
        user.setRoleSet(Set.of(role));
        Card card = createMockCard();
        card.setUser(user);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getAllById(user.getUserId(), userToAuthenticate));
    }

    @Test
    public void getById_Should_Call_Repository() {
        User userToAuthenticate = createMockUser();
        User user = createMockPayeeUser();
        Card card = createMockCard();

        service.getById(card.getId(),user.getUserId(), userToAuthenticate);
        Mockito.verify(repository, Mockito.times(1)).getById(card.getId());
    }

    @Test
    public void getById_Should_Throw_when_User_notAdmin_or_Customer() {
        User user = createMockPayeeUser();
        User userToAuthenticate = createMockUser();
        Role role = createMockRole();
        role.setName("nonAdmin");
        userToAuthenticate.setRoleSet(Set.of(role));
        user.setRoleSet(Set.of(role));
        Card card = createMockCard();
        card.setUser(user);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getById(user.getUserId(),user.getUserId(), userToAuthenticate));
    }

    @Test
    public void getAllByField_Should_Call_Repository() {
        User userToAuthenticate = createMockUser();
        User user = createMockPayeeUser();
        Card card = createMockCard();
        String fieldName = "fieldName";

        service.getAllByField(fieldName,card.getId(), user.getUserId(),userToAuthenticate);
        Mockito.verify(repository, Mockito.times(1)).getAllByField(fieldName, card.getId());
    }

    @Test
    public void getAllByField_Should_Throw_when_User_notAdmin_or_Customer() {
        User user = createMockPayeeUser();
        String fieldName = "fieldName";
        User userToAuthenticate = createMockUser();
        Role role = createMockRole();
        role.setName("nonAdmin");
        userToAuthenticate.setRoleSet(Set.of(role));
        user.setRoleSet(Set.of(role));
        Card card = createMockCard();
        card.setUser(user);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getAllByField(fieldName,card.getId(), user.getUserId(),userToAuthenticate));
    }

    @Test
    public void getByField_Should_Call_Repository() {
        User userToAuthenticate = createMockUser();
        User user = createMockPayeeUser();
        Card card = createMockCard();
        String fieldName = "fieldName";

        service.getByField(fieldName,card.getId(),user.getUserId(), userToAuthenticate);
        Mockito.verify(repository, Mockito.times(1)).getByField(fieldName,card.getId());
    }

    @Test
    public void getByField_Should_Throw_when_User_notAdmin_or_Customer() {
        User user = createMockPayeeUser();
        User userToAuthenticate = createMockUser();
        Role role = createMockRole();
        role.setName("nonAdmin");
        userToAuthenticate.setRoleSet(Set.of(role));
        user.setRoleSet(Set.of(role));
        Card card = createMockCard();
        card.setUser(user);
        String fieldName = "fieldName";

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getByField(fieldName,card.getId(),user.getUserId(), userToAuthenticate));
    }

    @Test
    public void create_verifiesUniqueness_and_CreatesCard() {
        User user = createMockUser();
        Card card = createMockCard();
        Card anotherCard= createMockCard();
        anotherCard.setCardNumber("123");
        List<Card> cards = List.of(anotherCard);
        when(repository.getAll())
                .thenReturn(cards);

        service.create(card,user);

        Mockito.verify(repository, Mockito.times(1)).create(card);
    }

    @Test
    public void create_throws_whenCard_Exists() {
        User user = createMockUser();
        Card card = createMockCard();
        List<Card> cards = List.of(card);
        when(repository.getAll())
                .thenReturn(cards);
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.create(card,user));
    }

    @Test
    public void create_throws_whenCard_Expired() {
        User user = createMockUser();
        Card card = createMockCard();
        LocalDate pastDate = LocalDate.now().minusMonths(1);
        card.setExpireDate(java.sql.Date.valueOf(pastDate));
        Card anotherCard= createMockCard();
        anotherCard.setCardNumber("123");
        List<Card> cards = List.of(anotherCard);
        when(repository.getAll())
                .thenReturn(cards);

        Assertions.assertThrows(InvalidDataInput.class,
                () -> service.create(card,user));
    }

}
