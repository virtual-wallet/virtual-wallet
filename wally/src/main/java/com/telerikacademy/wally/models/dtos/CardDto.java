package com.telerikacademy.wally.models.dtos;


import javax.validation.constraints.*;

public class CardDto extends UpdateCardDto {

    @PositiveOrZero(message = "userId should be positive or zero")
    private int userId;

    public CardDto() {
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
