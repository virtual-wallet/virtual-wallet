package com.telerikacademy.wally.models.dtos;

import javax.validation.constraints.PositiveOrZero;

public class PositiveValueDto {
    @PositiveOrZero(message = "Transaction value should be positive.")
    private double value;

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
