package com.telerikacademy.wally.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "overdraft_records")
public class OverdraftRecord {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "wallet_id")
    private Wallet wallet;

    @ManyToOne
    @JoinColumn(name = "overdraft_id")
    private OverdraftFeature overdraftFeature;

    @Column(name = "mth_unpaid")
    private int unpaidMonths;

    @Column(name = "date_started")
    @JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING)
    private Date dateStarted;

    @Column(name = "is_paid")
    private boolean isPaid;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        OverdraftRecord overdraftRecord = (OverdraftRecord) o;
        return this.getId() == (overdraftRecord.getId()) &&
                this.wallet.equals(overdraftRecord.getWallet()) &&
                this.overdraftFeature.equals(overdraftRecord.getOverdraftFeature()) &&
                this.dateStarted.equals(overdraftRecord.getDateStarted()) &&
                this.unpaidMonths == overdraftRecord.getUnpaidMonths() &&
                this.isPaid == overdraftRecord.isPaid();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id,
                wallet,
                overdraftFeature,
                dateStarted,
                unpaidMonths,
                isPaid);
    }
}
