package com.telerikacademy.wally.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "wallets")
public class Wallet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int walletId;

    @OneToOne
    @JoinColumn(name = "owner_id")
    private User owner;

    @OneToOne
    @JoinColumn(name = "currency_id")
    private Currency currency;

    @Column(name = "balance")
    private double balance;

    @Column(name = "has_overdraft")
    private boolean hasOverdraft;

//    @ManyToMany(fetch = FetchType.EAGER)
//    @JoinTable(name = "wallets_cards",
//            joinColumns = {@JoinColumn(name = "wallet_id")},
//            inverseJoinColumns ={ @JoinColumn(name = "card_id")}
//    )
//    private Set<Card> cardSet;

    public boolean hasOverdraftEnabled() {
        return hasOverdraft;
    }

    public void setHasOverdraftEnabled(boolean hasOverdraft) {
        this.hasOverdraft = hasOverdraft;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        Wallet wallet = (Wallet) o;
        return this.getWalletId() == (wallet.getWalletId()) &&
                this.getOwner().equals(wallet.getOwner()) &&
                this.getCurrency().equals(wallet.getCurrency()) &&
                this.getBalance() == (wallet.getBalance()) &&
                this.hasOverdraftEnabled() == wallet.hasOverdraftEnabled();
    }

    @Override
    public int hashCode() {
        return Objects.hash(walletId,
                owner,
                currency,
                balance,
                hasOverdraft);
    }
}
