package com.telerikacademy.wally.models;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "transactions")
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int transactionId;

    @ManyToOne
    @JoinColumn(name = "from_user_id")
    private User fromUser;

    @ManyToOne
    @JoinColumn(name = "from_wallet_id")
    @JsonManagedReference
    private Wallet fromWallet;

    @ManyToOne
    @JoinColumn(name = "to_wallet_id")
    @JsonManagedReference
    private Wallet toWallet;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @ManyToOne
    @JoinColumn(name = "status_id")
    private Status status;

    @Column(name = "transfer_value")
    private double transferValue;

    @Column(name = "exchange_rate")
    private double exchangeRate;

    @Column(name = "transaction_time")
    private Date transactionTime;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        Transaction transaction = (Transaction) o;
        return this.getTransactionId() == (transaction.getTransactionId()) &&
                this.fromUser.equals(transaction.fromUser) &&
                this.fromWallet.equals(transaction.fromWallet) &&
                this.toWallet.equals(transaction.toWallet) &&
                this.status.equals(transaction.getStatus()) &&
                this.transferValue == transaction.getTransferValue() &&
                this.transactionTime.equals(transaction.getTransactionTime()) &&
                this.exchangeRate ==  transaction.getExchangeRate();
    }

    @Override
    public int hashCode() {
        return Objects.hash(transactionId,
                fromUser,
                toWallet,
                fromWallet,
                transferValue,
                transactionTime,
                exchangeRate,
                status);
    }
}
