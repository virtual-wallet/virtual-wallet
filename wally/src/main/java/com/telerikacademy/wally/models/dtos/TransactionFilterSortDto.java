package com.telerikacademy.wally.models.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.ArrayList;

public class TransactionFilterSortDto {
    private String recipientUsername;
    private String senderUsername;
    private String isCredited;
    private String sort;
    private String category;
    private String status;
    private String fromWalletId;
    private String toWalletId;

    @JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING)
    private String dateFrom;

    @JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING)
    private String dateTo;


    public String getRecipientUsername() {
        return recipientUsername;
    }

    public void setRecipientUsername(String recipientUsername) {
        this.recipientUsername = recipientUsername;
    }

    public String getSenderUsername() {
        return senderUsername;
    }

    public void setSenderUsername(String senderUsername) {
        this.senderUsername = senderUsername;
    }

    public String isCredited() {
        return isCredited;
    }

    public void setCredited(String credited) {
        isCredited = credited;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getIsCredited() {
        return isCredited;
    }

    public void setIsCredited(String isCredited) {
        this.isCredited = isCredited;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFromWalletId() {
        return fromWalletId;
    }

    public void setFromWalletId(String fromWalletId) {
        this.fromWalletId = fromWalletId;
    }

    public String getToWalletId() {
        return toWalletId;
    }

    public void setToWalletId(String toWalletId) {
        this.toWalletId = toWalletId;
    }

    public String toQueryString(){
        StringBuilder query = new StringBuilder();
        var filters = new ArrayList<String>();
        if (recipientUsername!= null && !recipientUsername.equals("-1")){

            filters.add("recipient-username=" + recipientUsername);
        }

        if (senderUsername!= null && !senderUsername.equals("-1")){
            filters.add("sender-username="+senderUsername);
        }

        if (isCredited!= null && !isCredited.equals("-1")){
            filters.add("is-credited="+isCredited);
        }

        if (sort!= null){
            filters.add("sort="+sort);
        }

        if (fromWalletId != null && !fromWalletId.equals("")){
            filters.add("form-wallet="+ fromWalletId);
        }

        if (toWalletId != null && !toWalletId.equals("")){
            filters.add("to-wallet="+ toWalletId);
        }

        if (status!= null && !status.equals("-1")){
            filters.add("status="+status);
        }

        if (category!= null && !category.equals("-1")){
            filters.add("category="+category);
        }

        if (dateFrom!= null && !dateFrom.equals("")){
            filters.add("date-from="+dateFrom);
        }

        if (dateTo!= null && !dateTo.equals("")){
            filters.add("date-to="+dateTo);
        }

        if (filters.size()!=0){
            query.append("?").append(String.join("&", filters));
        }

        return query.toString();
    }

}