package com.telerikacademy.wally.models.dtos;

import javax.validation.constraints.NotEmpty;

public class RegisterDto extends UserDto {

    @NotEmpty(message = "Password confirmation cannot be empty")
    private String passwordConfirm;

    private String photo;

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
