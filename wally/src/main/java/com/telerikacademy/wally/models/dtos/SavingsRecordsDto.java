package com.telerikacademy.wally.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class SavingsRecordsDto extends SavingsSetUpDto {

    @NotNull(message = "AER has to be not null.")
    private double aer;

    @Positive(message = "Duration months have to be positive.")
    private int durationMonths;

    public SavingsRecordsDto(int walletId,
                             int savingsTypeID,
                             double aer,
                             int currencyId,
                             double baseAmount,
                             int durationMonths) {
        super(walletId, savingsTypeID, baseAmount, currencyId);
        this.aer = aer;
        this.durationMonths = durationMonths;
    }

    public SavingsRecordsDto() {
    }

    public double getAer() {
        return aer;
    }

    public void setAer(double aer) {
        this.aer = aer;
    }

    public int getDurationMonths() {
        return durationMonths;
    }

    public void setDurationMonths(int durationMonths) {
        this.durationMonths = durationMonths;
    }

}
