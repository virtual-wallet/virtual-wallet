package com.telerikacademy.wally.models.dtos;

import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

public class TransactionDto {

    @PositiveOrZero(message = "senderUserId should be positive or zero")
    private int fromUserId;

    @PositiveOrZero(message = "senderWalletId should be positive or zero")
    private int fromWalletId;

    @PositiveOrZero(message = "recipientWalletId should be positive or zero")
    private int toWalletId;

    @PositiveOrZero(message = "categoryId should be positive or zero")
    private int categoryId;

    @Positive(message = "Transfer amount cannot be zero.")
    private double value;

    private int statusId;

    private double exchangeRate;

    public TransactionDto(int fromUserId, int fromWalletId, int toWalletId, int categoryId, double value, int statusId, double exchangeRate) {
        setStatusId(statusId);
        setExchangeRate(exchangeRate);
        setFromUserId(fromUserId);
        setFromWalletId(fromWalletId);
        setToWalletId(toWalletId);
        setCategoryId(categoryId);
        setValue(value);
    }

    public TransactionDto() {
    }

    public int getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(int fromUserId) {
        this.fromUserId = fromUserId;
    }

    public int getFromWalletId() {
        return fromWalletId;
    }

    public void setFromWalletId(int fromWalletId) {
        this.fromWalletId = fromWalletId;
    }

    public int getToWalletId() {
        return toWalletId;
    }

    public void setToWalletId(int toWalletId) {
        this.toWalletId = toWalletId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public double getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(double exchangeRate) {
        this.exchangeRate = exchangeRate;
    }
}
