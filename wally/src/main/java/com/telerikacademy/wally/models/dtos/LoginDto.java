package com.telerikacademy.wally.models.dtos;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class LoginDto {
    @NotEmpty(message = "Username cannot be empty.")
    @Size(min = 2, max = 20, message = "Username should be between 2 and 20 characters.")
    private String username;

    @NotEmpty(message = "Password cannot be empty.")
    @Size(min = 8, max = 20, message = "Password should be between 8 and 20 characters.")
    @Pattern(regexp = "^(?=.*?[A-Z])(?=(.*[\\d]){1,})(?=(.*[\\W]){1,})(?!.*\\s).{8,}$",
    message = "Password should contain at least 1 upper case,1 special symbol and 1 digit")
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}


