package com.telerikacademy.wally.models.dtos;

import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.*;

public class MockBankDto {

    @NotEmpty(message = "Card number cannot be empty.")
    @Size(min = 16, max = 16, message = "Card number should be 16 digits")
    private String cardNumber;

    @NotEmpty(message = "Date cannot be empty.")
    private String expireDate;

    @NotEmpty(message = "CVV cannot be empty.")
    @Size(min = 3, max = 3, message = "CVV has to be 3 digits only.")
    private String cvv;

    @PositiveOrZero(message = "Transaction value cannot be empty or negative.")
    private double value;

    private int walletId;

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public int getWalletId() {
        return walletId;
    }

    public void setWalletId(int walletId) {
        this.walletId = walletId;
    }
}