package com.telerikacademy.wally.models.params.contracts;

import com.telerikacademy.wally.models.enums.TransactionSortOptions;
import com.telerikacademy.wally.models.enums.contracts.SortOptions;

import java.util.Optional;

public interface ModelParameters {

    Optional<SortOptions> getSortOptions();

    void setSortOptions(SortOptions sortOptions);
}
