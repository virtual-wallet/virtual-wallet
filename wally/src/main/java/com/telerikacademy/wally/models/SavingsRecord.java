package com.telerikacademy.wally.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "savings_records")
public class SavingsRecord {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "wallet_id")
    private Wallet wallet;

    @ManyToOne
    @JoinColumn(name = "savings_type_id")
    private SavingsFeature savingsFeature;

    @ManyToOne
    @JoinColumn(name = "currency_id")
    private Currency currency;

    @Column(name = "aer")
    private double aer;

    @Column(name = "base_amount")
    private double baseAmount;

    @Column(name = "duration_mth")
    private int durationMonths;

    @Column(name = "start_date")
    @JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING)
    private Date startDate;

    @Column(name = "end_date")
    @JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING)
    private Date endDate;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        SavingsRecord savingsRecord = (SavingsRecord) o;
        return this.getId() == (savingsRecord.getId()) &&
                this.savingsFeature.equals(savingsRecord.getSavingsFeature()) &&
                this.currency == (savingsRecord.getCurrency()) &&
                this.aer == savingsRecord.getAer() &&
                this.baseAmount == savingsRecord.getBaseAmount() &&
                this.durationMonths == savingsRecord.getDurationMonths() &&
                this.startDate.equals(savingsRecord.getStartDate()) &&
                this.endDate.equals(savingsRecord.getEndDate()) &&
                this.wallet.equals(savingsRecord.getWallet());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id,
                savingsFeature,
                currency,
                aer,
                baseAmount,
                durationMonths,
                startDate,
                endDate,
                wallet);
    }
}
