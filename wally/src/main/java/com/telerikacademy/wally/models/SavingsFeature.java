package com.telerikacademy.wally.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "savings_features")
public class SavingsFeature {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "duration_mth")
    private int duration;

    @Column(name = "aer")
    private double aer;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        SavingsFeature savingsFeature = (SavingsFeature) o;
        return this.getId() == (savingsFeature.getId()) &&
                this.name.equals(savingsFeature.getName()) &&
                this.duration == (savingsFeature.getDuration()) &&
                this.aer == savingsFeature.getAer();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id,
                name,
                aer,
                duration);
    }
}
