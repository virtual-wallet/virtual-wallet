package com.telerikacademy.wally.models.enums;

public enum TransactionStatus {

    APPROVED("Approved"),
    PENDING("Pending"),
    DECLINED("Declined");

    private final String preview;

    TransactionStatus(String preview) {
        this.preview = preview;
    }

    public String getPreview() {
        return preview;
    }
}
