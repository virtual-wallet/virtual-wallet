package com.telerikacademy.wally.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name="currencies")
public class Currency {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String currencyName;

    @Column(name = "iso")
    private String iso;

    @Column(name = "symbol")
    private String symbol;

    @Column(name = "exchange_rate")
    private double exchangeRate;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        Currency currency = (Currency) o;
        return this.getId()==(currency.getId()) &&
                this.getCurrencyName().equals(currency.getCurrencyName()) &&
                this.getIso().equals(currency.getIso()) &&
                this.getSymbol().equals(currency.getSymbol()) &&
                this.getExchangeRate() == currency.getExchangeRate();
    }

    @Override
    public int hashCode(){
        return Objects.hash(id,
                iso,
                currencyName,
                symbol,
                exchangeRate);
    }
}
