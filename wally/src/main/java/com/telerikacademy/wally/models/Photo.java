package com.telerikacademy.wally.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "photos")
public class Photo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "photo_id")
    private int id;

    @Column(name = "photo")
    private String photo;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        Photo photo = (Photo) o;
        return this.getId() == (photo.getId()) &&
                this.getPhoto().equalsIgnoreCase(photo.getPhoto()) &&
                this.getUser() == (photo.getUser());

    }

    @Override
    public int hashCode() {
        return Objects.hash(photo,
                user,
                id);
    }
}
