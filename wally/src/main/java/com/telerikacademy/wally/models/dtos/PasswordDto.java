package com.telerikacademy.wally.models.dtos;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class PasswordDto {
    @NotEmpty(message = "Password cannot be empty.")
    @Size(min = 8, max = 20, message = "Password should be between 8 and 20 characters.")
    @Pattern(regexp = "^(?=.*?[A-Z])(?=(.*[\\d]){1,})(?=(.*[\\W]){1,})(?!.*\\s).{8,}$",
            message = "Password should contain at least 1 upper case,1 special symbol and 1 digit")
    private String password;

    @NotEmpty(message = "Password cannot be empty.")
    @Size(min = 8, max = 20, message = "Password should be between 8 and 20 characters.")
    private String passwordConfirm;

    public PasswordDto(){

    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
