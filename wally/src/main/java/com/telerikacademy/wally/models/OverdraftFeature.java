package com.telerikacademy.wally.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "overdraft_features")
public class OverdraftFeature {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "max_mth")
    private int maxMonthsWithoutPayment;

    @Column(name = "apr")
    private double apr;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        OverdraftFeature overdraftFeature = (OverdraftFeature) o;
        return this.getId() == (overdraftFeature.getId()) &&
                this.name.equals(overdraftFeature.getName()) &&
                this.maxMonthsWithoutPayment == (overdraftFeature.getMaxMonthsWithoutPayment()) &&
                this.apr == overdraftFeature.getApr();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, maxMonthsWithoutPayment, apr);
    }
}
