package com.telerikacademy.wally.models.dtos;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

public class CurrencyDto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    @NotNull(message = "Currency name cannot be empty.")
    @Size(min = 2, max = 50, message = "Currency name should be between 2 and 50 characters.")
    private String currencyName;

    @Column(name = "iso")
    @NotNull(message = "Iso cannot be empty.")
    @Size(min = 2, max = 5, message = "Iso should be between 2 and 5 characters.")
    private String iso;

    @Column(name = "symbol")
    @NotNull(message = "Symbol cannot be empty.")
    @Size(min = 1, max = 5, message = "Symbol should be between 2 and 5 characters.")
    private String symbol;

    @Column(name = "exchange_rate")
    @PositiveOrZero(message = "Exchange rate has to be positive.")
    private double exchangeRate;

    public CurrencyDto(int id, String currencyName, String iso, String symbol, double exchangeRate) {
        this.id = id;
        this.currencyName = currencyName;
        this.iso = iso;
        this.symbol = symbol;
        this.exchangeRate = exchangeRate;
    }

    public CurrencyDto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public double getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(double exchangeRate) {
        this.exchangeRate = exchangeRate;
    }
}
