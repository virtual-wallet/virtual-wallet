package com.telerikacademy.wally.models.params;

import com.telerikacademy.wally.models.enums.UserSortOptions;
import com.telerikacademy.wally.models.enums.contracts.SortOptions;
import com.telerikacademy.wally.models.params.contracts.ModelParameters;

import java.util.Optional;

public class UserSearchParams implements ModelParameters {
    private Optional<String> username;
    private Optional<String> email;
    private Optional<String> phone;
    private Optional<String> all;
    private Optional<SortOptions> sortOptions;

    public UserSearchParams() {
    }

    public UserSearchParams(Optional<String> username,
                            Optional<String> email,
                            Optional<String> phone,
                            Optional<String> all,
                            Optional<SortOptions> sortOptions) {
        this.username = username;
        this.email = email;
        this.phone = phone;
        this.all= all;
        this.sortOptions = sortOptions;
    }

    public Optional<String> getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = Optional.ofNullable(username);
    }

    public Optional<String> getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = Optional.ofNullable(email);
    }

    public Optional<String> getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = Optional.ofNullable(phone);
    }

    public Optional<String> getAll() {
        return all;
    }

    public void setAll(String all) {
        this.all = Optional.ofNullable(all);
    }

    public Optional<SortOptions> getSortOptions() {
        return sortOptions;
    }

    public void setSortOptions(SortOptions sortOptions) {
        this.sortOptions = Optional.ofNullable(sortOptions);
    }
}
