package com.telerikacademy.wally.models.enums;

import com.telerikacademy.wally.models.enums.contracts.SortOptions;

import java.util.HashMap;
import java.util.Map;

public enum TransactionSortOptions implements SortOptions {
    TRANSFER_VALUE_ASC("Transfer value, ascending", "order by transferValue asc"),
    TRANSFER_VALUE_DESC("Transfer value, descending", "order by transferValue desc"),
    DATE_ASC("Date, ascending", "order by transactionTime asc"),
    DATE_DESC("Date, descending", "order by transactionTime desc");

    private final String preview;
    private final String query;
    public static final Map<String, TransactionSortOptions> BY_PREVIEW = new HashMap<>();

    static {
        for (TransactionSortOptions e : values()) {
            BY_PREVIEW.put(e.preview, e);
        }
    }

    TransactionSortOptions(String preview, String query) {
        this.preview = preview;
        this.query = query;
    }

    public static TransactionSortOptions valueOfPreview(String preview) {
        return BY_PREVIEW.get(preview);
    }

    public String getPreview() {
        return preview;
    }

    public String getQuery() {
        return query;
    }
}
