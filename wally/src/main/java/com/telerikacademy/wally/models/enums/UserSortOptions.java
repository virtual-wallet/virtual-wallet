package com.telerikacademy.wally.models.enums;

import com.telerikacademy.wally.models.enums.contracts.SortOptions;

import java.util.HashMap;
import java.util.Map;

public enum UserSortOptions implements SortOptions {
    USERNAME_ASC("Username, ascending", "order by username asc"),
    USERNAME_DESC("Username, descending", "order by username desc"),
    EMAIL_ASC("Email, ascending", "order by email asc"),
    EMAIL_DESC("Email, descending", "order by email desc");

    private final String preview;
    private final String query;
    public static final Map<String, UserSortOptions> BY_PREVIEW = new HashMap<>();

    static {
        for (UserSortOptions e : values()) {
            BY_PREVIEW.put(e.preview, e);
        }
    }

    UserSortOptions(String preview, String query) {
        this.preview = preview;
        this.query = query;
    }

    public static UserSortOptions valueOfPreview(String preview) {
        return BY_PREVIEW.get(preview);
    }

    public String getPreview() {
        return preview;
    }

    public String getQuery() {
        return query;
    }
}
