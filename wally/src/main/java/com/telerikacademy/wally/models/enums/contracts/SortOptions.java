package com.telerikacademy.wally.models.enums.contracts;

import com.telerikacademy.wally.models.enums.TransactionSortOptions;

public interface SortOptions {

    String getPreview();

    String getQuery();
}
