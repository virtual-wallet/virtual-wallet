package com.telerikacademy.wally.models.dtos;

import jdk.jfr.BooleanFlag;
import javax.validation.constraints.Positive;
import java.util.Date;
import java.util.Optional;

public class OverdraftRecordDto {

    @Positive(message = "Wallet ID has to be positive.")
    private int walletId;

    @Positive(message = "Overdraft type ID has to be positive.")
    private int overdraftFeatureId;

    @Positive(message = "Unpaid months have to be positive.")
    private int unpaidMonths;

    private String dateStarted;

    @BooleanFlag
    private boolean isPaid;

    public OverdraftRecordDto(int walletId,
                              int overdraftFeatureId,
                              int unpaidMonths,
                              String dateStarted,
                              boolean isPaid) {
        this.walletId = walletId;
        this.overdraftFeatureId = overdraftFeatureId;
        this.unpaidMonths = unpaidMonths;
        this.dateStarted = dateStarted;
        this.isPaid = isPaid;
    }

    public OverdraftRecordDto() {
    }

    public int getWalletId() {
        return walletId;
    }

    public void setWalletId(int walletId) {
        this.walletId = walletId;
    }

    public int getOverdraftFeatureId() {
        return overdraftFeatureId;
    }

    public void setOverdraftFeatureId(int overdraftFeatureId) {
        this.overdraftFeatureId = overdraftFeatureId;
    }

    public int getUnpaidMonths() {
        return unpaidMonths;
    }

    public void setUnpaidMonths(int unpaidMonths) {
        this.unpaidMonths = unpaidMonths;
    }

    public String getDateStarted() {
        return dateStarted;
    }

    public void setDateStarted(String dateStarted) {
        this.dateStarted = dateStarted;
    }

    public boolean getIsPaid() {
        return isPaid;
    }

    public void setIsPaid(boolean isPaid) {
        this.isPaid = isPaid;
    }
}
