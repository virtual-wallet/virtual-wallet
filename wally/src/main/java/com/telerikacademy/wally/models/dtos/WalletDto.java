package com.telerikacademy.wally.models.dtos;

import jdk.jfr.BooleanFlag;
import javax.validation.constraints.PositiveOrZero;

public class WalletDto {
    @PositiveOrZero(message = "ownerId should be positive or zero")
    private int ownerId;

    @PositiveOrZero(message = "currencyId should be positive or zero")
    private int currencyId;

    @BooleanFlag
    private boolean hasOverdraft;

//    @PositiveOrZero(message = "cardId should be positive or zero")
//    private int cardId;


    public WalletDto(int ownerId, int currencyId, boolean hasOverdraft) {
       setOwnerId(ownerId);
       setCurrencyId(currencyId);
       setHasOverdraft(hasOverdraft);
    }

    public WalletDto() {
    }

    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    public int getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(int currencyId) {
        this.currencyId = currencyId;
    }

    public boolean isHasOverdraft() {
        return hasOverdraft;
    }

    public void setHasOverdraft(boolean hasOverdraft) {
        this.hasOverdraft = hasOverdraft;
    }

}
