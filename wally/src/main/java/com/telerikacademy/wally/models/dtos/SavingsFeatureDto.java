package com.telerikacademy.wally.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class SavingsFeatureDto {

    @NotNull(message = "Name cannot be null.")
    @Size(min = 3, max = 50, message = "Name has to be between 3 and 50 characters.")
    private String name;

    @Positive(message = "Duration (months) has to be positive")
    private int durationMonths;

    @NotNull(message = "Please provide an AER.")
    private double aer;

    public SavingsFeatureDto(String name, int durationMonths, double aer) {
        this.name = name;
        this.durationMonths = durationMonths;
        this.aer = aer;
    }

    public SavingsFeatureDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDurationMonths() {
        return durationMonths;
    }

    public void setDurationMonths(int durationMonths) {
        this.durationMonths = durationMonths;
    }

    public double getAer() {
        return aer;
    }

    public void setAer(double aer) {
        this.aer = aer;
    }
}
