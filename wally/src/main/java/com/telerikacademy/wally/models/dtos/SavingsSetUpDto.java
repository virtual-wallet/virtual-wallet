package com.telerikacademy.wally.models.dtos;

import javax.validation.constraints.Positive;

public class SavingsSetUpDto {

    @Positive(message = "Wallet ID has to be positive.")
    private int walletId;

    @Positive(message = "Savings type ID has to be positive.")
    private int savingsTypeID;

    @Positive(message = "Base savings value has to be positive.")
    private double baseAmount;

    @Positive(message = "Currency ID has to be positive.")
    private int currencyId;

    public SavingsSetUpDto(int walletId, int savingsTypeID, double baseAmount, int currencyId) {
        this.walletId = walletId;
        this.savingsTypeID = savingsTypeID;
        this.baseAmount = baseAmount;
        this.currencyId = currencyId;
    }

    public SavingsSetUpDto() {

    }

    public int getWalletId() {
        return walletId;
    }

    public void setWalletId(int walletId) {
        this.walletId = walletId;
    }

    public int getSavingsTypeID() {
        return savingsTypeID;
    }

    public void setSavingsTypeID(int savingsTypeID) {
        this.savingsTypeID = savingsTypeID;
    }

    public double getBaseAmount() {
        return baseAmount;
    }

    public void setBaseAmount(double baseAmount) {
        this.baseAmount = baseAmount;
    }

    public int getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(int currencyId) {
        this.currencyId = currencyId;
    }
}
