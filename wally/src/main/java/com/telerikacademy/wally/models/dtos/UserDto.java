package com.telerikacademy.wally.models.dtos;

import javax.validation.constraints.*;


public class UserDto extends LoginDto{
    @NotEmpty(message = "Email cannot be empty.")
    @Email
    private String email;

    @NotNull(message = "Phone number cannot be empty.")
    @Size(max = 20, message = "Phone should be between 2 and 20 digits.")
    private String phone;

    public UserDto() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    }
