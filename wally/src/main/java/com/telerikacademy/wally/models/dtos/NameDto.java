package com.telerikacademy.wally.models.dtos;

import com.sun.istack.NotNull;

import javax.validation.constraints.Size;

public class NameDto {

    @NotNull
    @Size(min = 3, max = 50, message = "Name should be between 3 and 50 characters")
    private String value;

    public NameDto() {
    }

    public NameDto(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
