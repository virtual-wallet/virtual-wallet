package com.telerikacademy.wally.models.params;

import com.telerikacademy.wally.models.enums.contracts.SortOptions;
import com.telerikacademy.wally.models.params.contracts.ModelParameters;

import java.util.Date;
import java.util.Optional;

public class TransactionsFilterParams implements ModelParameters {

    private Optional<Date> dateFrom;
    private Optional<Date> dateTo;
    private Optional<String> toUsername;
    private Optional<String> fromUsername;
    private Optional<Boolean> isCredited;
    private Optional<SortOptions> sortOptions;
    private Optional<Integer> fromWalletId;
    private Optional<Integer> toWalletId;
    private Optional<Integer> category;
    private Optional<Integer> status;

    public TransactionsFilterParams(Optional<Date> dateFrom,
                                    Optional<Date> dateTo,
                                    Optional<String> toUsername,
                                    Optional<String> fromUsername,
                                    Optional<Boolean> isCredited,
                                    Optional<SortOptions> sortOptions,
                                    Optional<Integer> fromWalletId,
                                    Optional<Integer> toWalletId,
                                    Optional<Integer> category,
                                    Optional<Integer> status) {
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.toUsername = toUsername;
        this.fromUsername = fromUsername;
        this.isCredited = isCredited;
        this.sortOptions = sortOptions;
        this.fromWalletId = fromWalletId;
        this.toWalletId = toWalletId;
        this.category = category;
        this.status = status;
    }

    public TransactionsFilterParams() {
    }


    public Optional<Date> getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = Optional.ofNullable(dateFrom);
    }

    public void setFromUsername(Optional<String> fromUsername) {
        this.fromUsername = fromUsername;
    }

    public void setToUsername(Optional<String> toUsername) {
        this.toUsername = toUsername;
    }

    public void setDateFrom(Optional<Date> dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Optional<Date> getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = Optional.ofNullable(dateTo);
    }

    public void setDateTo(Optional<Date> dateTo) {
        this.dateTo = dateTo;
    }

    public Optional<String> getFromUsername() {
        return fromUsername;
    }

    public void setFromUsername(String fromUsername) {
        this.fromUsername = Optional.ofNullable(fromUsername);
    }

    public Optional<String> getToUsername() {
        return toUsername;
    }

    public void setToUsername(String toUsername) {
        this.toUsername = Optional.ofNullable(toUsername);
    }

    public Optional<Boolean> getIsCredited() {
        return isCredited;
    }

    public void setIsCredited(Boolean isCredited) {
        this.isCredited = Optional.ofNullable(isCredited);
    }

    public Optional<SortOptions> getSortOptions() {
        return sortOptions;
    }

    public void setSortOptions(SortOptions sortOptions) {
        this.sortOptions = Optional.ofNullable(sortOptions);
    }

    public Optional<Integer> getFromWalletId() {
        return fromWalletId;
    }

    public void setFromWalletId(Optional<Integer> fromWalletId) {
        this.fromWalletId = fromWalletId;
    }

    public void setFromWalletId(Integer walletId) {
        this.fromWalletId = Optional.ofNullable(walletId);
    }

    public Optional<Integer> getToWalletId() {
        return toWalletId;
    }

    public void setToWalletId(Integer toWalletId) {
        this.toWalletId = Optional.ofNullable(toWalletId);
    }

    public Optional<Integer> getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = Optional.ofNullable(category);
    }

    public Optional<Integer> getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = Optional.ofNullable(status);
    }

    public void setCategory(Optional<Integer> category) {
        this.category = category;
    }

    public void setStatus(Optional<Integer> status) {
        this.status = status;
    }
}
