package com.telerikacademy.wally.models.dtos;

import java.util.ArrayList;

public class UserFilterSearchSortDto {

    private String email;
    private String phone;
    private String username;
    private String sort;
    private String all;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getAll() {
        return all;
    }

    public void setAll(String all) {
        this.all = all;
    }

    public String toQueryString() {
        StringBuilder query = new StringBuilder();
        var filters = new ArrayList<String>();

        if (email != null && !email.equals("")){
            filters.add("email="+ email);
        }

        if (phone != null && !phone.equals("")){
            filters.add("phone="+ phone);
        }

        if (username != null && !username.equals("")){
            filters.add("username="+ username);
        }

        if (sort!= null){
            filters.add("sort="+sort);
        }

        if (all != null && !all.equals("")){
            filters.add("all="+ all);
        }

        if (filters.size()!=0){
            query.append("?").append(String.join("&", filters));
        }

        return query.toString();
    }
}
