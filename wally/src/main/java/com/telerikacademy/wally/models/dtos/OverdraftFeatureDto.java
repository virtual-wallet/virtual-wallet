package com.telerikacademy.wally.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class OverdraftFeatureDto {

    @NotNull(message = "Name cannot be null.")
    @Size(min = 3, max = 50, message = "Name has to be between 3 and 50 characters.")
    private String name;

    @Positive(message = "Max months without paying the overdraft have to be positive")
    private int maxMonths;

    @NotNull(message = "Please provide an APR.")
    private double apr;

    public OverdraftFeatureDto(String name, int maxMonths, double apr) {
        this.name = name;
        this.maxMonths = maxMonths;
        this.apr = apr;
    }

    public OverdraftFeatureDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMaxMonths() {
        return maxMonths;
    }

    public void setMaxMonths(int maxMonths) {
        this.maxMonths = maxMonths;
    }

    public double getApr() {
        return apr;
    }

    public void setApr(double apr) {
        this.apr = apr;
    }
}
