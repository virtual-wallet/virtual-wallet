package com.telerikacademy.wally.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;


@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int userId;

    @Column(name = "username", updatable = false)
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "email")
    private String email;

    @Column(name = "phone")
    private String phone;

    @Column(name = "enabled")
    private boolean isEnabled;

    @Column(name = "verification_code", updatable = false)
    private String verificationCode;

    @Column(name = "blocked")
    private boolean isBlocked;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "users_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    private Set<Role> roleSet;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "users_categories",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "category_id")
    )
    private Set<Category> categoriesSet;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        User user = (User) o;
        return this.getUserId() == (user.getUserId()) &&
                this.getUsername().equalsIgnoreCase(user.getUsername()) &&
                this.getEmail().equalsIgnoreCase(user.getEmail()) &&
                this.getPhone().equals(user.getPhone());
    }

    @Override
    public int hashCode() {
        return Objects.hash(username,
                email,
                roleSet,
                userId);
    }

    public boolean isAdmin() {
        for (Role role : roleSet) {
            if (role.getName().equalsIgnoreCase("admin")) {
                return true;
            }
        }
        return false;
    }

    public boolean isUser() {
        for (Role role : roleSet) {
            if (role.getName().equalsIgnoreCase("user")) {
                return true;
            }
        }
        return false;
    }
}
