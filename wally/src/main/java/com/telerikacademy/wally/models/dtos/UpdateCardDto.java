package com.telerikacademy.wally.models.dtos;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

public class UpdateCardDto {
    @Positive(message = "Currency should be chosen")
    private int currencyId;

    @NotEmpty(message = "Card number cannot be empty.")
    @Size(min = 16, max = 19, message = "Card number should be 16 digits")
    private String cardNumber;

    @NotEmpty(message = "Card holder cannot be empty.")
    private String cardHolder;

    @NotEmpty(message = "Date cannot be empty.")
    private String expireDate;

    @NotEmpty(message = "CVV cannot be empty.")
    @Size(min = 3, max = 3, message = "CVV has to be 3 digits only.")
    private String cvv;

    @Positive( message = "Card type should be chosen")
    private int cardTypeId;

    public UpdateCardDto() {
    }

    public int getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(int currencyId) {
        this.currencyId = currencyId;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public int getCardTypeId() {
        return cardTypeId;
    }

    public void setCardTypeId(int cardTypeId) {
        this.cardTypeId = cardTypeId;
    }

    public String getCardHolder() {
        return cardHolder;
    }

    public void setCardHolder(String cardHolder) {
        this.cardHolder = cardHolder;
    }
}
