package com.telerikacademy.wally.models.dtos;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UpdateUserDto {
    @NotEmpty(message = "Email cannot be empty.")
    @Email
    private String email;

    @NotNull(message = "Phone number cannot be empty.")
    @Size(max = 20, message = "Phone should be between 2 and 20 digits.")
    private String phone;

    public UpdateUserDto() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
