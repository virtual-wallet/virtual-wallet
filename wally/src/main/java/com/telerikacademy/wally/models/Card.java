package com.telerikacademy.wally.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "cards")
public class Card {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @OneToOne
    @JoinColumn(name = "currency_id")
    private Currency currency;

    @Column(name = "card_number")
    private String cardNumber;

    @Column(name = "cardholder")
    private String cardholder;

    @Column(name = "exp_date")
    private Date expireDate;

    @Column(name = "cvv")
    private int cvv;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    @OneToOne
    @JoinColumn(name = "card_type_id")
    private CardType cardType;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return this.currency.equals(card.getCurrency()) &&
                this.cardNumber.equalsIgnoreCase(card.getCardNumber()) &&
                this.cardholder.equalsIgnoreCase(card.getCardholder()) &&
                this.expireDate.equals(card.getExpireDate()) &&
                this.cvv == card.getCvv() &&
                this.cardType.equals(card.getCardType()) &&
                this.id == card.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(currency, cardholder, cardNumber, cardType, cvv, expireDate, id);
    }
}
