package com.telerikacademy.wally.repositories.contracts;

import com.telerikacademy.wally.models.OverdraftFeature;

public interface OverdraftFeatureRepository
        extends BaseGetRepository<OverdraftFeature>,
        BaseModifyRepository<OverdraftFeature> {
}
