package com.telerikacademy.wally.repositories;

import com.telerikacademy.wally.exceptions.EntityNotFoundException;
import com.telerikacademy.wally.models.User;
import com.telerikacademy.wally.models.params.UserSearchParams;
import com.telerikacademy.wally.repositories.contracts.BaseGetRepository;
import com.telerikacademy.wally.repositories.contracts.BaseModifyRepository;
import com.telerikacademy.wally.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Repository
public class UserRepositoryImpl
        extends BaseGetRepositoryImpl<User>
        implements UserRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        super(User.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public User create(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
        }
        return user;
    }

    @Override
    public void update(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        User userToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(userToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<User> searchAndSort(UserSearchParams userSearchParams) {
        try (Session session = sessionFactory.openSession()) {
            var baseQuery = new StringBuilder("from User u ");
            var filters = new ArrayList<String>();
            var params = new HashMap<String, Object>();

            userSearchParams.getEmail().ifPresent(value -> {
                filters.add(" email like :email ");
                params.put("email", "%" + value + "%");
            });

            userSearchParams.getPhone().ifPresent(value -> {
                filters.add((" phone like :phone "));
                params.put("phone", "%" + value + "%");
            });

            userSearchParams.getUsername().ifPresent(value -> {
                filters.add((" username like :username "));
                params.put("username", "%" + value + "%");
            });

            userSearchParams.getAll().ifPresent(value -> {
                filters.add((" username like :all "));
                filters.add((" phone like :all "));
                filters.add((" email like :all "));
                params.put("all", "%" + value + "%");
            });


            if (!filters.isEmpty()) baseQuery.append(" where ").append(String.join(" or ", filters));

            userSearchParams.getSortOptions().ifPresent(value -> {
                baseQuery.append(value.getQuery());
                params.put(" sort ", value);
            });

            return session.createQuery(baseQuery.toString(), User.class).
                    setProperties(params).
                    list();

        }
    }

    @Override
    public User findByVerificationCode(String code) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where verificationCode = :code", User.class);
            query.setParameter("code", code);
            List<User> users = query.list();
            if (users.size() == 0) {
                throw new EntityNotFoundException("User");
            }

            return users.get(0);
        }
    }
}
