package com.telerikacademy.wally.repositories.contracts;

import com.telerikacademy.wally.models.Currency;
import com.telerikacademy.wally.models.Photo;

public interface PhotoRepository extends BaseModifyRepository<Photo>{
}
