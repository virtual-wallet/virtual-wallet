package com.telerikacademy.wally.repositories.contracts;

import com.telerikacademy.wally.models.CardType;
import com.telerikacademy.wally.models.Wallet;

public interface CardTypeRepository extends BaseGetRepository<CardType>, BaseModifyRepository<CardType> {
}
