package com.telerikacademy.wally.repositories;

import com.telerikacademy.wally.models.SavingsFeature;
import com.telerikacademy.wally.repositories.contracts.SavingsFeatureRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class SavingsFeatureRepositoryImpl
        extends BaseModifyRepositoryImpl<SavingsFeature>
        implements SavingsFeatureRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public SavingsFeatureRepositoryImpl(SessionFactory sessionFactory) {
        super(SavingsFeature.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }
}
