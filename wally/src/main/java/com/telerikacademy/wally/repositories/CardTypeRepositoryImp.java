package com.telerikacademy.wally.repositories;

import com.telerikacademy.wally.models.Card;
import com.telerikacademy.wally.models.CardType;
import com.telerikacademy.wally.repositories.contracts.BaseModifyRepository;
import com.telerikacademy.wally.repositories.contracts.CardRepository;
import com.telerikacademy.wally.repositories.contracts.CardTypeRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CardTypeRepositoryImp  extends BaseModifyRepositoryImpl<CardType>
        implements BaseModifyRepository<CardType>, CardTypeRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public CardTypeRepositoryImp(SessionFactory sessionFactory) {
        super(CardType.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }
}
