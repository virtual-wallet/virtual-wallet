package com.telerikacademy.wally.repositories.contracts;

import com.telerikacademy.wally.models.Card;
import com.telerikacademy.wally.models.Wallet;

public interface CardRepository extends BaseGetRepository<Card>, BaseModifyRepository<Card> {
}
