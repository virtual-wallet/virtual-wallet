package com.telerikacademy.wally.repositories;

import com.telerikacademy.wally.models.SavingsRecord;
import com.telerikacademy.wally.repositories.contracts.SavingsRecordRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class SavingsRecordRepositoryImpl
        extends BaseModifyRepositoryImpl<SavingsRecord>
        implements SavingsRecordRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public SavingsRecordRepositoryImpl(SessionFactory sessionFactory) {
        super(SavingsRecord.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }
}
