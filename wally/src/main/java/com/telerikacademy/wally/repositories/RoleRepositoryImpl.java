package com.telerikacademy.wally.repositories;

import com.telerikacademy.wally.models.Role;
import com.telerikacademy.wally.repositories.contracts.BaseModifyRepository;
import com.telerikacademy.wally.repositories.contracts.RoleRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class RoleRepositoryImpl extends BaseModifyRepositoryImpl<Role>
        implements RoleRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public RoleRepositoryImpl(SessionFactory sessionFactory) {
        super(Role.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }
}
