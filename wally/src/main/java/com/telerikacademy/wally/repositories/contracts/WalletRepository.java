package com.telerikacademy.wally.repositories.contracts;

import com.telerikacademy.wally.models.Wallet;

import java.util.List;

public interface WalletRepository extends BaseGetRepository<Wallet>, BaseModifyRepository<Wallet> {

    List<Wallet> getAllInDept();
}
