package com.telerikacademy.wally.repositories.contracts;

import java.util.List;

public interface BaseGetRepository<T> {
    List<T> getAll();

    <V> List<T> getAllByField(String fieldName, V value);

    <V> T getByField(String fieldName, V value);

    T getById(int id);

    List<T> getAllById(int id);
}
