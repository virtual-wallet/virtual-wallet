package com.telerikacademy.wally.repositories;

import com.telerikacademy.wally.models.Photo;
import com.telerikacademy.wally.repositories.contracts.PhotoRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class PhotoRepositoryImpl extends BaseModifyRepositoryImpl<Photo>
        implements PhotoRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public PhotoRepositoryImpl(SessionFactory sessionFactory) {
        super(Photo.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }
}
