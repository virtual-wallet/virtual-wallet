package com.telerikacademy.wally.repositories.contracts;

import java.util.List;

public interface BaseModifyRepository<T> extends BaseGetRepository<T>{
    void create(T entity);

    void update(T entity);

    void updateAll(List<T> entity);

    void delete(int id);
}
