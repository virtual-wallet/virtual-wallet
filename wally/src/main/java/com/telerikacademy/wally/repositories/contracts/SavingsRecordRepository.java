package com.telerikacademy.wally.repositories.contracts;

import com.telerikacademy.wally.models.SavingsRecord;

public interface SavingsRecordRepository extends BaseModifyRepository<SavingsRecord> {
}
