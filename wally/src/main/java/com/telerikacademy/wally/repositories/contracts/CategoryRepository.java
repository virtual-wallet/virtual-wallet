package com.telerikacademy.wally.repositories.contracts;

import com.telerikacademy.wally.models.Category;

public interface CategoryRepository extends BaseModifyRepository<Category> {
}
