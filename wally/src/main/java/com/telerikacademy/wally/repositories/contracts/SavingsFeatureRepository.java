package com.telerikacademy.wally.repositories.contracts;

import com.telerikacademy.wally.models.SavingsFeature;

public interface SavingsFeatureRepository extends BaseModifyRepository<SavingsFeature> {
}
