package com.telerikacademy.wally.repositories.contracts;

import com.telerikacademy.wally.models.Role;

public interface RoleRepository extends BaseModifyRepository<Role>{
}
