package com.telerikacademy.wally.repositories.contracts;

import com.telerikacademy.wally.models.Category;
import com.telerikacademy.wally.models.Currency;

public interface CurrencyRepository extends BaseModifyRepository<Currency> {
}
