package com.telerikacademy.wally.repositories;

import com.telerikacademy.wally.models.OverdraftFeature;
import com.telerikacademy.wally.repositories.contracts.OverdraftFeatureRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class OverdraftFeatureRepositoryImpl
        extends BaseModifyRepositoryImpl<OverdraftFeature>
        implements OverdraftFeatureRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public OverdraftFeatureRepositoryImpl(SessionFactory sessionFactory) {
        super(OverdraftFeature.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }
}
