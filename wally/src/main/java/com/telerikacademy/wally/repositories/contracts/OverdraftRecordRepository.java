package com.telerikacademy.wally.repositories.contracts;

import com.telerikacademy.wally.models.OverdraftRecord;

public interface OverdraftRecordRepository extends BaseModifyRepository<OverdraftRecord> {
}
