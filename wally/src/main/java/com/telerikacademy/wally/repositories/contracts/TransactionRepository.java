package com.telerikacademy.wally.repositories.contracts;

import com.telerikacademy.wally.models.Transaction;
import com.telerikacademy.wally.models.params.TransactionsFilterParams;

import java.util.List;

public interface TransactionRepository extends BaseModifyRepository<Transaction> {

    List<com.telerikacademy.wally.models.Transaction> searchAndSort(TransactionsFilterParams transactionsFilterParams);
}
