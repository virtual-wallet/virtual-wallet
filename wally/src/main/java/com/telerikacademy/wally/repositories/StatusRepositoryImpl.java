package com.telerikacademy.wally.repositories;

import com.telerikacademy.wally.models.Status;
import com.telerikacademy.wally.repositories.contracts.StatusRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class StatusRepositoryImpl
        extends BaseModifyRepositoryImpl<Status>
        implements StatusRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public StatusRepositoryImpl(SessionFactory sessionFactory) {
        super(Status.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

}
