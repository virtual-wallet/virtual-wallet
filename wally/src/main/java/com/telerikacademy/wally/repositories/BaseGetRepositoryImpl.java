package com.telerikacademy.wally.repositories;

import com.telerikacademy.wally.exceptions.EntityNotFoundException;
import com.telerikacademy.wally.repositories.contracts.BaseGetRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import java.util.List;

import static java.lang.String.format;

public abstract class BaseGetRepositoryImpl<T> implements BaseGetRepository<T> {

    final Class<T> clazz;
    final SessionFactory sessionFactory;

    public BaseGetRepositoryImpl(Class<T> clazz,
                                 SessionFactory sessionFactory) {
        this.clazz = clazz;
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<T> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<T> query = session.createQuery("from " + clazz.getSimpleName(), clazz);
            return query.list();
        }
    }

    @Override
    public <V> List<T> getAllByField(String fieldName, V value) {
        try (Session session = sessionFactory.openSession()) {
            Query<T> query = session.createQuery(format("from %s where %s = :%s",
                    clazz.getSimpleName(), fieldName, fieldName), clazz);
            query.setParameter(fieldName, value);
            var result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException(clazz.getSimpleName(), fieldName, value);
            }
            return result;
        }
    }

    @Override
    public <V> T getByField(String fieldName, V value) {
        var result = getAllByField(fieldName, value);
        return result.get(0);
    }

    @Override
    public T getById(int id) {
        return getByField("id", id);
    }

    @Override
    public List<T> getAllById(int id) {
        return getAllByField("id", id);
    }


}
