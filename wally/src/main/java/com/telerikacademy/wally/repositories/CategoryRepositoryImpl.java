package com.telerikacademy.wally.repositories;

import com.telerikacademy.wally.models.Category;
import com.telerikacademy.wally.repositories.contracts.CategoryRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CategoryRepositoryImpl
        extends BaseModifyRepositoryImpl<Category>
        implements CategoryRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public CategoryRepositoryImpl(SessionFactory sessionFactory) {
        super(Category.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

}
