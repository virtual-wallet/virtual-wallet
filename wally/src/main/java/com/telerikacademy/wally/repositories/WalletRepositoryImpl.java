package com.telerikacademy.wally.repositories;

import com.telerikacademy.wally.exceptions.EntityNotFoundException;
import com.telerikacademy.wally.models.Wallet;
import com.telerikacademy.wally.repositories.contracts.BaseModifyRepository;
import com.telerikacademy.wally.repositories.contracts.WalletRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static java.lang.String.format;

@Repository
public class WalletRepositoryImpl 
        extends BaseModifyRepositoryImpl<Wallet>
        implements BaseModifyRepository<Wallet>, WalletRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public WalletRepositoryImpl(SessionFactory sessionFactory) {
        super(Wallet.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Wallet> getAllInDept() {
        try (Session session = sessionFactory.openSession()) {
            var result = session.createQuery("from Wallet where balance < 0", clazz).list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Wallets in dept");
            }
            return result;
        }
    }


}
