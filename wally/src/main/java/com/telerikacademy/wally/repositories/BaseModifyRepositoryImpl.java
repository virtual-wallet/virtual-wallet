package com.telerikacademy.wally.repositories;

import com.telerikacademy.wally.repositories.contracts.BaseModifyRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;


import java.util.List;

public abstract class BaseModifyRepositoryImpl<T> extends BaseGetRepositoryImpl<T> implements BaseModifyRepository<T> {

    public BaseModifyRepositoryImpl(Class<T> clazz, SessionFactory sessionFactory) {
        super(clazz, sessionFactory);
    }

    @Override
    public void create(Object entity) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(entity);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(Object entity) {
        try (Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(entity);
            session.getTransaction().commit();
        }
    }

    @Override
    public void updateAll(List<T> entity) {
        try (Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(entity);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        T entityToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(entityToDelete);
            session.getTransaction().commit();
        }
    }
}
