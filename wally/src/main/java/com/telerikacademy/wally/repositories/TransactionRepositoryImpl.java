package com.telerikacademy.wally.repositories;

import com.telerikacademy.wally.models.Transaction;
import com.telerikacademy.wally.models.params.TransactionsFilterParams;
import com.telerikacademy.wally.repositories.contracts.TransactionRepository;
import com.telerikacademy.wally.services.BaseModifyServiceImpl;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
@Repository
public class TransactionRepositoryImpl
        extends BaseModifyRepositoryImpl<Transaction>
        implements TransactionRepository {

    private final SessionFactory sessionFactory;

    public TransactionRepositoryImpl(SessionFactory sessionFactory) {
        super(Transaction.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    public List<Transaction> searchAndSort(TransactionsFilterParams transactionsFilterParams) {
        try (Session session = sessionFactory.openSession()) {
            var baseQuery = new StringBuilder("from Transaction t ");
            var filters = new ArrayList<String>();
            var params = new HashMap<String, Object>();

            transactionsFilterParams.getFromUsername().ifPresent(value -> {
                filters.add(" fromUser.username = :fromUsername ");
                params.put("fromUsername", value);
            });

            transactionsFilterParams.getToUsername().ifPresent(value -> {
                filters.add((" toWallet.owner.username = :toUsername "));
                params.put("toUsername", value);
            });

            transactionsFilterParams.getDateFrom().ifPresent(value -> {
                filters.add((" transactionTime >= :dateFrom "));
                params.put("dateFrom", value);
            });

            transactionsFilterParams.getDateTo().ifPresent(value -> {
                filters.add((" transactionTime <= :dateTo "));
                params.put("dateTo", value);
            });

            transactionsFilterParams.getFromWalletId().ifPresent(value -> {
                filters.add((" fromWallet.walletId = :fromWalletId "));
                params.put("fromWalletId", value);
            });

            transactionsFilterParams.getToWalletId().ifPresent(value -> {
                filters.add((" toWallet.walletId = :toWalletId "));
                params.put("toWalletId", value);
            });

            transactionsFilterParams.getStatus().ifPresent(value -> {
                filters.add((" status.id = :statusId "));
                params.put("statusId", value);
            });

            transactionsFilterParams.getCategory().ifPresent(value -> {
                filters.add((" category.id = :categoryId "));
                params.put("categoryId", value);
            });

            transactionsFilterParams.getIsCredited().ifPresent(value -> {
                if (value) {
                    filters.add((" transferValue < 0 "));
                } else {
                    filters.add((" transferValue > 0 "));
                }
            });

            if (!filters.isEmpty()) baseQuery.append(" where ").append(String.join(" and ", filters));

            transactionsFilterParams.getSortOptions().ifPresent(value -> {
                baseQuery.append(value.getQuery());
                params.put(" sort ", value);
            });

            return session.createQuery(baseQuery.toString(), Transaction.class).
                    setProperties(params).
                    list();

        }
    }

}
