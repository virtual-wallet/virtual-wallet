package com.telerikacademy.wally.repositories;

import com.telerikacademy.wally.models.Card;
import com.telerikacademy.wally.models.Wallet;
import com.telerikacademy.wally.repositories.contracts.BaseModifyRepository;
import com.telerikacademy.wally.repositories.contracts.CardRepository;
import com.telerikacademy.wally.repositories.contracts.WalletRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CardRepositoryImpl extends BaseModifyRepositoryImpl<Card>
        implements BaseModifyRepository<Card>, CardRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public CardRepositoryImpl(SessionFactory sessionFactory) {
        super(Card.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }
}
