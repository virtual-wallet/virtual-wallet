package com.telerikacademy.wally.repositories;

import com.telerikacademy.wally.models.Currency;
import com.telerikacademy.wally.repositories.contracts.CurrencyRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CurrencyRepositoryImpl  extends BaseModifyRepositoryImpl<Currency>
        implements CurrencyRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public CurrencyRepositoryImpl(SessionFactory sessionFactory) {
        super(Currency.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }
}
