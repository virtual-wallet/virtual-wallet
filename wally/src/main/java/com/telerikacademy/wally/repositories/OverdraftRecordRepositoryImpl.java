package com.telerikacademy.wally.repositories;

import com.telerikacademy.wally.models.OverdraftRecord;
import com.telerikacademy.wally.repositories.contracts.OverdraftRecordRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class OverdraftRecordRepositoryImpl
        extends BaseModifyRepositoryImpl<OverdraftRecord>
        implements OverdraftRecordRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public OverdraftRecordRepositoryImpl(SessionFactory sessionFactory) {
        super(OverdraftRecord.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }
}
