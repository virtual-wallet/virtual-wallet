package com.telerikacademy.wally.repositories.contracts;

import com.telerikacademy.wally.models.Status;

public interface StatusRepository extends BaseModifyRepository<Status> {
}
