package com.telerikacademy.wally.repositories.contracts;

import com.telerikacademy.wally.models.User;
import com.telerikacademy.wally.models.params.UserSearchParams;

import java.util.List;

public interface UserRepository extends BaseGetRepository<User> {

    User create(User user);

    void update(User user);

    void delete(int id);

    List<User> searchAndSort(UserSearchParams userSearchParams);

    User findByVerificationCode(String code);
}
