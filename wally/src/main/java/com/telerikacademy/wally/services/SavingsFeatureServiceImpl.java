package com.telerikacademy.wally.services;

import com.telerikacademy.wally.models.SavingsFeature;
import com.telerikacademy.wally.models.SavingsRecord;
import com.telerikacademy.wally.models.Wallet;
import com.telerikacademy.wally.repositories.contracts.SavingsFeatureRepository;
import com.telerikacademy.wally.services.contracts.SavingsFeatureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SavingsFeatureServiceImpl
        extends BaseModifyServiceImpl<SavingsFeature, SavingsFeatureRepository>
        implements SavingsFeatureService {

    private final SavingsFeatureRepository savingsFeatureRepository;

    @Autowired
    public SavingsFeatureServiceImpl(SavingsFeatureRepository savingsFeatureRepository) {
        super(SavingsFeature.class, savingsFeatureRepository);
        this.savingsFeatureRepository = savingsFeatureRepository;
    }
}
