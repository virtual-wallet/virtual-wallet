package com.telerikacademy.wally.services.contracts;

import com.telerikacademy.wally.models.Transaction;
import com.telerikacademy.wally.models.User;
import com.telerikacademy.wally.models.Wallet;
import com.telerikacademy.wally.models.dtos.TransactionDto;

public interface PaymentManager {

    void setCardPaymentIn(int walletId, TransactionDto transactionDto, User user);

    void setInternalPayment(Transaction transaction, User user);

    void paybackUserDeposit(Wallet walletFrom, Wallet walletTo, double transactionValue, Transaction transaction);

    void commitSavingsDeposit(Wallet bankWallet, Wallet userWallet, double depositAmount, Transaction transaction);
}
