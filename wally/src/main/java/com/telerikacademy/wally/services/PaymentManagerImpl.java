package com.telerikacademy.wally.services;

import com.telerikacademy.wally.config.ExternalApiUrlConfig;
import com.telerikacademy.wally.exceptions.EntityNotFoundException;
import com.telerikacademy.wally.exceptions.PaymentIssueException;
import com.telerikacademy.wally.models.*;
import com.telerikacademy.wally.models.dtos.MockBankDto;
import com.telerikacademy.wally.models.dtos.TransactionDto;
import com.telerikacademy.wally.services.contracts.*;
import com.telerikacademy.wally.utils.mappers.TransactionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static com.telerikacademy.wally.controllers.utils.Constants.*;
import static com.telerikacademy.wally.utils.ExternalQuery.createExternalTransferRequestQuery;

@Service
public class PaymentManagerImpl implements PaymentManager {
    public static final int SAME_CURRENCY_EXCHANGE_RATE = 1;
    private final WalletService walletService;
    private final TransactionService transactionService;
    private final ExternalApiUrlConfig externalApiUrlConfig;
    private final TransactionMapper transactionMapper;
    private final StatusService statusService;
    private final UserService userService;
    private final OverdraftManager overdraftManager;
    private final CurrencyService currencyService;

    @Autowired
    public PaymentManagerImpl(WalletService walletService,
                              TransactionService transactionService,
                              ExternalApiUrlConfig externalApiUrlConfig,
                              TransactionMapper transactionMapper,
                              StatusService statusService,
                              UserService userService,
                              OverdraftManager overdraftManager,
                              CurrencyService currencyService) {
        this.walletService = walletService;
        this.transactionService = transactionService;
        this.externalApiUrlConfig = externalApiUrlConfig;
        this.transactionMapper = transactionMapper;
        this.statusService = statusService;
        this.userService = userService;
        this.overdraftManager = overdraftManager;
        this.currencyService = currencyService;
    }

    @Override
    public void setCardPaymentIn(int payeeWalletId, TransactionDto transactionDto, User user) {
        Wallet payeeWallet = walletService.getById(payeeWalletId);

        Transaction transaction = transactionMapper.fromDtoExternalCard(transactionDto, payeeWallet, user);
        transactionService.create(transaction);

        if (transactionDto.getStatusId() == STATUS_COMPLETED) {
            walletService.executeCardToWalletTransaction(payeeWallet,
                    transactionDto.getValue(),
                    transactionDto.getExchangeRate());
        }
    }

    @Override
    public void setInternalPayment(Transaction transaction, User user) {
        int bankWalletId = externalApiUrlConfig.getBankMainAcc();
        Wallet payerWallet = transaction.getFromWallet();
        Wallet payeeWallet = transaction.getToWallet();

        double exchangeRate = transaction.getExchangeRate();
        double payerWalletBalance = payerWallet.getBalance();
        double transferValue = transaction.getTransferValue();

        processPayment(transaction, bankWalletId, payerWallet, payeeWallet, payerWalletBalance, transferValue, exchangeRate);
        transactionService.create(transaction);
    }

    private void processPayment(Transaction transaction,
                                int bankWalletId,
                                Wallet payerWallet,
                                Wallet payeeWallet,
                                double payerWalletBalance,
                                double transferValue,
                                double exchangeRate) {
        if (payerWalletBalance >= transferValue) {
            double payeeBalance = payeeWallet.getBalance();
            double newPayeeBalance;
            double transferValuePayeeCurrency;

            payerWalletBalance -= transferValue;
            payerWallet.setBalance(payerWalletBalance);

            transferValuePayeeCurrency = transferValue * exchangeRate;
            newPayeeBalance = payeeBalance + transferValuePayeeCurrency;

            if (payeeBalance < 0 && payeeWallet.getWalletId() != bankWalletId) {
                overdraftManager.paybackUserOverdraftToBankOnly(payeeWallet, transferValuePayeeCurrency);
            }

            payeeWallet.setBalance(newPayeeBalance);

            List<Wallet> walletsToUpdate = new ArrayList<>(Arrays.asList(payeeWallet, payerWallet));
            for (Wallet wallet : walletsToUpdate) {
                walletService.update(wallet);
            }
        } else if (payerWallet.hasOverdraftEnabled() && payerWallet.getWalletId() != bankWalletId) {
            overdraftManager.financeUserPayment(transaction, payerWallet, payerWalletBalance);
        } else {
            throw new PaymentIssueException("Insufficient Funds");
        }
    }

    @Scheduled(cron = "0 */1 * * * *")
    void checkPendingReceivedTransactionStatuses() {
        List<Transaction> transactions = null;
        User bankAuthorization = userService.getById(Integer.parseInt(externalApiUrlConfig.getBankUserId()));

        try {
            transactions = transactionService.getAllByField("status_id", STATUS_PENDING);
        } catch (EntityNotFoundException e) {
            return;
        }
        List<Transaction> transactionsToUpdate = new ArrayList<>();
        MockBankDto dto = new MockBankDto();

        for (Transaction transaction : transactions) {
            dto.setWalletId(transaction.getToWallet().getWalletId());
            ResponseEntity<String> response;
            try {
                String checkTransactionStatusUrl = externalApiUrlConfig.getMockBankStatusCheckUrl();
                response = createExternalTransferRequestQuery(dto, checkTransactionStatusUrl);
                if (Objects.requireNonNull(response.getBody()).equalsIgnoreCase("approved")) {
                    transaction.setStatus(statusService.getById(STATUS_COMPLETED));
                    transactionsToUpdate.add(transaction);

                    walletService.executeCardToWalletTransaction(
                            transaction.getToWallet(), transaction.getTransferValue(), transaction.getExchangeRate());
                }
            } catch (HttpClientErrorException ignored) {
            }
        }

        if (transactionsToUpdate.size() > 0) {
            transactionsToUpdate.forEach(transaction -> transactionService.update(transaction, bankAuthorization));
        }
    }

    @Override
    public void paybackUserDeposit(Wallet walletFrom, Wallet walletTo, double transactionValue, Transaction transaction) {
        int bankWalletId = externalApiUrlConfig.getBankMainAcc();
        double exchangeRate = currencyService.calculateExchangeRate(walletFrom, walletTo);

        processPayment(transaction, bankWalletId, walletFrom, walletTo, walletFrom.getBalance(), transactionValue, exchangeRate);
        transactionService.create(transaction);
    }

    @Override
    public void commitSavingsDeposit(Wallet bankWallet, Wallet userWallet, double depositAmount, Transaction transaction) {
        double exchangeRate = currencyService.calculateExchangeRate(userWallet, bankWallet);
        double amountPayableByUser = depositAmount / transaction.getExchangeRate();

        if (depositAmount * transaction.getExchangeRate() >= userWallet.getBalance()) {
            paybackUserDeposit(bankWallet, userWallet, depositAmount, transaction);
            return;
        }
        processPayment(transaction,
                bankWallet.getWalletId(),
                bankWallet,
                userWallet,
                bankWallet.getBalance(),
                amountPayableByUser,
                exchangeRate);
        transactionService.create(transaction);
    }

}
