package com.telerikacademy.wally.services;

import com.telerikacademy.wally.exceptions.DuplicateEntityException;
import com.telerikacademy.wally.exceptions.EntityNotFoundException;
import com.telerikacademy.wally.models.Card;
import com.telerikacademy.wally.models.Currency;
import com.telerikacademy.wally.models.Wallet;
import com.telerikacademy.wally.models.dtos.TransactionDto;
import com.telerikacademy.wally.repositories.contracts.CurrencyRepository;
import com.telerikacademy.wally.services.contracts.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CurrencyServiceImpl
        extends BaseModifyServiceImpl<Currency, CurrencyRepository>
        implements CurrencyService {

    private final CurrencyRepository currencyRepository;

    @Autowired
    public CurrencyServiceImpl(CurrencyRepository currencyRepository) {
        super(Currency.class, currencyRepository);
        this.currencyRepository = currencyRepository;
    }

    @Override
    public double calculateExchangeRate(Wallet walletFrom, Wallet walletTo) {
        Currency currencySender = walletFrom.getCurrency();
        Currency currencyRecipient = walletTo.getCurrency();

        return currencyRecipient.getExchangeRate() / currencySender.getExchangeRate();
    }

    @Override
    public double calculateExchangeRate(Card payerCard, Wallet walletTo) {
        Currency currencySender = payerCard.getCurrency();
        Currency currencyRecipient = walletTo.getCurrency();

        return currencyRecipient.getExchangeRate() / currencySender.getExchangeRate();
    }

    @Override
    public double calculateExchangeRate(Wallet payerWallet, Currency payeeCurrency) {
        Currency currencySender = payerWallet.getCurrency();

        return payeeCurrency.getExchangeRate() / currencySender.getExchangeRate();
    }

    @Override
    public double calculateExchangeRate(Currency currencyFrom, Wallet userWallet) {
        Currency currencySender = userWallet.getCurrency();

        return currencySender.getExchangeRate() / currencyFrom.getExchangeRate();
    }
}
