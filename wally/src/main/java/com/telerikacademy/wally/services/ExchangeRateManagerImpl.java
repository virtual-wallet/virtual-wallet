package com.telerikacademy.wally.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.telerikacademy.wally.config.ExternalApiUrlConfig;
import com.telerikacademy.wally.models.Currency;
import com.telerikacademy.wally.models.User;
import com.telerikacademy.wally.services.contracts.CurrencyService;
import com.telerikacademy.wally.services.contracts.UserService;
import com.telerikacademy.wally.utils.EmailNotificationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

@Service
public class ExchangeRateManagerImpl {

    private final ExternalApiUrlConfig externalApiUrlConfig;
    private final CurrencyService currencyService;
    private final UserService userService;

    @Autowired
    public ExchangeRateManagerImpl(ExternalApiUrlConfig externalApiUrlConfig,
                                   CurrencyService currencyService,
                                   UserService userService) {
        this.externalApiUrlConfig = externalApiUrlConfig;
        this.currencyService = currencyService;
        this.userService = userService;
    }

    private Map<String, Double> getRates(String forexUrl) throws JsonProcessingException {

        RestTemplate restTemplate = new RestTemplate();
        String result = null;
        try {
            result = restTemplate.getForObject(forexUrl, String.class);
        } catch (HttpClientErrorException e) {
            try {
                EmailNotificationUtil.sendNotificationEmail();
            } catch (MessagingException | UnsupportedEncodingException f) {
                f.printStackTrace();
            }

            if (!forexUrl.equalsIgnoreCase(externalApiUrlConfig.getBackupForexUrl())) {
                getRates(externalApiUrlConfig.getBackupForexUrl());
            }
        }
        Map<String, Object> jsonFileAsMap = new ObjectMapper().readValue(result, new TypeReference<>() {});

        return (Map<String, Double>) jsonFileAsMap.get("rates");
    }

    //todo activate only when needed!
//    @Scheduled(cron = "*/5 * * * * *")
    private void updateExchangeRates() throws JsonProcessingException {
        Map<String, Double> rates = getRates(externalApiUrlConfig.getForexUrl());
        rates.remove("EUR");

        List<Currency> currencies = currencyService.getAll();

        rates.forEach((key, value) -> currencies.forEach(currency -> {
            if (key.contains(currency.getIso())) {
                if (value > 0.5 * currency.getExchangeRate() || value < 0.5 * currency.getExchangeRate()) {
                    try {
                        EmailNotificationUtil.sendNotificationEmail();
                    } catch (MessagingException | UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
                currency.setExchangeRate(value);
            }
        }));

        User bankAuthorization = userService.getById(Integer.parseInt(externalApiUrlConfig.getBankUserId()));
        currencies.forEach(currency -> currencyService.update(currency, bankAuthorization));
    }
}