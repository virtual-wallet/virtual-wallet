package com.telerikacademy.wally.services;

import com.telerikacademy.wally.models.SavingsRecord;
import com.telerikacademy.wally.repositories.contracts.SavingsRecordRepository;
import com.telerikacademy.wally.services.contracts.SavingsRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SavingsRecordServiceImpl
        extends BaseModifyServiceImpl<SavingsRecord, SavingsRecordRepository>
        implements SavingsRecordService {

    private final SavingsRecordRepository savingsRecordRepository;

    @Autowired
    public SavingsRecordServiceImpl(SavingsRecordRepository savingsRecordRepository) {
        super(SavingsRecord.class, savingsRecordRepository);
        this.savingsRecordRepository = savingsRecordRepository;
    }
}
