package com.telerikacademy.wally.services;

import com.telerikacademy.wally.models.Transaction;
import com.telerikacademy.wally.models.User;
import com.telerikacademy.wally.models.Wallet;
import com.telerikacademy.wally.models.params.TransactionsFilterParams;
import com.telerikacademy.wally.repositories.contracts.WalletRepository;
import com.telerikacademy.wally.services.contracts.CurrencyService;
import com.telerikacademy.wally.services.contracts.TransactionService;
import com.telerikacademy.wally.services.contracts.WalletService;
import com.telerikacademy.wally.utils.Paginator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class WalletServiceImpl extends BaseModifyServiceImpl<Wallet, WalletRepository> implements WalletService {

    private final WalletRepository walletRepository;
    private final TransactionService transactionService;
    private final CurrencyService currencyService;

    @Autowired
    public WalletServiceImpl(WalletRepository walletRepository,
                             TransactionService transactionService,
                             CurrencyService currencyService) {
        super(Wallet.class, walletRepository);
        this.walletRepository = walletRepository;
        this.transactionService = transactionService;
        this.currencyService = currencyService;
    }

    @Override
    public Map<String, Double> getUserTotalBalance(List<Wallet> userWallets) {
        Map<String, Double> totalBalance = new HashMap<>();
        userWallets.forEach(wallet ->
                totalBalance.merge(wallet.getCurrency().getCurrencyName(),
                        wallet.getBalance(),
                        Double::sum));
        return totalBalance;
    }

    @Override
    public Map<String, Double> getWalletTotalBalance(Wallet wallet) {
        Map<String, Double> totalBalance = new HashMap<>();
        totalBalance.merge(wallet.getCurrency().getCurrencyName(), wallet.getBalance(), Double::sum);
        return totalBalance;
    }

    @Override
    public Map<String, Map<String, Double>> getLastMonthTransactionsSummary(Optional<Integer> walletId,
                                                                            User user,
                                                                            TransactionsFilterParams incomingTransactionsFilterParams,
                                                                            TransactionsFilterParams outgoingTransactionsFilterParams) {

        Map<String, Map<String, Double>> pastTransactions = new HashMap<>();
        pastTransactions.put("In", new HashMap<>());
        pastTransactions.put("Out", new HashMap<>());

        List<Transaction> transactionsOut = transactionService.searchAndSort(outgoingTransactionsFilterParams);
        transactionsOut = transactionsOut.stream()
                .filter(transaction -> transaction.getFromWallet().getWalletId()
                        != transaction.getToWallet().getWalletId()).collect(Collectors.toList());

        transactionsOut.forEach(transaction ->
                pastTransactions.get("Out")
                        .merge(transaction.getFromWallet().getCurrency().getCurrencyName(),
                                transaction.getTransferValue(),
                                Double::sum));

        List<Transaction> transactionsIn = transactionService.searchAndSort(incomingTransactionsFilterParams);
        transactionsIn.forEach(transaction ->
                pastTransactions.get("In")
                        .merge(transaction.getToWallet().getCurrency().getCurrencyName(),
                                transaction.getTransferValue() * transaction.getExchangeRate(),
                                Double::sum));
        return pastTransactions;
    }

    @Override
    public Page<Wallet> getPaginated(Pageable pageable) {
        return Paginator.paginate(pageable, getAll());
    }

    @Override
    public void executeCardToWalletTransaction(Wallet payeeWallet, double valueOfTransaction, double exchangeRate) {
        double currentBalance = payeeWallet.getBalance();
        payeeWallet.setBalance(currentBalance + valueOfTransaction * exchangeRate);
        walletRepository.update(payeeWallet);
    }

    @Override
    public List<Wallet> getAllInDept() {
        return walletRepository.getAllInDept();
    }

}
