package com.telerikacademy.wally.services;

import com.telerikacademy.wally.exceptions.DuplicateEntityException;
import com.telerikacademy.wally.exceptions.EntityNotFoundException;
import com.telerikacademy.wally.exceptions.UnauthorizedOperationException;
import com.telerikacademy.wally.models.User;

public class AuthorizationHelper {

    public static final String AUTHORIZATION_REQUIRED_MESSAGE = "Unauthorized access.";
    public static final String AUTHENTICATION_OR_AUTHENTICATION_REQUIRED_MESSAGE = "Only the owner of the account or a member of our team can access the profile.";
    public static final String AUTHENTICATION_REQUIRED_MESSAGE = "Authentication required.";

    public static void isAccountOwnerOrAdmin(int userId, User userToAuthenticate) {
        if (userToAuthenticate.getUserId() != userId) {
            if (!userToAuthenticate.isAdmin()) {
                throw new UnauthorizedOperationException(
                        AUTHENTICATION_OR_AUTHENTICATION_REQUIRED_MESSAGE);
            }
        }
    }

    public static void isAdmin(User userToAuthenticate) {
        if (!userToAuthenticate.isAdmin()) {
            throw new UnauthorizedOperationException(AUTHORIZATION_REQUIRED_MESSAGE);
        }
    }


//    static void isAccountOwnerOrAdmin(User userAccount, User userToAuthenticate) {
//        if (!userAccount.getUsername().equals(userToAuthenticate.getUsername())) {
//            if (userToAuthenticate.isAdmin()) {
//                throw new UnauthorizedOperationException(
//                        AUTHENTICATION_OR_AUTHENTICATION_REQUIRED_MESSAGE);
//            }
//        }
//    }
//    public static void isCustomerOrAdmin(User userToAuthenticate) {
//        if (!userToAuthenticate.isUser()) {
//            if (!userToAuthenticate.isAdmin()) {
//                throw new UnauthorizedOperationException(
//                        AUTHENTICATION_OR_AUTHENTICATION_REQUIRED_MESSAGE);
//            }
//        }
//    }
//
//    static void isCustomer(User userToAuthenticate) {
//        if (!userToAuthenticate.isUser()) {
//            throw new UnauthorizedOperationException(AUTHENTICATION_REQUIRED_MESSAGE);
//        }
//    }
}
