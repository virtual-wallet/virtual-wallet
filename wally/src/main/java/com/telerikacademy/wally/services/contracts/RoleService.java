package com.telerikacademy.wally.services.contracts;

import com.telerikacademy.wally.models.Role;
import com.telerikacademy.wally.models.Status;
import com.telerikacademy.wally.repositories.contracts.RoleRepository;
import com.telerikacademy.wally.repositories.contracts.StatusRepository;

public interface RoleService extends BaseModifyService<Role, RoleRepository>,BaseGetService<Role, RoleRepository>{
}
