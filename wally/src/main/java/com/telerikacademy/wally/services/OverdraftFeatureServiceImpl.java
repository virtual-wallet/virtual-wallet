package com.telerikacademy.wally.services;

import com.telerikacademy.wally.models.OverdraftFeature;
import com.telerikacademy.wally.repositories.contracts.OverdraftFeatureRepository;
import com.telerikacademy.wally.services.contracts.OverdraftFeatureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OverdraftFeatureServiceImpl
        extends BaseModifyServiceImpl<OverdraftFeature, OverdraftFeatureRepository>
        implements OverdraftFeatureService {

    private final OverdraftFeatureRepository overdraftFeatureRepository;

    @Autowired
    public OverdraftFeatureServiceImpl(OverdraftFeatureRepository overdraftFeatureRepository) {
        super(OverdraftFeature.class, overdraftFeatureRepository);
        this.overdraftFeatureRepository = overdraftFeatureRepository;
    }
}
