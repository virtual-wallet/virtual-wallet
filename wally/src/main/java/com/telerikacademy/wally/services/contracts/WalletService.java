package com.telerikacademy.wally.services.contracts;

import com.telerikacademy.wally.models.User;
import com.telerikacademy.wally.models.Wallet;
import com.telerikacademy.wally.models.dtos.TransactionDto;
import com.telerikacademy.wally.models.params.TransactionsFilterParams;
import com.telerikacademy.wally.repositories.contracts.WalletRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface WalletService extends BaseModifyService<Wallet, WalletRepository> {

    Map<String, Double> getUserTotalBalance(List<Wallet> userWallets);

    Map<String, Double> getWalletTotalBalance(Wallet wallet);

    Map<String, Map<String, Double>> getLastMonthTransactionsSummary(Optional<Integer> walletId,
                                                                     User user,
                                                                     TransactionsFilterParams incomingTransactionsFilterParams,
                                                                     TransactionsFilterParams outgoingTransactionsFilterParams);

    Page<Wallet> getPaginated(Pageable pageable);

        void executeCardToWalletTransaction(Wallet payeeWallet, double valueOfTransaction, double exchangeRate);

    List<Wallet> getAllInDept();
}
