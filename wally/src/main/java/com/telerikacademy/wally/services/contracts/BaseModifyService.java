package com.telerikacademy.wally.services.contracts;

import com.telerikacademy.wally.models.User;
import com.telerikacademy.wally.repositories.contracts.BaseModifyRepository;

public interface BaseModifyService<T, R extends BaseModifyRepository<T>> extends BaseGetService<T, R> {
    void create(T entity, User userToAuthenticateOrAuthorize);

    void create(T entity);

    void update(T entityToUpdate, User userToAuthenticateOrAuthorize);

    void update(T entityToUpdate);

    void delete(int id, User userToAuthenticateOrAuthorize);

    void delete(int id);
}
