package com.telerikacademy.wally.services;

import com.telerikacademy.wally.config.ExternalApiUrlConfig;
import com.telerikacademy.wally.models.Transaction;
import com.telerikacademy.wally.models.User;
import com.telerikacademy.wally.models.params.TransactionsFilterParams;
import com.telerikacademy.wally.repositories.contracts.TransactionRepository;
import com.telerikacademy.wally.services.contracts.StatusService;
import com.telerikacademy.wally.services.contracts.TransactionService;
import com.telerikacademy.wally.services.contracts.WalletService;
import com.telerikacademy.wally.utils.Paginator;
import com.telerikacademy.wally.utils.mappers.TransactionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


import java.util.*;
import java.util.stream.Collectors;

import static com.telerikacademy.wally.models.enums.TransactionSortOptions.DATE_DESC;

@Service
public class TransactionServiceImpl
        extends BaseModifyServiceImpl<Transaction, TransactionRepository>
        implements TransactionService {

    private final TransactionRepository transactionRepository;
    private final ExternalApiUrlConfig externalApiUrlConfig;
    private final StatusService statusService;
    private final TransactionMapper transactionMapper;
    private final WalletService walletService;

    @Autowired
    public TransactionServiceImpl(TransactionRepository transactionRepository,
                                  ExternalApiUrlConfig externalApiUrlConfig,
                                  StatusService statusService,
                                  TransactionMapper transactionMapper,
                                  @Lazy WalletService walletService) {
        super(Transaction.class, transactionRepository);
        this.transactionRepository = transactionRepository;
        this.externalApiUrlConfig = externalApiUrlConfig;
        this.statusService = statusService;
        this.transactionMapper = transactionMapper;
        this.walletService = walletService;
    }

    @Override
    public List<Transaction> searchAndSort(TransactionsFilterParams transactionsFilterParams) {
        return transactionRepository.searchAndSort(transactionsFilterParams);
    }

    @Override
    public Page<Transaction> findPaginated(Pageable pageable,User user) {
        AuthorizationHelper.isAdmin(user);
        List<Transaction> transactions = getAll();
        return Paginator.paginate(pageable, transactions);
    }

    @Override
    public Page<Transaction> findPaginated(Pageable pageable, TransactionsFilterParams transactionsFilterParams,User user) {
        AuthorizationHelper.isAdmin(user);
        List<Transaction> transactions = transactionRepository.searchAndSort(transactionsFilterParams);
        return Paginator.paginate(pageable, transactions);
    }

    @Override
    public Page<Transaction> loadUserPastTransactions(int page,
                                                      int size,
                                                      User user,
                                                      Optional<Integer> walletId) {

        TransactionsFilterParams transactionsFilterParams;
        transactionsFilterParams = transactionMapper.mapIncomingUserPaymentsToFilterParams(user, walletId, DATE_DESC);
        List<Transaction> transactionList = new ArrayList<>(transactionRepository.searchAndSort(transactionsFilterParams));

        transactionsFilterParams = transactionMapper.mapOutgoingUserPaymentsToFilterParams(user, walletId, DATE_DESC);
        transactionList.addAll(transactionRepository.searchAndSort(transactionsFilterParams));

        transactionList = transactionList.stream().distinct().collect(Collectors.toList());
        transactionList.sort(Comparator.comparing(Transaction::getTransactionTime));

        return Paginator.paginate(PageRequest.of(page - 1, size), transactionList);
    }

    @Override
    public void create(Transaction transaction) {
        transactionRepository.create(transaction);
    }

}
