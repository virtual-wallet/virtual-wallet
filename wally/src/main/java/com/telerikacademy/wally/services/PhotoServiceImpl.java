package com.telerikacademy.wally.services;

import com.telerikacademy.wally.models.Photo;
import com.telerikacademy.wally.repositories.contracts.PhotoRepository;
import com.telerikacademy.wally.services.contracts.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PhotoServiceImpl extends BaseModifyServiceImpl<Photo, PhotoRepository>
        implements PhotoService {
    private final PhotoRepository photoRepository;

    @Autowired
    public PhotoServiceImpl(PhotoRepository baseModifyRepository, PhotoRepository photoRepository) {
        super(Photo.class, baseModifyRepository);
        this.photoRepository = photoRepository;
    }
}
