package com.telerikacademy.wally.services.contracts;

import com.telerikacademy.wally.models.Category;
import com.telerikacademy.wally.models.User;
import com.telerikacademy.wally.models.params.UserSearchParams;
import com.telerikacademy.wally.repositories.contracts.UserRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;
import java.util.List;

public interface UserService extends BaseGetService<User, UserRepository>{

    User create(User user);

    void update(User user,User userToAuthenticate);

    void delete(int userId,User userToAuthenticate);

    void sendVerificationEmail(User user,String siteURL) throws MessagingException, UnsupportedEncodingException;

    List<User> searchAndSort(UserSearchParams userSearchParams,User userToAuthenticate);

    Page<User> findPaginated(Pageable pageable, UserSearchParams userSearchParams,User userToAuthenticate);

    Page<User> findPaginated(Pageable pageable,User userToAuthenticate);

    void addOrModifyCategoryToUserCategories(Category category, User user);

    void deleteUserCategory(Category categoryToDelete, User user);

    boolean verify(String code);
}
