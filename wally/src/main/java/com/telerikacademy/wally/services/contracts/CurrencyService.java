package com.telerikacademy.wally.services.contracts;

import com.telerikacademy.wally.models.Card;
import com.telerikacademy.wally.models.Currency;
import com.telerikacademy.wally.models.Wallet;
import com.telerikacademy.wally.models.dtos.TransactionDto;
import com.telerikacademy.wally.repositories.contracts.CurrencyRepository;

public interface CurrencyService extends BaseModifyService<Currency, CurrencyRepository> {

    double calculateExchangeRate(Wallet walletFrom, Wallet walletTo);

    double calculateExchangeRate(Card payerCard, Wallet walletTo);

    double calculateExchangeRate(Wallet payerWallet, Currency payeeCurrency);

    double calculateExchangeRate(Currency currencyFrom, Wallet userWallet);
}
