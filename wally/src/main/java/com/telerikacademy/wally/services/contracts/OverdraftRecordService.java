package com.telerikacademy.wally.services.contracts;

import com.telerikacademy.wally.models.OverdraftRecord;
import com.telerikacademy.wally.repositories.contracts.OverdraftRecordRepository;

public interface OverdraftRecordService extends BaseModifyService<OverdraftRecord, OverdraftRecordRepository> {
}
