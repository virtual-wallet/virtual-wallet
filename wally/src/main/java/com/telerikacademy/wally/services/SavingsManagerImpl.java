package com.telerikacademy.wally.services;

import com.telerikacademy.wally.config.ExternalApiUrlConfig;
import com.telerikacademy.wally.exceptions.EntityNotFoundException;
import com.telerikacademy.wally.models.*;
import com.telerikacademy.wally.services.contracts.*;
import com.telerikacademy.wally.utils.mappers.TransactionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;

import static com.telerikacademy.wally.controllers.utils.Constants.STATUS_COMPLETED;
import static com.telerikacademy.wally.controllers.utils.Constants.STATUS_TERMINATED;
import static java.lang.Math.pow;

@Service
public class SavingsManagerImpl implements SavingsManager {
    private final SavingsFeatureService savingsFeatureService;
    private final SavingsRecordService savingsRecordService;
    private final WalletService walletService;
    private final ExternalApiUrlConfig externalApiUrlConfig;
    private final TransactionService transactionService;
    private final PaymentManager paymentManager;
    private final TransactionMapper transactionMapper;
    private final StatusService statusService;
    private final CurrencyService currencyService;

    @Autowired
    public SavingsManagerImpl(SavingsFeatureService savingsFeatureService,
                              SavingsRecordService savingsRecordService,
                              WalletService walletService,
                              ExternalApiUrlConfig externalApiUrlConfig,
                              TransactionService transactionService,
                              PaymentManager paymentManager,
                              TransactionMapper transactionMapper,
                              StatusService statusService,
                              CurrencyService currencyService) {
        this.savingsFeatureService = savingsFeatureService;
        this.savingsRecordService = savingsRecordService;
        this.walletService = walletService;
        this.externalApiUrlConfig = externalApiUrlConfig;
        this.transactionService = transactionService;
        this.paymentManager = paymentManager;
        this.transactionMapper = transactionMapper;
        this.statusService = statusService;
        this.currencyService = currencyService;
    }

    @Override
    public void setSavingsDeposit(SavingsRecord savingsRecord, User user) {
        Wallet userWallet = walletService.getById(savingsRecord.getWallet().getWalletId());
        Wallet bankMainAcc = walletService.getById(externalApiUrlConfig.getBankMainAcc());
        Status statusCompleted = statusService.getById(STATUS_COMPLETED);
        double depositAmount = savingsRecord.getBaseAmount();

        double exchangeRateUserToDepositCurrency = currencyService.calculateExchangeRate(userWallet, savingsRecord.getCurrency());

        Transaction transaction = transactionMapper
                .mapTransactionForSavingsPayment(bankMainAcc,
                        savingsRecord.getCurrency(),
                        userWallet,
                        statusCompleted,
                        depositAmount,
                        exchangeRateUserToDepositCurrency);
        //todo check funds availability
        paymentManager.commitSavingsDeposit(userWallet, bankMainAcc, depositAmount, transaction);
        savingsRecordService.create(savingsRecord);
    }

    //todo for testing purposes only
//    @Scheduled(cron = "*/5 * * * * *")
    @Scheduled(cron = "0 0 0 */1 * ?")
    void paybackCustomerDeposits() {
        Date today = java.sql.Date.valueOf(LocalDate.now());
        List<SavingsRecord> savingsRecords = null;
        try {
            savingsRecords = savingsRecordService.getAllByField("end_date", today);
        } catch (EntityNotFoundException e) {
            return;
        }

        Wallet bankMainAcc = walletService.getById(externalApiUrlConfig.getBankMainAcc());
        Status statusCompleted = statusService.getById(STATUS_COMPLETED);

        for (SavingsRecord record : savingsRecords) {
            Wallet userWallet = record.getWallet();
            int depositDuration = record.getDurationMonths();

            double savingsAccrued = pow((1 + (record.getAer() / (depositDuration * 100))), depositDuration) * record.getBaseAmount();

            createTransactionAndSetPayment(record, userWallet, bankMainAcc, statusCompleted, savingsAccrued);
        }
    }

    @Override
    public void deleteSavingsDeposit(int depositRecordId, User owner) {
        SavingsRecord record = savingsRecordService.getById(depositRecordId);
        Wallet userWallet = record.getWallet();
        Wallet bankMainAcc = walletService.getById(externalApiUrlConfig.getBankMainAcc());
        Status statusTerminated = statusService.getById(STATUS_TERMINATED);

        double monthlyRate = pow((1 + record.getAer() / 100), 1.0 / 12.0) - 1.0;
        double depositDuration = ChronoUnit.MONTHS.between(
                new java.sql.Date(record.getStartDate().getTime()).toLocalDate(), LocalDate.now());

        double savingsAccrued = pow((1 + monthlyRate), depositDuration) * record.getBaseAmount();

        createTransactionAndSetPayment(record, userWallet, bankMainAcc, statusTerminated, savingsAccrued);
        record.setEndDate(new Date());
    }

    private void createTransactionAndSetPayment(SavingsRecord record,
                                                Wallet userWallet,
                                                Wallet bankMainAcc,
                                                Status statusTerminated,
                                                double baseAmount) {
        double exchangeRateDepositToBank = currencyService.calculateExchangeRate(record.getCurrency(), bankMainAcc);
        double exchangeRateDepositToUser = currencyService.calculateExchangeRate(record.getCurrency(), userWallet);

        double savingsAccruedBankPerspective = baseAmount * exchangeRateDepositToBank;

        Transaction transaction = transactionMapper
                .mapTransactionForSavingsPayment(userWallet,
                        record.getCurrency(),
                        bankMainAcc,
                        statusTerminated,
                        baseAmount,
                        exchangeRateDepositToUser);

        paymentManager.paybackUserDeposit(bankMainAcc, userWallet, savingsAccruedBankPerspective, transaction);
        savingsRecordService.delete(record.getId());
    }

}
