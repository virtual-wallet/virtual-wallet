package com.telerikacademy.wally.services;

import com.telerikacademy.wally.exceptions.DuplicateEntityException;
import com.telerikacademy.wally.exceptions.EntityNotFoundException;
import com.telerikacademy.wally.models.User;
import com.telerikacademy.wally.repositories.contracts.BaseModifyRepository;
import com.telerikacademy.wally.services.contracts.BaseModifyService;

public abstract class BaseModifyServiceImpl<T, R extends BaseModifyRepository<T>>
        extends BaseGetServiceImpl<T, R>
        implements BaseModifyService<T, R> {

    private Class<T> clazz;

    public BaseModifyServiceImpl(Class<T> clazz,
                                 R baseModifyRepository) {
        super(baseModifyRepository);
        this.clazz = clazz;
    }

    @Override
    public void create(T entity) {
        if (baseModifyRepository.getAll().contains(entity)) {
            throw new DuplicateEntityException(clazz.getSimpleName());
        }
        baseModifyRepository.create(entity);
    }

    @Override
    public void create(T entity, User userToAuthenticateOrAuthorize) {
        AuthorizationHelper.isAdmin(userToAuthenticateOrAuthorize);
        create(entity);
    }

    @Override
    public void update(T entityToUpdate) {

        //todo verify
//        try {
//            if (baseModifyRepository.getAll().contains(entityToUpdate)) {
//                throw new DuplicateEntityException(clazz.getSimpleName());
//            }
//        } catch (EntityNotFoundException e) {
//            baseModifyRepository.update(entityToUpdate);
//        }
        baseModifyRepository.update(entityToUpdate);
    }
    @Override
    public void update(T entityToUpdate, User userToAuthenticateOrAuthorize) {
        AuthorizationHelper.isAdmin(userToAuthenticateOrAuthorize);
        update(entityToUpdate);
    }

    @Override
    public void delete(int id) {
        baseModifyRepository.delete(id);
    }

    @Override
    public void delete(int id, User userToAuthenticateOrAuthorize) {
        AuthorizationHelper.isAdmin(userToAuthenticateOrAuthorize);
        delete(id);
    }
}
