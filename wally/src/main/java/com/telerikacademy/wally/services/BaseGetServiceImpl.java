package com.telerikacademy.wally.services;

import com.telerikacademy.wally.models.User;
import com.telerikacademy.wally.repositories.contracts.BaseGetRepository;
import com.telerikacademy.wally.services.contracts.BaseGetService;

import java.util.List;

public abstract class BaseGetServiceImpl<T, R extends BaseGetRepository<T>>
        implements BaseGetService<T, R> {

    final R baseModifyRepository;

    public BaseGetServiceImpl(R baseGetRepository) {
        this.baseModifyRepository = baseGetRepository;
    }

    @Override
    public List<T> getAll() {
        return baseModifyRepository.getAll();
    }

    @Override
    public List<T> getAll(User userToAuthenticate) {
        AuthorizationHelper.isAdmin(userToAuthenticate);
        return getAll();
    }

    @Override
    public <V> List<T> getAllByField(String fieldName, V value) {
        return baseModifyRepository.getAllByField(fieldName, value);
    }

    @Override
    public <V> List<T> getAllByField(String fieldName, V value, User userToAuthenticate) {
        AuthorizationHelper.isAdmin(userToAuthenticate);
        return getAllByField(fieldName, value);
    }

    @Override
    public <V> T getByField(String fieldName, V value) {
        return baseModifyRepository.getByField(fieldName, value);
    }

    @Override
    public <V> T getByField(String fieldName, V value, User userToAuthenticate) {
        AuthorizationHelper.isAdmin(userToAuthenticate);
        return getByField(fieldName, value);
    }

    @Override
    public T getById(int id) {
        return baseModifyRepository.getById(id);
    }

    @Override
    public T getById(int id, User userToAuthenticate) {
        AuthorizationHelper.isAdmin(userToAuthenticate);
        return getById(id);
    }

    @Override
    public List<T> getAllById(int id) {
        return baseModifyRepository.getAllById(id);
    }

    @Override
    public List<T> getAllById(int id, User userToAuthenticate) {
        AuthorizationHelper.isAdmin(userToAuthenticate);
        return getAllById(id);
    }

}
