package com.telerikacademy.wally.services.contracts;

import com.telerikacademy.wally.models.Card;
import com.telerikacademy.wally.models.User;
import com.telerikacademy.wally.models.params.UserSearchParams;

import java.util.List;

public interface CardService {

    <V> List<Card> getAllByField(String fieldName, V value,int id,User userToAuthenticate);

    <V> Card getByField(String fieldName, V value,int id,User userToAuthenticate);

    <V> Card getByField(String fieldName, V value);

    Card getById(int id, int ownerId,User userToAuthenticate);

    List<Card> getAllById(int id, User userToAuthenticate);

    void create(Card card, User userToAuthenticate);

    void update(Card card, User userToAuthenticate);

    void delete(int id, int ownerId, User userToAuthenticate);
}
