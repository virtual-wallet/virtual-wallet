package com.telerikacademy.wally.services.contracts;

import com.telerikacademy.wally.models.OverdraftFeature;
import com.telerikacademy.wally.repositories.contracts.OverdraftFeatureRepository;

public interface OverdraftFeatureService extends BaseModifyService<OverdraftFeature, OverdraftFeatureRepository> {
}
