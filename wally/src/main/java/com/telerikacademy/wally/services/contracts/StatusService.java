package com.telerikacademy.wally.services.contracts;

import com.telerikacademy.wally.models.Status;
import com.telerikacademy.wally.repositories.contracts.BaseModifyRepository;
import com.telerikacademy.wally.repositories.contracts.StatusRepository;

import java.util.List;

public interface StatusService extends BaseModifyService<Status, StatusRepository> {

}
