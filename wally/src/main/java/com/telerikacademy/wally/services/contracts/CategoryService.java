package com.telerikacademy.wally.services.contracts;

import com.telerikacademy.wally.models.Category;
import com.telerikacademy.wally.models.User;
import com.telerikacademy.wally.repositories.contracts.CategoryRepository;

import java.util.Set;

public interface CategoryService extends BaseModifyService<Category, CategoryRepository> {
    Set<Category> getBaseCategories();

    Category getOrCreateCategoryIfExists(Category category, User user);

    void deleteIfNotUsed(Category categoryToDelete, User user);
}
