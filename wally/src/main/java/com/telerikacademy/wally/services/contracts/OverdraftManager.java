package com.telerikacademy.wally.services.contracts;

import com.telerikacademy.wally.models.Transaction;
import com.telerikacademy.wally.models.Wallet;

public interface OverdraftManager {
    void financeUserPayment(Transaction transactionDto, Wallet payerWallet, double payerWalletBalance);

    void paybackUserOverdraftToBankOnly(Wallet payeeWallet, double transferValue);
}
