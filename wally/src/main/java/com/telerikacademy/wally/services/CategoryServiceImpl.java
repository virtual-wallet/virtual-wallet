package com.telerikacademy.wally.services;

import com.telerikacademy.wally.exceptions.EntityNotFoundException;
import com.telerikacademy.wally.models.Category;
import com.telerikacademy.wally.models.User;
import com.telerikacademy.wally.repositories.contracts.CategoryRepository;
import com.telerikacademy.wally.services.contracts.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service
public class CategoryServiceImpl
        extends BaseModifyServiceImpl<Category, CategoryRepository>
        implements CategoryService {

    public static final int ID_UP_TO_WHICH_DEFAULT_CATEGORIES_ARE_SELECTED = 11;
    private final CategoryRepository categoryRepository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        super(Category.class, categoryRepository);
        this.categoryRepository = categoryRepository;
    }

    @Override
    public Set<Category> getBaseCategories() {
        var categories = categoryRepository.getAll();
        return categories.stream()
                .filter(category -> category.getId() <= ID_UP_TO_WHICH_DEFAULT_CATEGORIES_ARE_SELECTED)
                .collect(Collectors.toSet());
    }

    @Override
    public Category getOrCreateCategoryIfExists(Category category, User user) {
        try {
            category = categoryRepository.getByField("name", category.getName());
        } catch (EntityNotFoundException e) {
            categoryRepository.create(category);
            category = categoryRepository.getByField("name", category.getName());
        }
        return category;
    }

    @Override
    public void deleteIfNotUsed(Category categoryToDelete, User user) {
        Set<User> userSet = categoryToDelete.getUserSet();
        if (userSet.size() == 0) categoryRepository.delete(categoryToDelete.getId());
    }

}
