package com.telerikacademy.wally.services;

import com.telerikacademy.wally.models.Status;
import com.telerikacademy.wally.repositories.contracts.StatusRepository;
import com.telerikacademy.wally.services.contracts.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StatusServiceImpl
        extends BaseModifyServiceImpl<Status, StatusRepository>
        implements StatusService {
    
    private final StatusRepository statusRepository;

    @Autowired
    public StatusServiceImpl(StatusRepository statusRepository) {
        super(Status.class, statusRepository);
        this.statusRepository = statusRepository;
    }


}
