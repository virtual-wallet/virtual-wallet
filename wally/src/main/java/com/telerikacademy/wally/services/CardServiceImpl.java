package com.telerikacademy.wally.services;

import com.telerikacademy.wally.exceptions.DuplicateEntityException;
import com.telerikacademy.wally.exceptions.EntityNotFoundException;
import com.telerikacademy.wally.exceptions.InvalidDataInput;
import com.telerikacademy.wally.models.Card;
import com.telerikacademy.wally.models.User;
import com.telerikacademy.wally.repositories.contracts.CardRepository;
import com.telerikacademy.wally.services.contracts.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Date;
import java.util.List;

@Service
public class CardServiceImpl implements CardService {

    private final CardRepository cardRepository;

    @Autowired
    public CardServiceImpl(CardRepository cardRepository) {
        this.cardRepository = cardRepository;
    }


    @Override
    public <V> List<Card> getAllByField(String fieldName, V value, int id, User userToAuthenticate) {
        AuthorizationHelper.isAccountOwnerOrAdmin(id, userToAuthenticate);
        return cardRepository.getAllByField(fieldName, value);
    }

    @Override
    public <V> Card getByField(String fieldName, V value, int id, User userToAuthenticate) {
        AuthorizationHelper.isAccountOwnerOrAdmin(id, userToAuthenticate);
        return getByField(fieldName, value);
    }

    @Override
    public <V> Card getByField(String fieldName, V value) {
        return cardRepository.getByField(fieldName, value);
    }

    @Override
    public Card getById(int id, int ownerId, User userToAuthenticate) {
        //id of card
        AuthorizationHelper.isAccountOwnerOrAdmin(ownerId, userToAuthenticate);
        return cardRepository.getById(id);
    }

    @Override
    public List<Card> getAllById(int id, User userToAuthenticate) {
        AuthorizationHelper.isAccountOwnerOrAdmin(id, userToAuthenticate);
        return cardRepository.getAllById(id);
    }

    @Override
    public void create(Card card, User userToAuthenticate) {
        AuthorizationHelper.isAccountOwnerOrAdmin(card.getUser().getUserId(), userToAuthenticate);
        checkForDuplicate(card);
        checkDate(card.getExpireDate());
        cardRepository.create(card);
    }

    @Override
    public void update(Card card, User userToAuthenticate) {
        AuthorizationHelper.isAccountOwnerOrAdmin(card.getUser().getUserId(), userToAuthenticate);
        cardRepository.update(card);
    }

    @Override
    public void delete(int id, int ownerId, User userToAuthenticate) {
        AuthorizationHelper.isAccountOwnerOrAdmin(ownerId, userToAuthenticate);
        cardRepository.delete(id);
    }

    public void checkForDuplicate(Card card) {
        List<Card> cards = cardRepository.getAll();
        for (Card c : cards) {
            if (c.getCardNumber().equals(card.getCardNumber())) {
                throw new DuplicateEntityException("Card", "card_number", card.getCardNumber());
            }        }
    }

    public void checkDate(Date date) {
        if (date.before(Date.from(Instant.now()))) {
            throw new InvalidDataInput("Expire date");
        }
    }
}
