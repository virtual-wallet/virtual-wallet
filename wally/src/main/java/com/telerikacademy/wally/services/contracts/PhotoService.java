package com.telerikacademy.wally.services.contracts;

import com.telerikacademy.wally.models.Photo;
import com.telerikacademy.wally.repositories.contracts.PhotoRepository;

public interface PhotoService extends BaseModifyService<Photo, PhotoRepository> {
}
