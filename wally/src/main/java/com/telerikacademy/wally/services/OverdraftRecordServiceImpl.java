package com.telerikacademy.wally.services;

import com.telerikacademy.wally.models.OverdraftRecord;
import com.telerikacademy.wally.repositories.contracts.OverdraftRecordRepository;
import com.telerikacademy.wally.services.contracts.OverdraftRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OverdraftRecordServiceImpl
        extends BaseModifyServiceImpl<OverdraftRecord, OverdraftRecordRepository>
        implements OverdraftRecordService {

    private final OverdraftRecordRepository overdraftRecordRepository;

    @Autowired
    public OverdraftRecordServiceImpl(OverdraftRecordRepository overdraftRecordRepository) {
        super(OverdraftRecord.class, overdraftRecordRepository);
        this.overdraftRecordRepository = overdraftRecordRepository;
    }
}
