package com.telerikacademy.wally.services;

import com.telerikacademy.wally.models.Role;
import com.telerikacademy.wally.repositories.contracts.RoleRepository;
import com.telerikacademy.wally.services.contracts.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl
        extends BaseModifyServiceImpl<Role, RoleRepository>
        implements RoleService {

    private final RoleRepository roleRepository;

    @Autowired
    public RoleServiceImpl(RoleRepository roleRepository) {
        super(Role.class, roleRepository);
        this.roleRepository = roleRepository;
    }
}
