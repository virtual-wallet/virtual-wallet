package com.telerikacademy.wally.services;

import com.telerikacademy.wally.models.CardType;
import com.telerikacademy.wally.models.Role;
import com.telerikacademy.wally.repositories.contracts.CardRepository;
import com.telerikacademy.wally.repositories.contracts.CardTypeRepository;
import com.telerikacademy.wally.repositories.contracts.RoleRepository;
import com.telerikacademy.wally.services.contracts.CardTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CardTypeServiceImpl
        extends BaseModifyServiceImpl<CardType, CardTypeRepository>
        implements CardTypeService {

    private final CardTypeRepository cardTypeRepository;

    @Autowired
    public CardTypeServiceImpl(CardTypeRepository cardTypeRepository) {
        super(CardType.class, cardTypeRepository);
        this.cardTypeRepository = cardTypeRepository;
    }

}
