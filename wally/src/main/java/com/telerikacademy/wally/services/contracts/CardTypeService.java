package com.telerikacademy.wally.services.contracts;

import com.telerikacademy.wally.models.Card;
import com.telerikacademy.wally.models.CardType;
import com.telerikacademy.wally.repositories.contracts.CardTypeRepository;

import java.util.List;

public interface CardTypeService extends BaseModifyService<CardType, CardTypeRepository>,BaseGetService<CardType, CardTypeRepository> {

}
