package com.telerikacademy.wally.services;

import com.telerikacademy.wally.exceptions.DuplicateEntityException;
import com.telerikacademy.wally.exceptions.EntityNotFoundException;
import com.telerikacademy.wally.models.Category;
import com.telerikacademy.wally.models.User;
import com.telerikacademy.wally.models.params.UserSearchParams;
import com.telerikacademy.wally.repositories.contracts.UserRepository;
import com.telerikacademy.wally.services.contracts.CategoryService;
import com.telerikacademy.wally.services.contracts.UserService;
import com.telerikacademy.wally.utils.Paginator;
import net.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UserServiceImpl extends BaseGetServiceImpl<User,UserRepository> implements UserService  {

    private final UserRepository userRepository;
    private final CategoryService categoryService;
    private final JavaMailSender mailSender;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           CategoryService categoryService,
                           JavaMailSender mailSender) {
        super(userRepository);
        this.userRepository = userRepository;
        this.categoryService = categoryService;
        this.mailSender = mailSender;
    }

    @Override
    public User create(User user) {
        checkForDuplicate("username", user.getUsername());
        checkForDuplicate("email", user.getEmail());
        checkForDuplicate("phone", user.getPhone());

        user.setEnabled(false);
        String randomCode = RandomString.make(64);
        user.setVerificationCode(randomCode);

        return userRepository.create(user);

    }

    @Override
    public void sendVerificationEmail(User user, String siteURL) throws MessagingException, UnsupportedEncodingException {
        String subject = "Please verify your registration";
        String senderName = "Wally";
        String verifyURL = siteURL + "/auth/verify?code=" + user.getVerificationCode();
        String mailContent = "Dear " + user.getUsername() + ", \n" +
                "Please click the link below to verify your registration! \n" +
                "<a href=\"" + verifyURL + "\">VERIFY</a> \n" +
                "Thank you. \n" +
                "The Wally Team";
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setFrom("telerikwally@gmail.com", senderName);
        helper.setTo(user.getEmail());
        helper.setSubject(subject);

        helper.setText(mailContent, true);

        mailSender.send(message);
    }

    @Override
    public void update(User user,User userToAuthenticate) {
        AuthorizationHelper.isAccountOwnerOrAdmin(user.getUserId(),userToAuthenticate);
        userRepository.update(user);
    }

    @Override
    public void delete(int userId,User userToAuthenticate) {
        AuthorizationHelper.isAccountOwnerOrAdmin(userId,userToAuthenticate);
        userRepository.delete(userId);
    }

    @Override
    public List<User> searchAndSort(UserSearchParams userSearchParams,User userToAuthenticate) {
        AuthorizationHelper.isAdmin(userToAuthenticate);
        return userRepository.searchAndSort(userSearchParams);
    }

    @Override
    public Page<User> findPaginated(Pageable pageable,User userToAuthenticate) {
        AuthorizationHelper.isAdmin(userToAuthenticate);
        List<User> users = getAll(userToAuthenticate);
        return Paginator.paginate(pageable, users);
    }

    @Override
    public Page<User> findPaginated(Pageable pageable, UserSearchParams userSearchParams,User userToAuthenticate) {
        AuthorizationHelper.isAdmin(userToAuthenticate);
        List<User> users = userRepository.searchAndSort(userSearchParams);
        return Paginator.paginate(pageable, users);
    }

    public void checkForDuplicate(String field, String value) {
        try {
            if (userRepository.getAll().contains(userRepository.getByField(field, value))) {
                throw new DuplicateEntityException("User", field, value);
            }
        } catch (EntityNotFoundException ignore) {}
    }

    @Override
    public void addOrModifyCategoryToUserCategories(Category category, User user) {
        AuthorizationHelper.isAccountOwnerOrAdmin(user.getUserId(),user);
        Category newCategory = categoryService.getOrCreateCategoryIfExists(category, user);
        Set<Category> categorySet = new HashSet<Category>(user.getCategoriesSet());
        categorySet.add(newCategory);
        user.setCategoriesSet(categorySet);
        userRepository.update(user);
    }

    @Override
    public void deleteUserCategory(Category categoryToDelete, User user) {
        AuthorizationHelper.isAccountOwnerOrAdmin(user.getUserId(), user);
        Set<Category> categorySet = new HashSet<>(user.getCategoriesSet());
        categorySet.remove(categoryToDelete);
        user.setCategoriesSet(categorySet);
        userRepository.update(user);

        categoryService.deleteIfNotUsed(categoryToDelete, user);
    }

    @Override
    public boolean verify(String verificationCode) {
        User user = userRepository.findByVerificationCode(verificationCode);
        if (user == null || user.isEnabled()) {
            return false;
        } else {
            user.setEnabled(true);
            userRepository.update(user);
            return true;
        }
    }

}
