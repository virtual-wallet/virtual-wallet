package com.telerikacademy.wally.services.contracts;

import com.telerikacademy.wally.models.Transaction;
import com.telerikacademy.wally.models.User;
import com.telerikacademy.wally.models.dtos.TransactionDto;
import com.telerikacademy.wally.models.params.TransactionsFilterParams;
import com.telerikacademy.wally.repositories.contracts.TransactionRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface TransactionService extends BaseModifyService<Transaction, TransactionRepository> {

    List<Transaction> searchAndSort(TransactionsFilterParams transactionsFilterParams);

    Page<Transaction> findPaginated(Pageable pageable,User user);

    Page<Transaction> findPaginated(Pageable pageable, TransactionsFilterParams transactionsFilterParams,User user);

    void create(Transaction transaction);

    Page<Transaction> loadUserPastTransactions(int page, int size, User user, Optional<Integer> wallet);

}
