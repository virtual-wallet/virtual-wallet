package com.telerikacademy.wally.services.contracts;

import com.telerikacademy.wally.models.User;
import com.telerikacademy.wally.repositories.contracts.BaseGetRepository;

import java.util.List;

public interface BaseGetService<T, R extends BaseGetRepository<T>> {
    List<T> getAll();

    List<T> getAll(User userToAuthenticate);

    <V> List<T> getAllByField(String fieldName, V value);

    <V> List<T> getAllByField(String fieldName, V value, User userToAuthenticate);

    <V> T getByField(String fieldName, V value);

    <V> T getByField(String fieldName, V value, User userToAuthenticate);

    T getById(int id);

    T getById(int id, User userToAuthenticate);

    List<T> getAllById(int id);

    List<T> getAllById(int id, User userToAuthenticate);
}
