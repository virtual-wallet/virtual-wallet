package com.telerikacademy.wally.services.contracts;

import com.telerikacademy.wally.models.SavingsFeature;
import com.telerikacademy.wally.repositories.contracts.SavingsFeatureRepository;

public interface SavingsFeatureService extends BaseModifyService<SavingsFeature, SavingsFeatureRepository> {
}
