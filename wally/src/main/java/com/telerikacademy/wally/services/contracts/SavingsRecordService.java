package com.telerikacademy.wally.services.contracts;

import com.telerikacademy.wally.models.SavingsRecord;
import com.telerikacademy.wally.repositories.contracts.SavingsRecordRepository;


public interface SavingsRecordService extends BaseModifyService<SavingsRecord, SavingsRecordRepository> {
}
