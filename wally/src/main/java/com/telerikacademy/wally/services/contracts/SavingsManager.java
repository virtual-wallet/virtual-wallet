package com.telerikacademy.wally.services.contracts;

import com.telerikacademy.wally.models.SavingsRecord;
import com.telerikacademy.wally.models.User;

public interface SavingsManager {
    void setSavingsDeposit(SavingsRecord savingsRecordsDto, User user);

    void deleteSavingsDeposit(int depositRecordId, User owner);
}
