package com.telerikacademy.wally.services;

import com.telerikacademy.wally.config.ExternalApiUrlConfig;
import com.telerikacademy.wally.exceptions.EntityNotFoundException;
import com.telerikacademy.wally.models.*;
import com.telerikacademy.wally.services.contracts.*;
import com.telerikacademy.wally.utils.mappers.TransactionMapper;
import com.telerikacademy.wally.utils.mappers.OverdraftMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.telerikacademy.wally.services.PaymentManagerImpl.SAME_CURRENCY_EXCHANGE_RATE;

@Service
public class OverdraftManagerImpl implements OverdraftManager {

    private final WalletService walletService;
    private final UserService userService;
    private final OverdraftRecordService overdraftRecordService;
    private final OverdraftMapper overdraftMapper;
    private final OverdraftFeatureService overdraftFeatureService;
    private final ExternalApiUrlConfig externalApiUrlConfig;
    private final TransactionService transactionService;
    private final TransactionMapper transactionMapper;
    private final CurrencyService currencyService;

    @Autowired
    public OverdraftManagerImpl(WalletService walletService,
                                UserService userService,
                                OverdraftRecordService overdraftRecordService,
                                OverdraftMapper overdraftMapper,
                                OverdraftFeatureService overdraftFeatureService,
                                ExternalApiUrlConfig externalApiUrlConfig,
                                TransactionService transactionService,
                                TransactionMapper transactionMapper,
                                CurrencyService currencyService) {
        this.walletService = walletService;
        this.userService = userService;
        this.overdraftRecordService = overdraftRecordService;
        this.overdraftMapper = overdraftMapper;
        this.overdraftFeatureService = overdraftFeatureService;
        this.externalApiUrlConfig = externalApiUrlConfig;
        this.transactionService = transactionService;
        this.transactionMapper = transactionMapper;
        this.currencyService = currencyService;
    }

    @Scheduled(cron = "0 0 0 1 * ?")
//    for tests only
//    @Scheduled(cron = "*/5 * * * * *")
    void manageUserOverdrafts() {
        List<OverdraftRecord> recordsOfLastMonthDebtors = null;
        try {
            recordsOfLastMonthDebtors = overdraftRecordService.getAllByField("isPaid", false);
        } catch (EntityNotFoundException e) {
            return;
        }
        User bankAuthorization = userService.getById(externalApiUrlConfig.getBankMainAcc());

        managePastUserOverdraftRecords(recordsOfLastMonthDebtors, bankAuthorization);

        List<Wallet> walletsInDept = walletService.getAllInDept();
        List<OverdraftRecord> updatedRecordsOfLastMonthDebtors = overdraftRecordService.getAllByField("isPaid", false);
        manageCurrentUserOverdraftRecords(updatedRecordsOfLastMonthDebtors, walletsInDept, bankAuthorization);

        List<OverdraftRecord> currentRecordsOfDebtors = overdraftRecordService.getAllByField("isPaid", false);
        accrueApr(currentRecordsOfDebtors, bankAuthorization);
    }

    private void managePastUserOverdraftRecords(List<OverdraftRecord> recordsOfLastMonthDebtors, User bankAuthorization) {
        List<User> usersToUpdate = new ArrayList<>();
        List<OverdraftRecord> overdraftRecordsToUpdate = new ArrayList<>();

        for (OverdraftRecord record : recordsOfLastMonthDebtors) {
            if (record.getWallet().getBalance() < 0) {
                if ((record.getUnpaidMonths() + 1) >= record.getOverdraftFeature().getMaxMonthsWithoutPayment()) {
                    User user = record.getWallet().getOwner();
                    if (!user.isBlocked()) {
                        user.setBlocked(true);
                        usersToUpdate.add(user);
                    }
                }
            } else {
                User user = record.getWallet().getOwner();
                if (user.isBlocked()) {
                    user.setBlocked(false);
                    usersToUpdate.add(user);
                }

                record.setPaid(true);
                overdraftRecordsToUpdate.add(record);
            }
        }

        if (usersToUpdate.size() > 0) {
            usersToUpdate.forEach(user -> userService.update(user, bankAuthorization));
        }

        if (overdraftRecordsToUpdate.size() > 0) {
            overdraftRecordsToUpdate.forEach(overdraftRecord -> overdraftRecordService.update(overdraftRecord, bankAuthorization));
        }
    }

    private void manageCurrentUserOverdraftRecords(List<OverdraftRecord> recordsOfLastMonthDebtors, List<Wallet> walletsInDept, User bankAuthorization) {
        List<Wallet> walletsToStartOverdraftRecord = new ArrayList<>(walletsInDept);

        for (OverdraftRecord record : recordsOfLastMonthDebtors) {
            if (walletsToStartOverdraftRecord.contains(record.getWallet())) {
                int tempMonthUnpaid;
                tempMonthUnpaid = record.getUnpaidMonths();
                record.setUnpaidMonths(++tempMonthUnpaid);

                walletsToStartOverdraftRecord.remove(record.getWallet());
            }
            overdraftRecordService.update(record, bankAuthorization);
        }
        createOverdraftRecord(walletsToStartOverdraftRecord, bankAuthorization);
    }

    private void createOverdraftRecord(List<Wallet> walletWithNewOverdraft, User bankAuthorization) {
        List<OverdraftRecord> newOverdraftRecords = new ArrayList<>();
        OverdraftFeature currentOverdraftPlan = overdraftFeatureService
                .getByField("name", externalApiUrlConfig.getCurrentOverdraftPlan());

        for (Wallet wallet : walletWithNewOverdraft) {
            OverdraftRecord record = overdraftMapper.fromWallet(wallet, currentOverdraftPlan);
            newOverdraftRecords.add(record);
        }

        for (OverdraftRecord record : newOverdraftRecords) {
            overdraftRecordService.create(record, bankAuthorization);
        }
    }

    private void accrueApr(List<OverdraftRecord> overdraftRecord, User user) {
        double tempBalance;
        double tempApr;
        double newTempBalance;

        for (OverdraftRecord record : overdraftRecord) {
            tempBalance = record.getWallet().getBalance();
            tempApr = record.getOverdraftFeature().getApr() / 100;

            newTempBalance = (tempApr / 12 + 1) * tempBalance;
            record.getWallet().setBalance(newTempBalance);

            walletService.update(record.getWallet(), record.getWallet().getOwner());
        }
    }

    @Override
    public void financeUserPayment(Transaction transaction, Wallet payerWallet, double payerWalletBalance) {
        double transferValue = transaction.getTransferValue();
        payerWalletBalance -= transferValue;
        payerWallet.setBalance(payerWalletBalance);

        Wallet payeeWallet = transaction.getToWallet();
        double payeeBalance = payeeWallet.getBalance();
        double newPayeeBalance;

        double exchangeRate = currencyService.calculateExchangeRate(payeeWallet, payerWallet);
        transferValue = transferValue * exchangeRate;

        newPayeeBalance = payeeBalance + transferValue;
        payeeWallet.setBalance(newPayeeBalance);

        Wallet bankWallet = walletService.getById(externalApiUrlConfig.getBankMainAcc());
        double bankBalance = bankWallet.getBalance();
        double newBankBalance;

        exchangeRate = currencyService.calculateExchangeRate(payeeWallet, bankWallet);
        transferValue = transferValue * exchangeRate;

        if (payeeBalance >= 0) {
            newBankBalance = bankBalance - transferValue;
            bankWallet.setBalance(newBankBalance);
        } else if (transferValue + payeeBalance > 0) {
            newBankBalance = bankBalance - (transferValue + payeeBalance);
            bankWallet.setBalance(newBankBalance);
        }

        List<Wallet> walletsToUpdate = new ArrayList<>(Arrays.asList(bankWallet, payeeWallet, payerWallet));
        for (Wallet wallet : walletsToUpdate) {
            walletService.update(wallet);
        }
    }

    @Override
    public void paybackUserOverdraftToBankOnly(Wallet payeeWallet, double transferValuePayeeReference) {
        User bankAuthorization = userService.getById(Integer.parseInt(externalApiUrlConfig.getBankUserId()));
        Wallet bankWallet = walletService.getById(externalApiUrlConfig.getBankMainAcc());
        double bankBalance = bankWallet.getBalance();
        double payeeBalance = payeeWallet.getBalance();
        double exchangeRate = currencyService.calculateExchangeRate(payeeWallet, bankWallet);
        double newBankBalance;

        transferValuePayeeReference = transferValuePayeeReference * exchangeRate;

        if (transferValuePayeeReference + payeeBalance < 0) {
            newBankBalance = bankBalance + transferValuePayeeReference;
        } else {
            newBankBalance = bankBalance + payeeBalance;
        }
        bankWallet.setBalance(newBankBalance);

        walletService.update(bankWallet, bankWallet.getOwner());
    }

}
