package com.telerikacademy.wally;

import com.telerikacademy.wally.config.ExternalApiUrlConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class WallyApplication {

    public static void main(String[] args) {
        SpringApplication.run(WallyApplication.class, args);
    }

}