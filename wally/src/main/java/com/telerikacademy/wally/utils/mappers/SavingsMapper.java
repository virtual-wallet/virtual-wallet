package com.telerikacademy.wally.utils.mappers;

import com.telerikacademy.wally.models.Currency;
import com.telerikacademy.wally.models.SavingsFeature;
import com.telerikacademy.wally.models.SavingsRecord;
import com.telerikacademy.wally.models.Wallet;
import com.telerikacademy.wally.models.dtos.SavingsFeatureDto;
import com.telerikacademy.wally.models.dtos.SavingsRecordsDto;
import com.telerikacademy.wally.models.dtos.SavingsSetUpDto;
import com.telerikacademy.wally.services.contracts.CurrencyService;
import com.telerikacademy.wally.services.contracts.SavingsFeatureService;
import com.telerikacademy.wally.services.contracts.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Date;


@Component
public class SavingsMapper {

    public final SavingsFeatureService savingsFeatureService;
    public final WalletService walletService;
    public final CurrencyService currencyService;

    @Autowired
    public SavingsMapper(SavingsFeatureService savingsFeatureService,
                         WalletService walletService,
                         CurrencyService currencyService) {
        this.savingsFeatureService = savingsFeatureService;
        this.walletService = walletService;
        this.currencyService = currencyService;
    }

    public SavingsFeature fromDto(SavingsFeatureDto dto) {
        SavingsFeature savingsFeature = new SavingsFeature();
        savingsFeature.setAer(dto.getAer());
        savingsFeature.setDuration(dto.getDurationMonths());
        savingsFeature.setName(dto.getName());
        return savingsFeature;
    }

    public SavingsFeature fromDto(SavingsFeatureDto dto, int id) {
        SavingsFeature savingsFeature = fromDto(dto);
        savingsFeature.setId(id);
        return savingsFeature;
    }

    public SavingsFeatureDto toDto(SavingsFeature savingsFeature) {
        SavingsFeatureDto dto = new SavingsFeatureDto();
        dto.setAer(savingsFeature.getAer());
        dto.setDurationMonths(savingsFeature.getDuration());
        dto.setName(savingsFeature.getName());
        return dto;
    }

    public SavingsRecord fromDto(SavingsRecordsDto dto) {
        SavingsRecord savingsRecord = new SavingsRecord();
        SavingsFeature savingsFeature = savingsFeatureService.getById(dto.getSavingsTypeID());
        Wallet wallet = walletService.getById(dto.getWalletId());
        Currency currency = currencyService.getById(dto.getCurrencyId());
        LocalDate endLocalDate = LocalDate.now().plusMonths(dto.getDurationMonths());
        Date endDate = java.sql.Date.valueOf(endLocalDate);

        savingsRecord.setSavingsFeature(savingsFeature);
        savingsRecord.setWallet(wallet);
        savingsRecord.setBaseAmount(dto.getBaseAmount());
        savingsRecord.setAer(dto.getAer());
        savingsRecord.setDurationMonths(dto.getDurationMonths());
        savingsRecord.setCurrency(currency);
        savingsRecord.setEndDate(endDate);
        savingsRecord.setStartDate(new Date());
        return savingsRecord;
    }

    public SavingsRecord fromDto(SavingsRecordsDto dto, int id) {
        SavingsRecord savingsRecord = fromDto(dto);
        savingsRecord.setId(id);
        return savingsRecord;
    }

    public SavingsRecordsDto toDto(SavingsRecord savingsRecord) {
        SavingsRecordsDto dto = new SavingsRecordsDto();

        dto.setWalletId(savingsRecord.getWallet().getWalletId());
        dto.setSavingsTypeID(savingsRecord.getSavingsFeature().getId());
        dto.setAer(savingsRecord.getAer());
        dto.setDurationMonths(savingsRecord.getDurationMonths());
        dto.setBaseAmount(savingsRecord.getBaseAmount());
        dto.setCurrencyId(savingsRecord.getCurrency().getId());
        return dto;
    }

    public SavingsRecord fromDto(SavingsSetUpDto dto) {
        SavingsRecord savingsRecord = new SavingsRecord();
        SavingsFeature savingsFeature = savingsFeatureService.getById(dto.getSavingsTypeID());
        Wallet wallet = walletService.getById(dto.getWalletId());
        Currency currency = currencyService.getById(dto.getCurrencyId());
        double aer = savingsFeature.getAer();
        int durationMonths = savingsFeature.getDuration();

        LocalDate endLocalDate = LocalDate.now().plusMonths(durationMonths);
        Date endDate = java.sql.Date.valueOf(endLocalDate);

        savingsRecord.setSavingsFeature(savingsFeature);
        savingsRecord.setWallet(wallet);
        savingsRecord.setBaseAmount(dto.getBaseAmount());
        savingsRecord.setAer(aer);
        savingsRecord.setDurationMonths(durationMonths);
        savingsRecord.setCurrency(currency);
        savingsRecord.setEndDate(endDate);
        savingsRecord.setStartDate(new Date());
        return savingsRecord;
    }

    public void mapSavingsRecordNewWallet(SavingsRecord savingsRecord, int newWalletId){
        Wallet wallet = walletService.getById(newWalletId);
        savingsRecord.setWallet(wallet);
    }
}
