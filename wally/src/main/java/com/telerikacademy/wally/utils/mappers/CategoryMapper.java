package com.telerikacademy.wally.utils.mappers;

import com.telerikacademy.wally.models.Category;
import com.telerikacademy.wally.models.dtos.NameDto;
import org.springframework.stereotype.Component;

@Component
public class CategoryMapper {

    public Category fromDto(NameDto dto){
        Category category = new Category();
        category.setName(dto.getValue());
        return category;
    }

    public NameDto toDto(Category category) {
        NameDto nameDto = new NameDto();
        nameDto.setValue(category.getName());
        return nameDto;
    }
}
