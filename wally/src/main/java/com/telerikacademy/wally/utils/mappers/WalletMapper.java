package com.telerikacademy.wally.utils.mappers;

import com.telerikacademy.wally.models.*;
import com.telerikacademy.wally.models.dtos.WalletDto;
import com.telerikacademy.wally.repositories.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WalletMapper {
    private final UserRepository userRepository;
    private final WalletRepository walletRepository;
    private final CurrencyRepository currencyRepository;
    private final CardRepository cardRepository;

    @Autowired
    public WalletMapper(UserRepository userRepository, WalletRepository walletRepository, CurrencyRepository currencyRepository, CardRepository cardRepository) {
        this.userRepository = userRepository;
        this.walletRepository = walletRepository;
        this.currencyRepository = currencyRepository;
        this.cardRepository = cardRepository;
    }

    public WalletDto toDto(Wallet wallet) {
        WalletDto walletDto = new WalletDto();
        walletDto.setOwnerId(wallet.getOwner().getUserId());
        walletDto.setCurrencyId(wallet.getCurrency().getId());
        walletDto.setHasOverdraft(wallet.hasOverdraftEnabled());
        return walletDto;
    }

    public Wallet fromDto(WalletDto walletDto) {
        Wallet wallet = new Wallet();
        dtoToObject(walletDto, wallet);
        return wallet;
    }

    public Wallet fromDto(WalletDto walletDto, int id) {
        Wallet wallet = walletRepository.getById(id);
        dtoToObject(walletDto, wallet);
        return wallet;
    }

    private void dtoToObject(WalletDto walletDto, Wallet wallet) {
        User owner = userRepository.getById(walletDto.getOwnerId());
        Currency currency = currencyRepository.getById(walletDto.getCurrencyId());
     //   Card card = cardRepository.getById(walletDto.getCardId());

        //todo fix with bank api
        wallet.setOwner(owner);
        wallet.setCurrency(currency);
        wallet.setBalance(0);
        wallet.setHasOverdraftEnabled(false);
     //   wallet.setCardSet(Set.of(card));
    }

}
