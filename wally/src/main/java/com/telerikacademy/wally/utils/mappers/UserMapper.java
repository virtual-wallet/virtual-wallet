package com.telerikacademy.wally.utils.mappers;

import com.telerikacademy.wally.models.Category;
import com.telerikacademy.wally.models.Role;
import com.telerikacademy.wally.models.User;
import com.telerikacademy.wally.models.dtos.*;
import com.telerikacademy.wally.models.dtos.RegisterDto;
import com.telerikacademy.wally.models.dtos.UserDto;
import com.telerikacademy.wally.models.dtos.UserFilterSearchSortDto;
import com.telerikacademy.wally.models.enums.UserSortOptions;
import com.telerikacademy.wally.models.params.UserSearchParams;
import com.telerikacademy.wally.repositories.contracts.RoleRepository;
import com.telerikacademy.wally.repositories.contracts.UserRepository;
import com.telerikacademy.wally.services.contracts.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.Set;

@Component
public class UserMapper {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final CategoryService categoryService;

    @Autowired
    public UserMapper(UserRepository userRepository,
                      RoleRepository roleRepository,
                      CategoryService categoryService) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.categoryService = categoryService;
    }

    public User fromDto(UserDto userDto) {
        User user = new User();
        dtoToObject(userDto, user);
        return user;
    }

    public User fromDto(UserDto userDto, int id) {
        User user = userRepository.getById(id);
        dtoToObject(userDto, user);
        return user;
    }

    public User fromDto(RegisterDto registerDto) {
        User user = new User();
        Role role = roleRepository.getById(1);
        Set<Category> categorySet = categoryService.getBaseCategories();

        user.setUsername(registerDto.getUsername());
        user.setPassword(registerDto.getPassword());
        user.setEmail(registerDto.getEmail());
        user.setPhone(registerDto.getPhone());
        user.setRoleSet(Set.of(role));
        user.setCategoriesSet(categorySet);

        return user;
    }

    public User fromDto(PasswordDto passwordDto, int id) {
        User user = userRepository.getById(id);
        user.setPassword(passwordDto.getPassword());
        return user;
    }

    public User fromDto(UpdateUserDto userDto, int id) {
        User user = userRepository.getById(id);
        user.setEmail(userDto.getEmail());
        user.setPhone(userDto.getPhone());

        return user;

    }


    public UserSearchParams fromDto(UserFilterSearchSortDto dto) {
        UserSearchParams userSearchParams = new UserSearchParams();

        String phone = dto.getPhone() != null && !dto.getPhone().equals("") ?
                dto.getPhone().trim() : null;
        userSearchParams.setPhone(phone);

        String email = dto.getEmail() != null && !dto.getEmail().equals("") ?
                dto.getEmail().trim() : null;
        userSearchParams.setEmail(email);

        String username = dto.getUsername() != null && !dto.getUsername().equals("") ?
                dto.getUsername().trim() : null;
        userSearchParams.setUsername(username);

        String all = dto.getAll() != null && !dto.getAll().equals("") ?
                dto.getAll().trim() : null;
        userSearchParams.setAll(all);

        userSearchParams.setSortOptions(UserSortOptions.valueOfPreview(dto.getSort()));

        return userSearchParams;
    }

    private void dtoToObject(UserDto userDto, User user) {
        user.setUsername(userDto.getUsername());
        user.setPassword(userDto.getPassword());
        user.setEmail(userDto.getEmail());
        user.setPhone(userDto.getPhone());
        Role role = roleRepository.getById(1);
        user.setRoleSet(Set.of(role));

    }

    public UserDto toDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setUsername(user.getUsername());
        userDto.setPassword(user.getPassword());
        userDto.setEmail(user.getEmail());
        userDto.setPhone(user.getPhone());
        return userDto;
    }

    public UpdateUserDto updatedUserToDto(User user) {
        UpdateUserDto userDto = new UpdateUserDto();
        userDto.setEmail(user.getEmail());
        userDto.setPhone(user.getPhone());
        return userDto;
    }

    public PasswordDto passwordToDto(User user) {
        PasswordDto passwordDto = new PasswordDto();
        passwordDto.setPassword(user.getPassword());
        return passwordDto;
    }

    public UserFilterSearchSortDto toDto(Optional<String> sort,
                                         Optional<String> username,
                                         Optional<String> phone,
                                         Optional<String> email,
                                         Optional<String> all) {
        UserFilterSearchSortDto dto = new UserFilterSearchSortDto();

        email.ifPresent(dto::setEmail);
        phone.ifPresent(dto::setPhone);
        username.ifPresent(dto::setUsername);
        sort.ifPresent(dto::setSort);
        all.ifPresent(dto::setAll);

        return dto;
    }

}
