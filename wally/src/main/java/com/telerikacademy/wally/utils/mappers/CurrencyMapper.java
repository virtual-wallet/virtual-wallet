package com.telerikacademy.wally.utils.mappers;

import com.telerikacademy.wally.models.Currency;
import com.telerikacademy.wally.models.dtos.CurrencyDto;
import org.springframework.stereotype.Component;

@Component
public class CurrencyMapper {

    public Currency fromDto(CurrencyDto dto) {
        Currency currency = new Currency();
        currency.setExchangeRate(dto.getExchangeRate());
        currency.setCurrencyName(dto.getCurrencyName());
        currency.setIso(dto.getIso());
        currency.setSymbol(dto.getSymbol());
        return currency;
    }

    public Currency fromDto(CurrencyDto dto, int id) {
        Currency currency = fromDto(dto);
        currency.setId(id);
        return currency;
    }

    public CurrencyDto toDto(Currency currency) {
        CurrencyDto dto = new CurrencyDto();
        dto.setCurrencyName(currency.getCurrencyName());
        dto.setExchangeRate(currency.getExchangeRate());
        dto.setIso(currency.getIso());
        dto.setSymbol(currency.getSymbol());
        dto.setId(currency.getId());
        return dto;
    }
}
