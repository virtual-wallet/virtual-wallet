package com.telerikacademy.wally.utils.mappers;

import com.telerikacademy.wally.models.Card;
import com.telerikacademy.wally.models.CardType;
import com.telerikacademy.wally.models.Currency;
import com.telerikacademy.wally.models.User;
import com.telerikacademy.wally.models.dtos.CardDto;
import com.telerikacademy.wally.models.dtos.UpdateCardDto;
import com.telerikacademy.wally.repositories.contracts.CardRepository;
import com.telerikacademy.wally.repositories.contracts.CardTypeRepository;
import com.telerikacademy.wally.repositories.contracts.CurrencyRepository;
import com.telerikacademy.wally.repositories.contracts.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.telerikacademy.wally.controllers.utils.Parsers.parseCvvPresent;

@Component

public class CardMapper {
    private final CardRepository cardRepository;
    private final UserRepository userRepository;
    private final CardTypeRepository cardTypeRepository;
    private final CurrencyRepository currencyRepository;

    @Autowired
    public CardMapper(CardRepository cardRepository, UserRepository userRepository, CardTypeRepository cardTypeRepository, CurrencyRepository currencyRepository) {
        this.cardRepository = cardRepository;
        this.userRepository = userRepository;
        this.cardTypeRepository = cardTypeRepository;
        this.currencyRepository = currencyRepository;
    }

    public Card fromDto(CardDto cardDto) throws ParseException {
        Card card = new Card();
        dtoToObject(cardDto, card);
        return card;
    }

    public Card fromDto(CardDto cardDto, int id) throws ParseException {
        Card card = cardRepository.getById(id);
        dtoToObject(cardDto, card);
        return card;
    }

    public Card fromDto(UpdateCardDto cardDto, int id) throws ParseException {
        Card card = cardRepository.getById(id);
        commonDtoToCard(cardDto, card);
        return card;
    }

    public UpdateCardDto toUpdateDto(Card card) {
        UpdateCardDto cardDto = new UpdateCardDto();
        cardDto.setCardNumber(card.getCardNumber());
        cardDto.setCardTypeId(card.getCardType().getId());
        cardDto.setCvv(parseCvvPresent(card.getCvv()));
        cardDto.setCurrencyId(card.getCurrency().getId());
        cardDto.setExpireDate(card.getExpireDate().toString());
        cardDto.setCardHolder(card.getCardholder());
        return cardDto;
    }

    public CardDto toDto(Card card) {
        CardDto cardDto = new CardDto();
        cardDto.setCardNumber(card.getCardNumber());
        cardDto.setUserId(card.getUser().getUserId());
        cardDto.setCardTypeId(card.getCardType().getId());
        cardDto.setCvv(parseCvvPresent(card.getCvv()));
        cardDto.setCurrencyId(card.getCurrency().getId());
        cardDto.setExpireDate(card.getExpireDate().toString());
        cardDto.setCardHolder(card.getCardholder());
        return cardDto;
    }

    private void dtoToObject(CardDto cardDto, Card card) throws ParseException {
        User user = userRepository.getById(cardDto.getUserId());
        card.setUser(user);
        commonDtoToCard(cardDto, card);
    }

    private <E extends UpdateCardDto> void commonDtoToCard(E cardDto, Card card) throws ParseException {
        CardType cardType = cardTypeRepository.getById(cardDto.getCardTypeId());
        String date = cardDto.getExpireDate();
        Date date1 = new SimpleDateFormat("yyyy-MM").parse(date);
        Currency currency = currencyRepository.getById(cardDto.getCurrencyId());
        String cardNumber = cardDto.getCardNumber().replace(" ", "");
        card.setCurrency(currency);
        card.setCardNumber(cardNumber);
        card.setCvv(Integer.parseInt(cardDto.getCvv()));
        card.setExpireDate(date1);
        card.setCardType(cardType);
        card.setCardholder(cardDto.getCardHolder());
    }
}
