package com.telerikacademy.wally.utils.mappers;

import com.telerikacademy.wally.exceptions.InvalidDataInput;
import com.telerikacademy.wally.models.OverdraftFeature;
import com.telerikacademy.wally.models.OverdraftRecord;
import com.telerikacademy.wally.models.Wallet;
import com.telerikacademy.wally.models.dtos.OverdraftFeatureDto;
import com.telerikacademy.wally.models.dtos.OverdraftRecordDto;
import com.telerikacademy.wally.services.contracts.OverdraftFeatureService;
import com.telerikacademy.wally.services.contracts.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Date;

import static com.telerikacademy.wally.utils.mappers.TransactionMapper.DATE_FORMAT_ERROR_MESSAGE;

@Component
public class OverdraftMapper {

    public static final int UNPAID_MONTHS_BEGINNING_AT = 0;
    private final OverdraftFeatureService overdraftFeatureService;
    public final WalletService walletService;

    @Autowired
    public OverdraftMapper(OverdraftFeatureService overdraftFeatureService,
                           WalletService walletService) {
        this.overdraftFeatureService = overdraftFeatureService;
        this.walletService = walletService;
    }

    public OverdraftFeature fromDto(OverdraftFeatureDto dto) {
        OverdraftFeature overdraftFeature = new OverdraftFeature();
        overdraftFeature.setName(dto.getName());
        overdraftFeature.setApr(dto.getApr());
        overdraftFeature.setMaxMonthsWithoutPayment(dto.getMaxMonths());
        return overdraftFeature;
    }

    public OverdraftFeature fromDto(OverdraftFeatureDto dto, int id) {
        OverdraftFeature overdraftFeature = fromDto(dto);
        overdraftFeature.setId(id);
        return overdraftFeature;
    }

    public OverdraftFeatureDto toDto(OverdraftFeature overdraftFeature) {
        OverdraftFeatureDto dto = new OverdraftFeatureDto();
        dto.setMaxMonths(overdraftFeature.getMaxMonthsWithoutPayment());
        dto.setApr(overdraftFeature.getApr());
        dto.setName(overdraftFeature.getName());
        return dto;
    }

    public OverdraftRecord fromDto(OverdraftRecordDto dto) {
        OverdraftRecord overdraftRecord = new OverdraftRecord();
        OverdraftFeature overdraftFeature = overdraftFeatureService.getById(dto.getOverdraftFeatureId());
        Wallet wallet = walletService.getById(dto.getWalletId());
        Date dateToday = Date.from(Instant.from(LocalDate.now()));

        overdraftRecord.setOverdraftFeature(overdraftFeature);
        overdraftRecord.setWallet(wallet);
        overdraftRecord.setUnpaidMonths(dto.getUnpaidMonths());
        overdraftRecord.setPaid(dto.getIsPaid());

        try {
            Date date = dto.getDateStarted() != null && !dto.getDateStarted().equals("") ?
                    new SimpleDateFormat("yyyy-MM-dd").parse(dto.getDateStarted()) : dateToday;
            overdraftRecord.setDateStarted(date);

        } catch (ParseException e) {
            throw new InvalidDataInput(DATE_FORMAT_ERROR_MESSAGE);
        }

        return overdraftRecord;
    }

    public OverdraftRecord fromDto(OverdraftRecordDto dto, int id) {
        OverdraftRecord overdraftRecord = fromDto(dto);
        overdraftRecord.setId(id);
        return overdraftRecord;
    }

    public OverdraftRecordDto toDto(OverdraftRecord overdraftRecord) {
        OverdraftRecordDto dto = new OverdraftRecordDto();

        dto.setIsPaid(overdraftRecord.isPaid());
        dto.setOverdraftFeatureId(overdraftRecord.getOverdraftFeature().getId());
        dto.setWalletId(overdraftRecord.getWallet().getWalletId());
        dto.setDateStarted(overdraftRecord.getDateStarted().toString());
        dto.setUnpaidMonths(overdraftRecord.getUnpaidMonths());
        return dto;
    }

    public OverdraftRecord fromWallet(Wallet wallet, OverdraftFeature currentOverdraftPlan) {
        OverdraftRecord record = new OverdraftRecord();

        record.setWallet(wallet);
        record.setOverdraftFeature(currentOverdraftPlan);
        record.setPaid(false);
        record.setDateStarted(new Date());
        record.setUnpaidMonths(UNPAID_MONTHS_BEGINNING_AT);
        return record;
    }
}
