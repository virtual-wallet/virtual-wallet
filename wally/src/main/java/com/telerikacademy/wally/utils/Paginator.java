package com.telerikacademy.wally.utils;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Collections;
import java.util.List;

public class Paginator<T> {
    public static <T> Page<T> paginate(Pageable pageable,
                                       List<T> transactions) {
        List<T> newPage;
        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;

        if (transactions.size() < startItem) {
            newPage = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, transactions.size());
            newPage = transactions.subList(startItem, toIndex);
        }

        return new PageImpl<>(newPage, PageRequest.of(currentPage, pageSize), transactions.size());
    }
}
