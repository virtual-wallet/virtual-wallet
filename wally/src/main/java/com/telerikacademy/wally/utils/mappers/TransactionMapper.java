package com.telerikacademy.wally.utils.mappers;

import com.telerikacademy.wally.exceptions.InvalidDataInput;
import com.telerikacademy.wally.models.*;
import com.telerikacademy.wally.models.dtos.CardDto;
import com.telerikacademy.wally.models.dtos.MockBankDto;
import com.telerikacademy.wally.models.dtos.TransactionDto;
import com.telerikacademy.wally.models.dtos.TransactionFilterSortDto;
import com.telerikacademy.wally.models.enums.TransactionSortOptions;
import com.telerikacademy.wally.models.enums.contracts.SortOptions;
import com.telerikacademy.wally.models.params.TransactionsFilterParams;
import com.telerikacademy.wally.repositories.contracts.UserRepository;
import com.telerikacademy.wally.services.contracts.CategoryService;
import com.telerikacademy.wally.services.contracts.CurrencyService;
import com.telerikacademy.wally.services.contracts.StatusService;
import com.telerikacademy.wally.services.contracts.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Optional;

import static com.telerikacademy.wally.controllers.utils.Constants.CATEGORY_SAVINGS;
import static com.telerikacademy.wally.controllers.utils.Constants.STATUS_COMPLETED;
import static com.telerikacademy.wally.models.enums.TransactionSortOptions.DATE_DESC;

@Component
public class TransactionMapper {

    public static final String DATE_FORMAT_ERROR_MESSAGE = "Wrong date format used. Please enter date in yyyy-MM.";
    public static final int CARD_TO_WALLET_EXCHANGE_RATE = 1;
    public static final int TRANSFER_CATEGORY = 1;

    private final UserRepository userRepository;
    private final WalletService walletService;
    private final CategoryService categoryService;
    private final StatusService statusService;
    private final CurrencyService currencyService;

    @Autowired
    public TransactionMapper(@Lazy UserRepository userRepository,
                             @Lazy WalletService walletService,
                             @Lazy CategoryService categoryService,
                             @Lazy StatusService statusService,
                             @Lazy CurrencyService currencyService) {
        this.userRepository = userRepository;
        this.walletService = walletService;
        this.categoryService = categoryService;
        this.statusService = statusService;
        this.currencyService = currencyService;
    }

    public Transaction fromDto(TransactionDto transactionDto, User user) {
        transactionDto.setFromUserId(user.getUserId());
        return fromDto(transactionDto);
    }

    public Transaction fromDto(TransactionDto transactionDto) {
        Transaction transaction = new Transaction();
        User sender = userRepository.getById(transactionDto.getFromUserId());
        Wallet senderWallet = walletService.getById(transactionDto.getFromWalletId());
        Wallet recipientWallet = walletService.getById(transactionDto.getToWalletId());
        Category category = categoryService.getById(transactionDto.getCategoryId());
        Status status = statusService.getById(STATUS_COMPLETED);
        double exchangeRate = currencyService.calculateExchangeRate(senderWallet, recipientWallet);

        transaction.setFromUser(sender);
        transaction.setFromWallet(senderWallet);
        transaction.setToWallet(recipientWallet);
        transaction.setCategory(category);
        transaction.setStatus(status);
        transaction.setTransferValue(transactionDto.getValue());
        transaction.setExchangeRate(exchangeRate);

        return transaction;
    }

    public TransactionsFilterParams fromDto(TransactionFilterSortDto dto) {
        var transactionsFilterParams = new TransactionsFilterParams();

        try {
            Date dateFromParsed = dto.getDateFrom() != null && !dto.getDateFrom().equals("") ?
                    new SimpleDateFormat("yyyy-MM").parse(dto.getDateFrom()) : null;
            transactionsFilterParams.setDateFrom(dateFromParsed);

            Date dateToParsed = dto.getDateTo() != null && !dto.getDateTo().equals("") ?
                    new SimpleDateFormat("yyyy-MM").parse(dto.getDateTo()) : null;
            transactionsFilterParams.setDateTo(dateToParsed);

        } catch (ParseException e) {
            throw new InvalidDataInput(DATE_FORMAT_ERROR_MESSAGE);
        }

        Boolean isCreditedSelection = null;
        if (dto.isCredited() != null && dto.isCredited().equals("1")) {
            isCreditedSelection = true;
        } else if (dto.isCredited() != null && dto.isCredited().equals("0")) {
            isCreditedSelection = false;
        }
        transactionsFilterParams.setIsCredited(isCreditedSelection);

        String recipientUsername = dto.getRecipientUsername() != null && !dto.getRecipientUsername().equals("-1")
                ? dto.getRecipientUsername() : null;
        transactionsFilterParams.setToUsername(recipientUsername);

        String senderUsername = dto.getSenderUsername() != null && !dto.getSenderUsername().equals("-1")
                ? dto.getSenderUsername() : null;
        transactionsFilterParams.setFromUsername(senderUsername);

        Integer fromWalletId = dto.getFromWalletId() != null && !dto.getFromWalletId().equals("")
                ? Integer.parseInt(dto.getFromWalletId()) : null;
        transactionsFilterParams.setFromWalletId(fromWalletId);

        Integer toWalletId = dto.getToWalletId() != null && !dto.getToWalletId().equals("")
                ? Integer.parseInt(dto.getToWalletId()) : null;
        transactionsFilterParams.setToWalletId(toWalletId);

        Integer status = dto.getStatus() != null && !dto.getStatus().equals("")
                ? Integer.parseInt(dto.getStatus()) : null;
        transactionsFilterParams.setStatus(status);

        Integer category = dto.getCategory() != null && !dto.getCategory().equals("")
                ? Integer.parseInt(dto.getCategory()) : null;
        transactionsFilterParams.setCategory(category);

        transactionsFilterParams.setSortOptions(TransactionSortOptions.valueOfPreview(dto.getSort()));
        return transactionsFilterParams;
    }

    public TransactionFilterSortDto toDto(Optional<String> senderUsername,
                                          Optional<String> recipientUsername,
                                          Optional<String> dateFrom,
                                          Optional<String> dateTo,
                                          Optional<String> sort,
                                          Optional<String> fromWalletId,
                                          Optional<String> toWalletId,
                                          Optional<String> isCredited,
                                          Optional<String> category,
                                          Optional<String> status) {

        TransactionFilterSortDto dto = new TransactionFilterSortDto();

        senderUsername.ifPresent(dto::setSenderUsername);
        recipientUsername.ifPresent(dto::setRecipientUsername);
        dateFrom.ifPresent(dto::setDateFrom);
        dateTo.ifPresent(dto::setDateTo);
        sort.ifPresent(dto::setSort);
        fromWalletId.ifPresent(dto::setFromWalletId);
        toWalletId.ifPresent(dto::setToWalletId);
        isCredited.ifPresent(dto::setIsCredited);
        category.ifPresent(dto::setCategory);
        status.ifPresent(dto::setStatus);

        return dto;
    }

    public MockBankDto toDto(CardDto cardDto, TransactionDto transactionDto) {
        MockBankDto mockBankDto = new MockBankDto();

        mockBankDto.setCvv(cardDto.getCvv());
        mockBankDto.setExpireDate(cardDto.getExpireDate());
        mockBankDto.setValue(transactionDto.getValue());

        return mockBankDto;
    }

    public Transaction fromDtoExternalCard(TransactionDto transactionDto, Wallet walletTo, User user) {
        Transaction transaction = new Transaction();
        Status transactionStatus = statusService.getById(transactionDto.getStatusId());
        Category transactionCategory = categoryService.getById(TRANSFER_CATEGORY);

        return mapTransactionFromPaymentDetails(transactionDto,
                user,
                transaction,
                walletTo,
                walletTo,
                transactionStatus,
                transactionCategory,
                transactionDto.getExchangeRate());
    }

    public Transaction fromDtoWalletToWallet(TransactionDto transactionDto, User user) {
        Transaction transaction = new Transaction();
        Wallet payerWallet = walletService.getById(transactionDto.getFromWalletId());
        Wallet payeeWallet = walletService.getById(transactionDto.getToWalletId());
        Status transactionStatus = statusService.getById(STATUS_COMPLETED);
        Category transactionCategory = categoryService.getById(transactionDto.getCategoryId());
        double exchangeRate = currencyService.calculateExchangeRate(payerWallet, payeeWallet);

        return mapTransactionFromPaymentDetails(transactionDto,
                user,
                transaction,
                payerWallet,
                payeeWallet,
                transactionStatus,
                transactionCategory,
                exchangeRate);
    }

    private Transaction mapTransactionFromPaymentDetails(TransactionDto transactionDto,
                                                         User user,
                                                         Transaction transaction,
                                                         Wallet payerWallet,
                                                         Wallet payeeWallet,
                                                         Status transactionStatus,
                                                         Category transactionCategory,
                                                         double exchangeRate) {
        transaction.setFromWallet(payerWallet);
        transaction.setToWallet(payeeWallet);
        transaction.setTransferValue(transactionDto.getValue());
        transaction.setFromUser(user);
        transaction.setCategory(transactionCategory);
        transaction.setStatus(transactionStatus);
        transaction.setExchangeRate(exchangeRate);

        return transaction;
    }

    public TransactionsFilterParams populateOutgoingTransactionFilterSortDto(User user, Optional<Integer> walletId, int pastPeriod) {
        TransactionsFilterParams transactionsFilterParams;
        TransactionFilterSortDto transactionFilterSortDto = new TransactionFilterSortDto();

        transactionFilterSortDto.setSenderUsername(user.getUsername());
        setPastDateToDto(pastPeriod, transactionFilterSortDto);
        transactionFilterSortDto.setStatus(String.valueOf(STATUS_COMPLETED));
        walletId.ifPresent(value -> transactionFilterSortDto.setFromWalletId(String.valueOf(value)));

        transactionsFilterParams = fromDto(transactionFilterSortDto);
        return transactionsFilterParams;
    }

    private void setPastDateToDto(int pastPeriod, TransactionFilterSortDto transactionFilterSortDto) {
        LocalDate now = LocalDate.now();
        LocalDate earlier = now.minusMonths(pastPeriod);
        transactionFilterSortDto.setDateFrom(DateTimeFormatter.ofPattern("yyyy-MM").format(earlier));
    }

    public TransactionsFilterParams populateIncomingTransactionsFilterSortDto(User user,
                                                                              Optional<Integer> walletId,
                                                                              int pastPeriod) {
        TransactionFilterSortDto transactionFilterSortDto = new TransactionFilterSortDto();
        TransactionsFilterParams transactionsFilterParams;

        transactionFilterSortDto.setRecipientUsername(user.getUsername());
        setPastDateToDto(pastPeriod, transactionFilterSortDto);
        transactionFilterSortDto.setStatus(String.valueOf(STATUS_COMPLETED));
        walletId.ifPresent(value -> transactionFilterSortDto.setToWalletId(String.valueOf(value)));

        transactionsFilterParams = fromDto(transactionFilterSortDto);
        return transactionsFilterParams;
    }

    public TransactionsFilterParams mapIncomingUserPaymentsToFilterParams(User user,
                                                                          Optional<Integer> walletId,
                                                                          SortOptions sortOption) {
        TransactionsFilterParams transactionsFilterParams;
        TransactionFilterSortDto transactionFilterSortDto = new TransactionFilterSortDto();
        transactionFilterSortDto.setSort(sortOption.getPreview());
        transactionFilterSortDto.setRecipientUsername(user.getUsername());
        walletId.ifPresent(value -> transactionFilterSortDto.setToWalletId(String.valueOf(value)));

        transactionsFilterParams = fromDto(transactionFilterSortDto);
        return transactionsFilterParams;
    }

    public TransactionsFilterParams mapOutgoingUserPaymentsToFilterParams(User user,
                                                                          Optional<Integer> walletId,
                                                                          SortOptions sortOption) {
        TransactionsFilterParams transactionsFilterParams;
        TransactionFilterSortDto transactionFilterSortDto = new TransactionFilterSortDto();
        transactionFilterSortDto.setSort(DATE_DESC.getPreview());
        transactionFilterSortDto.setSenderUsername(user.getUsername());
        walletId.ifPresent(value -> transactionFilterSortDto.setFromWalletId(String.valueOf(value)));

        transactionsFilterParams = fromDto(transactionFilterSortDto);
        return transactionsFilterParams;
    }

    public void mapTransactionDtoForExternalCardToWalletPayment(TransactionDto transactionDto, int statusId, int toWalletId, Card selectedCard) {
        Wallet toWallet = walletService.getById(toWalletId);
        transactionDto.setStatusId(statusId);
        transactionDto.setToWalletId(toWalletId);
        transactionDto.setFromWalletId(toWalletId);
        transactionDto.setCategoryId(TRANSFER_CATEGORY);
        transactionDto.setExchangeRate(currencyService.calculateExchangeRate(selectedCard, toWallet));
    }

    public Transaction mapTransactionForSavingsPayment(Wallet customerWallet,
                                                       Currency savingsCurrency,
                                                       Wallet bankMainAcc,
                                                       Status statusCompleted,
                                                       double savingsAccrued, double exchangeRate) {
        Transaction transaction = new Transaction();
        Category category = categoryService.getById(CATEGORY_SAVINGS);

        transaction.setStatus(statusCompleted);
        transaction.setTransferValue(savingsAccrued);
        transaction.setFromWallet(bankMainAcc);
        transaction.setFromUser(bankMainAcc.getOwner());
        transaction.setToWallet(customerWallet);
        transaction.setCategory(category);
        transaction.setExchangeRate(exchangeRate);
        return transaction;
    }
}
