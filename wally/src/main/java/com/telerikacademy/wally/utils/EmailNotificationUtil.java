package com.telerikacademy.wally.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;

@Component
public class EmailNotificationUtil {
    private static JavaMailSender mailSender;

    public EmailNotificationUtil(JavaMailSender mailSender) {
        EmailNotificationUtil.mailSender = mailSender;
    }

    @Autowired
    public static void sendNotificationEmail() throws MessagingException, UnsupportedEncodingException {
        String subject = "Exchange rate Issues";
        String senderName = "Wally";
        String mailContent = "This email is send in order to inform about exchange rate issues \n" +
                "Please check exchange rates! \n" +
                "The Wally Team";
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setFrom("telerikwally@gmail.com", senderName);
        helper.setTo("telerikwally@gmail.com");
        helper.setSubject(subject);

        helper.setText(mailContent, true);

        mailSender.send(message);
    }

}
