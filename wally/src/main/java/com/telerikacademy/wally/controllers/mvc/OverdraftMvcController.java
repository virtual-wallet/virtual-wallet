package com.telerikacademy.wally.controllers.mvc;

import com.telerikacademy.wally.controllers.AuthenticationHelper;
import com.telerikacademy.wally.exceptions.AuthenticationFailureException;
import com.telerikacademy.wally.exceptions.DuplicateEntityException;
import com.telerikacademy.wally.exceptions.EntityNotFoundException;
import com.telerikacademy.wally.models.OverdraftFeature;
import com.telerikacademy.wally.models.OverdraftRecord;
import com.telerikacademy.wally.models.User;
import com.telerikacademy.wally.models.dtos.OverdraftFeatureDto;
import com.telerikacademy.wally.services.contracts.OverdraftFeatureService;
import com.telerikacademy.wally.services.contracts.OverdraftRecordService;
import com.telerikacademy.wally.utils.mappers.OverdraftMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/overdrafts")
public class OverdraftMvcController extends BaseAuthenticationController {

    private final OverdraftFeatureService overdraftFeatureService;
    private final OverdraftRecordService overdraftRecordService;
    private final OverdraftMapper overdraftMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public OverdraftMvcController(OverdraftFeatureService overdraftFeatureService,
                                  OverdraftRecordService overdraftRecordService,
                                  OverdraftMapper overdraftMapper,
                                  AuthenticationHelper authenticationHelper) {
        super(authenticationHelper);
        this.overdraftFeatureService = overdraftFeatureService;
        this.overdraftRecordService = overdraftRecordService;
        this.overdraftMapper = overdraftMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("overdraftFeatures")
    public List<OverdraftFeature> populateOverdraftFeatures() {
        return overdraftFeatureService.getAll();
    }

    @ModelAttribute("overdraftRecords")
    public List<OverdraftRecord> populateOverdraftRecords() {
        return overdraftRecordService.getAll();
    }

    @GetMapping
    public String viewAllOverdrafts(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            model.addAttribute("overdraftRecords",
                    overdraftRecordService.getAllByField("wallet.owner.id", user.getUserId()));
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e){
        }
        return "overdraft-features";
    }

    @GetMapping("/new")
    public String setUpNewOverdraftFeature(Model model, HttpSession session) {

        model.addAttribute("overdraftFeatureDto", new OverdraftFeatureDto());
        return "overdraft-feature-form";
    }

    @PostMapping("/new")
    public String createNewOverdraftFeature(@Valid @ModelAttribute("overdraftFeatureDto") OverdraftFeatureDto overdraftFeatureDto,
                                            BindingResult bindingResult,
                                            Model model,
                                            HttpSession session) {

        if (bindingResult.hasErrors()) {
            return "overdraft-feature-form";
        }

        User user;
        try {
            //todo admin only
            user = authenticationHelper.tryGetUser(session);

            OverdraftFeature overdraftFeature = overdraftMapper.fromDto(overdraftFeatureDto);
            overdraftFeatureService.create(overdraftFeature, user);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("overdraftFeatureName", e.getMessage());
            return "overdraft-feature-form";
        }

        return "redirect:/overdrafts";
    }

    @GetMapping("/{id}/update")
    public String updateOverdraftFeatureView(@PathVariable int id,
                                             Model model,
                                             HttpSession session) {
        User user;

        try {
            //todo admin only
            user = authenticationHelper.tryGetUser(session);

            OverdraftFeatureDto overdraftFeatureDto = overdraftMapper.toDto(overdraftFeatureService.getById(id));
            model.addAttribute("overdraftFeatureDto", overdraftFeatureDto);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            return "overdraft-feature-form";
        }
        return "overdraft-feature-form";
    }

    @PostMapping("/{id}/update")
    public String updateOverdraftFeature(@PathVariable int id,
                                         @Valid @ModelAttribute("overdraftFeatureDto") OverdraftFeatureDto overdraftFeatureDto,
                                         BindingResult bindingResult,
                                         Model model,
                                         HttpSession session) {

        if (bindingResult.hasErrors()) {
            //todo solve for checks
            return "overdraft-feature-form";
        }

        User user;
        try {
            //todo admin only
            user = authenticationHelper.tryGetUser(session);

            OverdraftFeature overdraftFeature = overdraftMapper.fromDto(overdraftFeatureDto, id);
            overdraftFeatureService.update(overdraftFeature, user);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("name", "OverdraftFeature.exists", e.getMessage());
            return "overdraft-feature-form";
        } catch (EntityNotFoundException e) {
            bindingResult.rejectValue("name", "OverdraftFeature.doesNotExist", e.getMessage());
            return "overdraft-feature-form";
        }

        return "redirect:/overdrafts";
    }
}
