package com.telerikacademy.wally.controllers.rest;

import com.telerikacademy.wally.controllers.AuthenticationHelper;
import com.telerikacademy.wally.exceptions.DuplicateEntityException;
import com.telerikacademy.wally.exceptions.EntityNotFoundException;
import com.telerikacademy.wally.exceptions.UnauthorizedOperationException;
import com.telerikacademy.wally.models.User;
import com.telerikacademy.wally.models.Wallet;
import com.telerikacademy.wally.models.dtos.WalletDto;
import com.telerikacademy.wally.services.contracts.WalletService;
import com.telerikacademy.wally.utils.mappers.WalletMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/wallets")
public class WalletController {
    private final WalletService walletService;
    private final WalletMapper walletMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public WalletController(WalletService walletService, WalletMapper walletMapper, AuthenticationHelper authenticationHelper) {
        this.walletService = walletService;
        this.walletMapper = walletMapper;
        this.authenticationHelper = authenticationHelper;
    }


    @GetMapping("/user/{id}")
    public List<Wallet> getAllUserWallets(@PathVariable int id, @RequestHeader HttpHeaders headers) {

        User user = authenticationHelper.tryGetUser(headers);
        return walletService.getAllByField("owner_id", id);

    }

    @GetMapping("/{id}")
    public Wallet getById(@PathVariable int id, @RequestHeader HttpHeaders headers) {

        User user = authenticationHelper.tryGetUser(headers);
        return walletService.getById(id);

    }

    @PostMapping
    public Wallet create(@Valid @RequestBody WalletDto walletDto, @RequestHeader HttpHeaders headers) {
        authenticationHelper.tryGetUser(headers);
        Wallet wallet = walletMapper.fromDto(walletDto);
        walletService.create(wallet);
        return wallet;
    }

    @PutMapping("/{id}")
    public Wallet update(@PathVariable int id, @Valid @RequestBody WalletDto walletDto, @RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        Wallet userWallet = walletService.getById(id);

        if (userWallet.getOwner().equals(user)) {
            Wallet walletToUpdate = walletMapper.fromDto(walletDto, id);
            walletService.update(walletToUpdate, user);
            return userWallet;
        } else throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Unauthorized access.");

    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        Wallet userWallet = walletService.getById(id);

        if (userWallet.getOwner().equals(user)) {
            walletService.delete(id);
        } else throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Unauthorized access.");
    }
}
