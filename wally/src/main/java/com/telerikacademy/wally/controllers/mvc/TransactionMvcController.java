package com.telerikacademy.wally.controllers.mvc;

import com.telerikacademy.wally.controllers.AuthenticationHelper;
import com.telerikacademy.wally.exceptions.AuthenticationFailureException;
import com.telerikacademy.wally.exceptions.EntityNotFoundException;
import com.telerikacademy.wally.exceptions.UnauthorizedOperationException;
import com.telerikacademy.wally.models.*;
import com.telerikacademy.wally.models.dtos.TransactionFilterSortDto;
import com.telerikacademy.wally.models.enums.TransactionSortOptions;
import com.telerikacademy.wally.models.params.TransactionsFilterParams;
import com.telerikacademy.wally.services.contracts.*;
import com.telerikacademy.wally.utils.mappers.TransactionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.telerikacademy.wally.controllers.utils.Constants.*;
import static com.telerikacademy.wally.controllers.utils.PaginationHelper.setPageNavigation;

@Controller
@RequestMapping("/transactions")
public class TransactionMvcController extends BaseAuthenticationController {

    private final AuthenticationHelper authenticationHelper;
    private final TransactionService transactionService;
    private final TransactionMapper transactionMapper;
    private final UserService userService;
    private final WalletService walletService;
    private final CardService cardService;
    private final CategoryService categoryService;
    private final StatusService statusService;

    @Autowired
    public TransactionMvcController(AuthenticationHelper authenticationHelper,
                                    TransactionService transactionService,
                                    TransactionMapper transactionMapper,
                                    UserService userService,
                                    WalletService walletService,
                                    CardService cardService, CategoryService categoryService, StatusService statusService) {
        super(authenticationHelper);
        this.authenticationHelper = authenticationHelper;
        this.transactionService = transactionService;
        this.transactionMapper = transactionMapper;
        this.userService = userService;
        this.walletService = walletService;
        this.cardService = cardService;
        this.categoryService = categoryService;
        this.statusService = statusService;
    }

    //todo set personal transactions view and admin transactions view
    @ModelAttribute
    public void addAttributes(Model model, HttpSession session) {
        model.addAttribute("transactions", transactionService.getAll());
        model.addAttribute("users", userService.getAll());
        model.addAttribute("transactionSortOptions", TransactionSortOptions.values());
        model.addAttribute("wallets", walletService.getAll());
        model.addAttribute("categories", categoryService.getAll());
        model.addAttribute("statuses", statusService.getAll());
    }

    @GetMapping()
    public String getPaginatedTransactions(@RequestParam("sender-username") Optional<String> senderUsername,
                                           @RequestParam("recipient-username") Optional<String> recipientUsername,
                                           @RequestParam("date-from") Optional<String> dateFrom,
                                           @RequestParam("date-to") Optional<String> dateTo,
                                           @RequestParam("sort") Optional<String> sort,
                                           @RequestParam("from-wallet") Optional<String> fromWalletId,
                                           @RequestParam("to-wallet") Optional<String> toWalletId,
                                           @RequestParam("is-credited") Optional<String> isCredited,
                                           @RequestParam("category") Optional<String> category,
                                           @RequestParam("status") Optional<String> status,
                                           @RequestParam("page") Optional<Integer> page,
                                           @RequestParam("size") Optional<Integer> size,
                                           Model model,
                                           HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        int currentPage = page.orElse(DEFAULT_MAIN_PAGE);
        int pageSize = size.orElse(DEFAULT_PAGE_SIZE);

        TransactionFilterSortDto transactionFilterSortDto = transactionMapper.toDto(senderUsername,
                recipientUsername,
                dateFrom,
                dateTo,
                sort,
                fromWalletId,
                toWalletId,
                isCredited,
                category,
                status);

        TransactionsFilterParams filterParams = transactionMapper.fromDto(transactionFilterSortDto);

        try {
            var filtered = transactionService.findPaginated(
                    PageRequest.of(currentPage - 1, pageSize),
                    filterParams,user);

            setPageNavigation(model, filtered.getTotalPages(), currentPage);

            model.addAttribute("transactionPage", filtered);
            model.addAttribute("transactionFilterSortDto", transactionFilterSortDto);
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }catch (UnauthorizedOperationException e)
        {
            return "access-denied";
        }
        return "transactions";
    }

    @PostMapping()
    public String filterPaginatedTransactions(@ModelAttribute TransactionFilterSortDto transactionFilterSortDto,
                                              @RequestParam("page") Optional<Integer> page,
                                              @RequestParam("size") Optional<Integer> size,
                                              Model model,
                                              HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        int currentPage = page.orElse(DEFAULT_MAIN_PAGE);
        int pageSize = size.orElse(DEFAULT_PAGE_SIZE);

        try {
            var filtered = transactionService.findPaginated(
                    PageRequest.of(currentPage - 1, pageSize),
                    transactionMapper.fromDto(transactionFilterSortDto),user);

            setPageNavigation(model, filtered.getTotalPages(), currentPage);

            model.addAttribute("transactionPage", filtered);
            model.addAttribute("transactionFilterSortDto", transactionFilterSortDto);
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }catch (UnauthorizedOperationException e)
        {
            return "access-denied";
        }
        return "transactions";
    }



}