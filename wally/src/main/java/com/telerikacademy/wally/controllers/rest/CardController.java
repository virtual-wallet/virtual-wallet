package com.telerikacademy.wally.controllers.rest;

import com.telerikacademy.wally.controllers.AuthenticationHelper;
import com.telerikacademy.wally.models.Card;
import com.telerikacademy.wally.models.User;
import com.telerikacademy.wally.models.dtos.CardDto;
import com.telerikacademy.wally.models.dtos.UpdateCardDto;
import com.telerikacademy.wally.services.contracts.CardService;
import com.telerikacademy.wally.utils.mappers.CardMapper;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/cards")
public class CardController {

    private final CardService cardService;
    private final CardMapper cardMapper;
    private final AuthenticationHelper authenticationHelper;


    @Autowired
    public CardController(CardService cardService, CardMapper cardMapper, AuthenticationHelper authenticationHelper) {
        this.cardService = cardService;
        this.cardMapper = cardMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("/user/{id}")
    public List<Card> getAllUserCards(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        return cardService.getAllByField("user_id", id, id, user);
    }

    @GetMapping("/{id}")
    public Card getById(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        List<Card> userCards = cardService.getAllByField("user_id", user.getUserId(), user.getUserId(), user);
        for (Card card : userCards) {
            if (card.getId() == id) {
                return cardService.getById(id, card.getUser().getUserId(), user);
            }
        }
        throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Unauthorized access.");
    }

    @SneakyThrows
    @PostMapping
    public Card create(@Valid @RequestBody CardDto cardDto, @RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        Card card = cardMapper.fromDto(cardDto);
        cardService.create(card, user);
        return card;
    }

    @SneakyThrows
    @PutMapping("/{id}")
    public Card update(@PathVariable int id, @Valid @RequestBody UpdateCardDto cardDto, @RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        Card userCard = cardService.getById(id, user.getUserId(), user);

        if (userCard.getUser().equals(user)) {
            Card cardToUpdate = cardService.getById(id, userCard.getUser().getUserId(), user);
            Card updatedCard = cardMapper.fromDto(cardDto, id);
            updatedCard.setUser(cardToUpdate.getUser());
            cardService.update(updatedCard, user);
            return userCard;
        } else throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Unauthorized access.");
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        List<Card> userCards = cardService.getAllByField("user_id", user.getUserId(), user.getUserId(), user);
        for (Card card : userCards) {
            if (card.getId() == id) {
                cardService.delete(id, card.getUser().getUserId(), user);
            }
        }
    }

}

