package com.telerikacademy.wally.controllers.mvc;

import com.telerikacademy.wally.controllers.AuthenticationHelper;
import com.telerikacademy.wally.exceptions.AuthenticationFailureException;
import com.telerikacademy.wally.exceptions.EntityNotFoundException;
import com.telerikacademy.wally.exceptions.UnauthorizedOperationException;
import com.telerikacademy.wally.models.Category;
import com.telerikacademy.wally.models.User;
import com.telerikacademy.wally.models.dtos.NameDto;
import com.telerikacademy.wally.services.contracts.CategoryService;
import com.telerikacademy.wally.services.contracts.UserService;
import com.telerikacademy.wally.utils.mappers.CategoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/categories")
public class CategoriesMvcController extends BaseAuthenticationController{

    private final AuthenticationHelper authenticationHelper;
    private final CategoryService categoryService;
    private final CategoryMapper categoryMapper;
    private final UserService userService;

    @Autowired
    public CategoriesMvcController(AuthenticationHelper authenticationController,
                                   CategoryService categoryService,
                                   CategoryMapper categoryMapper,
                                   UserService userService) {
        super(authenticationController);
        this.authenticationHelper = authenticationController;
        this.categoryService = categoryService;
        this.categoryMapper = categoryMapper;
        this.userService = userService;
    }

    @ModelAttribute("categories")
    public List<Category> populateCategories() {
        return categoryService.getAll();
    }

    @GetMapping
    public String getCategories(Model model, HttpSession session) {
        User user;
        try {
            //todo
            user = authenticationHelper.tryGetUser(session);

            model.addAttribute("userCategories", user.getCategoriesSet());
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
        return "categories";
    }

    @GetMapping("/new")
    public String showNewCategoryPage(Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("category", new NameDto());
        return "category-form";
    }

    @PostMapping("/new")
    public String createCategory(@Valid @ModelAttribute("category") NameDto nameDto,
                                 BindingResult errors,
                                 Model model,
                                 HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "category-form";
        }

        Category newCategory = categoryMapper.fromDto(nameDto);
        try {
            userService.addOrModifyCategoryToUserCategories(newCategory, user);
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }

        return "redirect:/categories";
    }

    @GetMapping("/{id}/update")
    public String updateCategoryPage(@PathVariable int id, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);

            NameDto nameDto = categoryMapper.toDto(categoryService.getById(id));

            model.addAttribute("category", nameDto);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        return "category-form";
    }

    @PostMapping("/{id}/update")
    public String updateCategory(@PathVariable int id,
                                 @Valid @ModelAttribute("category") NameDto nameDto,
                                 BindingResult errors,
                                 Model model,
                                 HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "category-form";
        }

        Category currentCategory = categoryMapper.fromDto(nameDto);
        currentCategory.setId(id);
        try {
            userService.addOrModifyCategoryToUserCategories(currentCategory, user);
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }

        return "redirect:/categories";
    }

    @GetMapping("/{id}/delete")
    public String deleteCategory(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);

            Category categoryToDelete = categoryService.getById(id);
            userService.deleteUserCategory(categoryToDelete, user);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        return "redirect:/categories";
    }

}
