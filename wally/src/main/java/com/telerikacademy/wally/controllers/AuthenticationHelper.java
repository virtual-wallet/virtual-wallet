package com.telerikacademy.wally.controllers;

import com.telerikacademy.wally.exceptions.AuthenticationFailureException;
import com.telerikacademy.wally.exceptions.EntityNotFoundException;
import com.telerikacademy.wally.models.Role;
import com.telerikacademy.wally.models.User;
import com.telerikacademy.wally.services.contracts.RoleService;
import com.telerikacademy.wally.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;

@Component
public class AuthenticationHelper {

    public static final String AUTHORIZATION_HEADER_NAME = "Authorization";
    public static final String AUTHENTICATION_FAILURE_MESSAGE = "Wrong username or password.";

    private final UserService userService;
    private final RoleService roleService;

    @Autowired
    public AuthenticationHelper(UserService userService, RoleService roleService) {
        this.userService = userService;
        this.roleService = roleService;
    }

    public User tryGetUser(HttpHeaders headers) {
        if (!headers.containsKey(AUTHORIZATION_HEADER_NAME)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,
                    "The requested resource requires authentication.");
        }

        try {
            String username = headers.getFirst(AUTHORIZATION_HEADER_NAME);
            return userService.getByField("username", username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid username.");
        }
    }

    public User tryGetUser(HttpSession session) {
        String currentUser = (String) session.getAttribute("currentUser");

        if (currentUser == null) {
            throw new AuthenticationFailureException("No user logged in.");
        }

        return userService.getByField("username", currentUser);
    }

    public boolean verifyAuthorization(HttpSession session) {
        String currentUser = (String) session.getAttribute("currentUser");
        User user = userService.getByField("username",currentUser);
        Role authorizedRole = roleService.getByField("name","admin");
        if (!user.getRoleSet().contains(authorizedRole)) {
            return false;
        }
        return true;
    }

    public void verifyAuthentication(String username, String password) {
        try {
            User user = userService.getByField("username", username);
            if (!user.getPassword().equals(password) || !user.isEnabled()) {
                throw new AuthenticationFailureException(AUTHENTICATION_FAILURE_MESSAGE);
            }
        } catch (EntityNotFoundException e) {
            throw new AuthenticationFailureException(AUTHENTICATION_FAILURE_MESSAGE);
        }
    }

}



