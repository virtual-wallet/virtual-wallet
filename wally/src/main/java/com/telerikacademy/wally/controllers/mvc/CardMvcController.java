package com.telerikacademy.wally.controllers.mvc;

import com.telerikacademy.wally.controllers.AuthenticationHelper;
import com.telerikacademy.wally.exceptions.*;
import com.telerikacademy.wally.models.Card;
import com.telerikacademy.wally.models.User;
import com.telerikacademy.wally.models.dtos.CardDto;
import com.telerikacademy.wally.models.dtos.UpdateCardDto;
import com.telerikacademy.wally.services.contracts.CardService;
import com.telerikacademy.wally.services.contracts.CardTypeService;
import com.telerikacademy.wally.services.contracts.CurrencyService;
import com.telerikacademy.wally.utils.mappers.CardMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/cards")
public class CardMvcController extends BaseAuthenticationController {
    private final CardMapper cardMapper;
    private final CardService cardService;
    private final CardTypeService cardTypeService;
    private final AuthenticationHelper authenticationHelper;
    private final CurrencyService currencyService;

    @Autowired
    public CardMvcController(CardMapper cardMapper,
                             CardService cardService,
                             CardTypeService cardTypeService,
                             AuthenticationHelper authenticationHelper,
                             CurrencyService currencyService) {
        super(authenticationHelper);
        this.cardMapper = cardMapper;
        this.cardService = cardService;
        this.cardTypeService = cardTypeService;
        this.authenticationHelper = authenticationHelper;
        this.currencyService = currencyService;
    }

    @ModelAttribute
    public void addAttributes(Model model, HttpSession session) {
        model.addAttribute("currencies", currencyService.getAll());
        model.addAttribute("types", cardTypeService.getAll());
        try {
            model.addAttribute("userCards", authenticationHelper.tryGetUser(session).getUserId());
        } catch (EntityNotFoundException e) {
            model.addAttribute("userCards", new ArrayList<>());
        }
    }

    @GetMapping
    public String showUserCards(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            model.addAttribute("cards", cardService.getAllByField("user_id", user.getUserId(), user.getUserId(), user));
            return "cards-user";
        } catch (EntityNotFoundException e) {
            return "redirect:/cards/new";
        }
    }

    @GetMapping("/{id}")
    public String showSingleCard(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            List<Card> userCards = cardService.getAllByField("user_id", user.getUserId(), user.getUserId(), user);
            for (Card card : userCards) {
                if (card.getId() == id) {
                    cardService.getById(id, card.getUser().getUserId(), user);
                    model.addAttribute("card", card);
                    return "card";
                }
            }
            return "access-denied";

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/new")
    public String showNewCardPage(Model model, HttpSession session) {

        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        model.addAttribute("card", new CardDto());

        return "card-new";
    }

    @PostMapping("/new")
    public String createCard(@Valid @ModelAttribute("card") CardDto cardDto,
                             BindingResult bindingResult, HttpSession session) {

        if (bindingResult.hasErrors()) {
            return "card-new";
        }

        try {
            User creator = authenticationHelper.tryGetUser(session);
            cardDto.setUserId(creator.getUserId());
            Card card = cardMapper.fromDto(cardDto);
            cardService.create(card, creator);

            return "redirect:/cards";
        } catch (DuplicateEntityException | ParseException e) {
            bindingResult.rejectValue("cardNumber", "card.exist", e.getMessage());
            return "card-new";
        } catch (InvalidDataInput e) {
            bindingResult.rejectValue("expireDate", "expireDate.past", e.getMessage());
            return "card-new";
        }
    }

    @GetMapping("/{id}/update")
    public String showEditCardPage(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {

            List<Card> userCards = cardService.getAllByField("user_id", user.getUserId(), user.getUserId(), user);
            for (Card card : userCards) {
                if (card.getId() == id) {
                    cardService.getById(id, card.getUser().getUserId(), user);
                    model.addAttribute("card", card);
                    CardDto cardDto = cardMapper.toDto(card);
                    model.addAttribute("card", cardDto);
                    return "card-update";
                }
            }
            // Card card = cardService.getById(id, user);
//            CardDto cardDto = cardMapper.toDto(card);
//            model.addAttribute("card", cardDto);
            return "access-denied";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }


    }

    @PostMapping("/{id}/update")
    public String updateCard(@PathVariable int id,
                             @Valid @ModelAttribute("card") UpdateCardDto cardDto,
                             BindingResult errors,
                             Model model,
                             HttpSession session) {

        User user;

        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        if (errors.hasErrors()) {
            return "card-update";
        }
        try {
            List<Card> userCards = cardService.getAllByField("user_id", user.getUserId(), user.getUserId(), user);
            for (Card card : userCards) {
                if (card.getId() == id) {
                    Card cardToUpdate = cardService.getById(id, card.getUser().getUserId(), user);
                    Card updatedCard = cardMapper.fromDto(cardDto, id);
                    updatedCard.setUser(cardToUpdate.getUser());
                    cardService.update(updatedCard, user);
                    return "redirect:/cards";
                }
            }
            // return "access-denied";
            return "redirect:/cards";

        } catch (DuplicateEntityException | ParseException e) {
            errors.rejectValue("name", "card.exists", e.getMessage());
            return "card-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }

    }

    @GetMapping("/{id}/delete")
    public String deleteCard(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }


        try {
            List<Card> userCards = cardService.getAllByField("user_id", user.getUserId(), user.getUserId(), user);
            for (Card card : userCards) {
                if (card.getId() == id) {
                    cardService.delete(id, card.getUser().getUserId(), user);
                    model.addAttribute("card", card);
                    return "redirect:/cards";
                }
            }
            return "access-denied";

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }

    }
}



