package com.telerikacademy.wally.controllers.mvc;

import com.telerikacademy.wally.config.ExternalApiUrlConfig;
import com.telerikacademy.wally.controllers.AuthenticationHelper;
import com.telerikacademy.wally.exceptions.AuthenticationFailureException;
import com.telerikacademy.wally.exceptions.EntityNotFoundException;
import com.telerikacademy.wally.exceptions.PaymentIssueException;
import com.telerikacademy.wally.exceptions.UnauthorizedOperationException;
import com.telerikacademy.wally.models.*;
import com.telerikacademy.wally.models.Currency;
import com.telerikacademy.wally.models.dtos.*;
import com.telerikacademy.wally.models.enums.TransactionSortOptions;
import com.telerikacademy.wally.models.params.TransactionsFilterParams;
import com.telerikacademy.wally.models.dtos.CardDto;
import com.telerikacademy.wally.models.dtos.WalletDto;
import com.telerikacademy.wally.services.contracts.*;
import com.telerikacademy.wally.utils.mappers.TransactionMapper;
import com.telerikacademy.wally.utils.mappers.WalletMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.*;

import static com.telerikacademy.wally.controllers.utils.Constants.*;
import static com.telerikacademy.wally.controllers.utils.PaginationHelper.setPageNavigation;
import static com.telerikacademy.wally.utils.ExternalQuery.createExternalTransferRequestQuery;

@Controller
@RequestMapping("/wallets")
public class WalletMvcController extends BaseAuthenticationController {
    public static final String REQUEST_TIMEOUT_ERROR = "We couldn't reach your bank. Please try again later!";
    public static final String GENERAL_HTTP_STATUS_ERROR = "There has been a problem with your transaction. Please contact us!";
    public static final String INSUFFICIENT_FUNDS_ERROR = "Insufficient funds!";
    public static final String UNKNOWN_HTTP_RESPONSE_STATUS_ERROR = "Unknown Server Response. Please contact us.";
    public static final int STATUS_ID_DECLINED = 1;
    public static final String WRONG_WALLET_ACCESS = "This is not your wallet.";

    private final AuthenticationHelper authenticationHelper;
    private final WalletService walletService;
    private final WalletMapper walletMapper;
    private final UserService userService;
    private final CurrencyService currencyService;
    private final CardService cardService;
    private final TransactionService transactionService;
    private final TransactionMapper transactionMapper;
    private final ExternalApiUrlConfig externalApiUrlConfig;
    private final CategoryService categoryService;
    private final PaymentManager paymentManager;
    private static int requestsLeft = 2;

    @Autowired
    public WalletMvcController(AuthenticationHelper authenticationHelper,
                               WalletService walletService,
                               WalletMapper walletMapper,
                               UserService userService,
                               CurrencyService currencyService,
                               CardService cardService,
                               TransactionService transactionService,
                               TransactionMapper transactionMapper,
                               ExternalApiUrlConfig externalApiUrlConfig,
                               CategoryService categoryService,
                               PaymentManager paymentManager) {
        super(authenticationHelper);
        this.authenticationHelper = authenticationHelper;
        this.walletService = walletService;
        this.walletMapper = walletMapper;
        this.userService = userService;
        this.currencyService = currencyService;
        this.cardService = cardService;
        this.transactionService = transactionService;
        this.transactionMapper = transactionMapper;
        this.externalApiUrlConfig = externalApiUrlConfig;
        this.categoryService = categoryService;
        this.paymentManager = paymentManager;
    }

    @ModelAttribute
    public void addAttributes(Model model, HttpSession session) {
        model.addAttribute("currencies", currencyService.getAll());
        try {
            User user = authenticationHelper.tryGetUser(session);
            model.addAttribute("senderCards",
                    cardService.getAllByField("user_id", user.getUserId(), user.getUserId(), user));
            model.addAttribute("walletsUser",
                    walletService.getAllByField("owner_id", authenticationHelper.tryGetUser(session).getUserId()));
        } catch (Exception e) {
            model.addAttribute("senderCards", new ArrayList<>());
            model.addAttribute("walletsUser", new ArrayList<>());
        }
    }

    @GetMapping
    public String showAllUsersWallets(@RequestParam("page") Optional<Integer> page,
                                      @RequestParam("size") Optional<Integer> size,
                                      Model model,
                                      HttpSession session) {

        int currentPage = page.orElse(DEFAULT_MAIN_PAGE);
        int pageSize = size.orElse(DEFAULT_PAGE_SIZE);

        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        List<Wallet> userWallets;
        try {
            userWallets = walletService.getAllByField("owner_id", user.getUserId());
        } catch (EntityNotFoundException e) {
            return "redirect:/wallets/new";
        }
        if (userWallets.size() == 1) {
            return "redirect:/wallets/" + userWallets.get(0).getWalletId();
        }

        Map<String, Double> totalBalance = walletService.getUserTotalBalance(userWallets);

        TransactionsFilterParams incomingTransactionsFilterParams;
        TransactionsFilterParams outgoingTransactionsFilterParams;

        incomingTransactionsFilterParams = transactionMapper
                .populateOutgoingTransactionFilterSortDto(user, Optional.empty(), TRANSACTIONS_PAST_MONTHS_NUM);

        outgoingTransactionsFilterParams = transactionMapper
                .populateIncomingTransactionsFilterSortDto(user, Optional.empty(), TRANSACTIONS_PAST_MONTHS_NUM);

        Map<String, Map<String, Double>> lastMonthTransactionSummary = walletService
                .getLastMonthTransactionsSummary(Optional.empty(),
                        user,
                        incomingTransactionsFilterParams,
                        outgoingTransactionsFilterParams);

        var paginatedUserTransactions = transactionService.loadUserPastTransactions(currentPage,
                pageSize,
                user,
                Optional.empty());
        setPageNavigation(model, paginatedUserTransactions.getTotalPages(), currentPage);

        model.addAttribute("paginatedTransactions", paginatedUserTransactions);
        model.addAttribute("lastMonthsTransactionsSummary", lastMonthTransactionSummary);
        model.addAttribute("totalBalance", totalBalance);
        model.addAttribute("walletsUser", walletService
                .getAllByField("owner_id", user.getUserId()));
        return "wallets-user";
    }

    @GetMapping("/{walletId}")
    public String showWallet(@RequestParam("page") Optional<Integer> page,
                             @RequestParam("size") Optional<Integer> size,
                             @PathVariable int walletId,
                             Model model,
                             HttpSession session) {

        int currentPage = page.orElse(DEFAULT_MAIN_PAGE);
        int pageSize = size.orElse(DEFAULT_PAGE_SIZE);

        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        //todo is the wallet owner the one requesting the wallet?
        List<Wallet> wallets = new ArrayList<>();
        Wallet walletToGet = new Wallet();
        List<Wallet> userWallets = walletService.getAllByField("owner_id", user.getUserId());
        for (Wallet wallet : userWallets) {
            if (wallet.getWalletId() == walletId) {
                walletToGet = walletService.getById(walletId);
                wallets.add(walletToGet);
            }
        }
        if (wallets.isEmpty()) return "access-denied";


        Map<String, Double> totalBalance = walletService.getWalletTotalBalance(walletToGet);

        TransactionsFilterParams incomingTransactionsFilterParams;
        TransactionsFilterParams outgoingTransactionsFilterParams;

        incomingTransactionsFilterParams = transactionMapper
                .populateIncomingTransactionsFilterSortDto(user, Optional.of(walletId), TRANSACTIONS_PAST_MONTHS_NUM);

        outgoingTransactionsFilterParams = transactionMapper
                .populateOutgoingTransactionFilterSortDto(user, Optional.of(walletId), TRANSACTIONS_PAST_MONTHS_NUM);

        Map<String, Map<String, Double>> lastMonthTransactionSummary = walletService
                .getLastMonthTransactionsSummary(Optional.empty(),
                        user,
                        incomingTransactionsFilterParams,
                        outgoingTransactionsFilterParams);

        var paginatedUserTransactions = transactionService
                .loadUserPastTransactions(currentPage, pageSize, user, Optional.of(walletId));
        setPageNavigation(model, paginatedUserTransactions.getTotalPages(), currentPage);

        model.addAttribute("paginatedTransactions", paginatedUserTransactions);
        model.addAttribute("lastMonthsTransactionsSummary", lastMonthTransactionSummary);
        model.addAttribute("totalBalance", totalBalance);
        model.addAttribute("walletsUser", wallets);
        return "wallets-user";
    }

    @GetMapping("/{id}/add-money")
    public String addMoneyFromCardView(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (!walletService.getAllByField("owner_id", user.getUserId()).contains(walletService.getById(id))) {
            model.addAttribute("error", WRONG_WALLET_ACCESS);
            return "access-denied";
        }

        model.addAttribute("senderCards", cardService.getAllByField("user_id", user.getUserId(), user.getUserId(), user));
        model.addAttribute("userWallet", walletService.getById(id));
        model.addAttribute("cardDto", new CardDto());
        model.addAttribute("transactionValue", new TransactionDto());
        return "add-money";
    }

    @PostMapping("/{id}/add-money")
    public String addMoneyFromCardTransfer(@PathVariable int id,
                                           Model model,
                                           HttpSession session,
                                           @Valid @ModelAttribute("cardDto") CardDto cardDto,
                                           BindingResult cardBindingResult,
                                           @Valid @ModelAttribute("transactionValue") TransactionDto transactionDto,
                                           BindingResult valueBindingResult) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (!walletService.getAllByField("owner_id", user.getUserId()).contains(walletService.getById(id))) {
            model.addAttribute("error", WRONG_WALLET_ACCESS);
            return "access-denied";
        }

        if (valueBindingResult.hasErrors()) return "add-money";

        Card selectedCard = cardService.getByField("cardNumber", cardDto.getCardNumber());
        int cardCurrentCvv = selectedCard.getCvv();
        if (cardCurrentCvv != Integer.parseInt(cardDto.getCvv())) {
            model.addAttribute("userWallet", walletService.getById(id));
            cardBindingResult.rejectValue("cvv", "cvv.mismatch", "Wrong CVV.");
            return "add-money";
        } else if (!selectedCard.getUser().equals(user)) {
            cardBindingResult.rejectValue("cvv", "cvv.mismatch", "Wrong CVV.");
            return "access-denied";
        }

        MockBankDto mockBankDto = transactionMapper.toDto(cardDto, transactionDto);
        String url = externalApiUrlConfig.getMockBankUrl();

        ResponseEntity<String> response;
        try {
            response = createExternalTransferRequestQuery(mockBankDto, url);
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode().value() == 408) {
                if (requestsLeft > 0) {
                    --requestsLeft;
                    addMoneyFromCardTransfer(id,
                            model,
                            session,
                            cardDto,
                            cardBindingResult,
                            transactionDto,
                            valueBindingResult);
                } else {
                    requestsLeft = 2;
                    model.addAttribute("error", REQUEST_TIMEOUT_ERROR);
                }
            } else model.addAttribute("error", GENERAL_HTTP_STATUS_ERROR);
            requestsLeft = 2;
            return "transaction-error";
        }

        requestsLeft = 2;
        transactionDto.setFromUserId(user.getUserId());

        try {
            switch (Objects.requireNonNull(response).getStatusCode().value()) {
                case 202:
                    transactionMapper.mapTransactionDtoForExternalCardToWalletPayment(
                            transactionDto, STATUS_PENDING, id, selectedCard);
                    paymentManager.setCardPaymentIn(id, transactionDto, user);
                    return "redirect:/wallets";
                case 200:
                    switch (Objects.requireNonNull(response.getBody()).toUpperCase()) {
                        case "APPROVED":
                            transactionMapper.mapTransactionDtoForExternalCardToWalletPayment(
                                    transactionDto, STATUS_COMPLETED, id, selectedCard);
                            paymentManager.setCardPaymentIn(id, transactionDto, user);
                            return "redirect:/wallets";
                        case "DECLINED":
                            transactionMapper.mapTransactionDtoForExternalCardToWalletPayment(
                                    transactionDto, STATUS_ID_DECLINED, id, selectedCard);
                            paymentManager.setCardPaymentIn(id, transactionDto, user);
                            model.addAttribute("error", INSUFFICIENT_FUNDS_ERROR);
                            return "transaction-error";
                    }
                default:
                    model.addAttribute("error", UNKNOWN_HTTP_RESPONSE_STATUS_ERROR);
                    return "not-found";
            }
        } catch (UnauthorizedOperationException e) {
            return "access-denied";
        }
    }

    @GetMapping("/all")
    public String showAllWallets(@RequestParam("page") Optional<Integer> page,
                                 @RequestParam("size") Optional<Integer> size,
                                 Model model,
                                 HttpSession session) {

        int currentPage = page.orElse(DEFAULT_MAIN_PAGE);
        int pageSize = size.orElse(DEFAULT_PAGE_SIZE);
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        var wallets = walletService.getPaginated(PageRequest.of(currentPage - 1, pageSize));
        setPageNavigation(model, wallets.getTotalPages(), currentPage);

        model.addAttribute("walletsAll", wallets);
        return "wallets-all";
    }

    @GetMapping("/new")
    public String showNewWalletPage(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            model.addAttribute("senderCards", cardService.getAllByField("user_id", user.getUserId(), user.getUserId(), user));
            model.addAttribute("wallet", new WalletDto());
        } catch (EntityNotFoundException e) {
            return "redirect:/cards/new";
        }
        return "wallets-new";
    }


    @PostMapping("/new")
    public String createWallet(@Valid @ModelAttribute("wallet") WalletDto walletDto,
                               BindingResult errors, HttpSession session) {
        if (errors.hasErrors()) {
            return "wallets-new";
        }

        User user = authenticationHelper.tryGetUser(session);
        walletDto.setOwnerId(user.getUserId());
        Wallet wallet = walletMapper.fromDto(walletDto);
        walletService.create(wallet);
        return "redirect:/wallets";
    }

    @GetMapping("/{id}/delete")
    public String deleteWallet(@PathVariable int id, Model model, HttpSession session) {

        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }


        try {
            List<Wallet> userWallets = walletService.getAllByField("owner_id", user.getUserId());
            for (Wallet wallet : userWallets) {
                if (wallet.getWalletId() == id) {
                    walletService.delete(id);
                    return "redirect:/wallets";
                }
            }
            return "access-denied";


        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }

    }

    @GetMapping("/transaction")
    public String showNewTransactionPage(Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        model.addAttribute("senderWallets", walletService.getAllByField(
                "owner_id", authenticationHelper.tryGetUser(session).getUserId()));
        model.addAttribute("wallets", walletService.getAll());
        model.addAttribute("categories", categoryService.getAll());
        model.addAttribute("transaction", new TransactionDto());
        return "wallets-transaction";
    }

    @PostMapping("/transaction")
    public String createTransaction(@Valid @ModelAttribute("transaction") TransactionDto transactionDto,
                                    BindingResult errors,
                                    Model model,
                                    HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "wallets-transaction";
        }

        if (transactionDto.getToWalletId() == transactionDto.getFromWalletId()) {
            errors.rejectValue("toWalletId", "You cannot pay to the same wallet.");
            return "wallets-transaction";
        }

        try {
            Transaction transaction = transactionMapper.fromDto(transactionDto, user);
            paymentManager.setInternalPayment(transaction, user);
        } catch (PaymentIssueException e) {
            model.addAttribute("error", e.getMessage());
            return "transaction-error";
        }

        return "redirect:/wallets";
    }

    @GetMapping("/overdraft/{id}")
    public String activateOverdraft(@PathVariable int id, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        Wallet wallet = walletService.getById(id);
        wallet.setHasOverdraft(!wallet.isHasOverdraft());
        walletService.update(wallet);
        return "redirect:/wallets";
    }

}
