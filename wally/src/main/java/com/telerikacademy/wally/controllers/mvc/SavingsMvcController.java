package com.telerikacademy.wally.controllers.mvc;

import com.telerikacademy.wally.controllers.AuthenticationHelper;
import com.telerikacademy.wally.exceptions.*;
import com.telerikacademy.wally.models.*;
import com.telerikacademy.wally.models.dtos.PositiveValueDto;
import com.telerikacademy.wally.models.dtos.SavingsFeatureDto;
import com.telerikacademy.wally.models.dtos.SavingsSetUpDto;
import com.telerikacademy.wally.services.contracts.*;
import com.telerikacademy.wally.utils.mappers.SavingsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/savings")
public class SavingsMvcController extends BaseAuthenticationController {


    private final SavingsFeatureService savingsFeatureService;
    private final SavingsRecordService savingsRecordService;
    private final SavingsMapper savingsMapper;
    private final AuthenticationHelper authenticationHelper;
    private final SavingsManager savingsManager;
    private final WalletService walletService;
    private final CurrencyService currencyService;

    @Autowired
    public SavingsMvcController(SavingsFeatureService savingsFeatureService,
                                SavingsRecordService savingsRecordService,
                                SavingsMapper savingsMapper,
                                AuthenticationHelper authenticationHelper,
                                SavingsManager savingsManager,
                                WalletService walletService,
                                CurrencyService currencyService) {
        super(authenticationHelper);
        this.savingsFeatureService = savingsFeatureService;
        this.savingsRecordService = savingsRecordService;
        this.savingsMapper = savingsMapper;
        this.authenticationHelper = authenticationHelper;
        this.savingsManager = savingsManager;
        this.walletService = walletService;
        this.currencyService = currencyService;
    }

    //todo fix html integration
    @ModelAttribute
    public void addAttributes(Model model, HttpSession session) {
        model.addAttribute("savingsFeatures", savingsFeatureService.getAll());
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            List<SavingsRecord> records = new ArrayList<>();
            try {
                records = savingsRecordService.getAllByField("wallet.owner.id", user.getUserId());
                model.addAttribute("savingsRecords", records);
            } catch (EntityNotFoundException ignored) {
                model.addAttribute("savingsRecords", records);
            }
        } catch (AuthenticationFailureException ignored) {
        }
    }

    @GetMapping()
    public String getAllCurrentSavingsWallets(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);

            List<SavingsRecord> records = new ArrayList<>();
            try {
                records = savingsRecordService.getAllByField("wallet.owner.id", user.getUserId());
            } catch (EntityNotFoundException ignored) {
            }

            model.addAttribute("userWallets", walletService.getAllByField("owner_id", user.getUserId()));
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        return "savings-features";
    }

    @GetMapping("/{id}")
    public String getCurrentSavingsWallet(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            //todo admin only
            user = authenticationHelper.tryGetUser(session);
            List<SavingsRecord> savingsRecord = new ArrayList<>();
            savingsRecord.add(savingsRecordService.getById(id));
            SavingsFeature savingsFeature = savingsRecord.get(0).getSavingsFeature();

            model.addAttribute("savingsRecords", savingsRecord);
            model.addAttribute("savings", savingsFeature);
            model.addAttribute("userWallets",
                    walletService.getAllByField("owner_id", user.getUserId()));
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        return "savings-features";
    }

    @GetMapping("/{id}/update")
    public String updateCurrentSavingsWallet(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            //todo admin only
            user = authenticationHelper.tryGetUser(session);
            List<SavingsRecord> savingsRecord = new ArrayList<>();
            savingsRecord.add(savingsRecordService.getById(id));
            SavingsFeature savingsFeature = savingsRecord.get(0).getSavingsFeature();

            model.addAttribute("savingsRecords", savingsRecord);
            model.addAttribute("savings", savingsFeature);
            model.addAttribute("userWallets",
                    walletService.getAllByField("owner_id", user.getUserId()));
            model.addAttribute("newWalletId", new PositiveValueDto());
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        return "savings-update";
    }

    @PostMapping("/{id}/update")
    public String setUpdateCurrentSavingsWallet(@PathVariable int id,
                                                @Valid @ModelAttribute("newWalletId") PositiveValueDto positiveValueDto,
                                                BindingResult bindingResult,
                                                Model model,
                                                HttpSession session) {
        User user;
        try {
            //todo admin only
            user = authenticationHelper.tryGetUser(session);
            SavingsRecord currentRecord = savingsRecordService.getById(id);
            savingsMapper.mapSavingsRecordNewWallet(currentRecord, (int) positiveValueDto.getValue());

            savingsRecordService.update(currentRecord);
            model.addAttribute("savingsRecords",
                    savingsRecordService.getAllByField("wallet.owner.id",user.getUserId()));
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        return "savings-features";
    }

    @PostMapping("/{id}")
    public String cancelCurrentSavingsWallet(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            //todo admin only
            user = authenticationHelper.tryGetUser(session);
            model.addAttribute("savingsRecords", savingsRecordService.getAll());
            model.addAttribute("newWalletId", new PositiveValueDto());
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        return "savings-features";
    }

    @GetMapping("/delete/{id}")
    public String getMyCurrentSavingsWallets(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            savingsManager.deleteSavingsDeposit(id, user);

            model.addAttribute("savingsRecords", savingsRecordService.getAll());
            model.addAttribute("userWallets", walletService.getAllByField("owner_id", user.getUserId()));
            model.addAttribute("savings", savingsFeatureService.getAll());
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        return "savings-features";
    }

    @GetMapping("/new")
    public String choseSavingsPlan(Model model, HttpSession session) {

        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            model.addAttribute("userWallets", walletService.getAllByField("owner_id", user.getUserId()));
            model.addAttribute("savingsRecordsDto", new SavingsSetUpDto());
            model.addAttribute("currencies", currencyService.getAll());
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        return "savings-new";
    }

    @PostMapping("/new")
    public String startSavingsPlan(@Valid @ModelAttribute("savingsRecordsDto") SavingsSetUpDto savingsRecordsDto,
                                   Model model,
                                   HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            SavingsRecord savingsRecord = savingsMapper.fromDto(savingsRecordsDto);
            savingsManager.setSavingsDeposit(savingsRecord, user);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (PaymentIssueException e) {
            model.addAttribute("error", e.getMessage());
            return "transaction-error";
        }

        return "redirect:/savings";
    }


    @GetMapping("/plans")
    public String viewAllSavings(Model model, HttpSession session) {

        model.addAttribute("savings", savingsFeatureService.getAll());
        return "savings-features";
    }

    @GetMapping("/records")
    public String viewAllUserSavings(Model model, HttpSession session) {
        return "savings-records";
    }

    @GetMapping("/plans/new")
    public String setUpNewSavingsFeature(Model model, HttpSession session) {

        model.addAttribute("savingsFeatureDto", new SavingsFeatureDto());
        return "savings-feature-form";
    }

    @PostMapping("/plans/new")
    public String createNewSavingsFeature(@Valid @ModelAttribute("savingsFeatureDto") SavingsFeatureDto savingsFeatureDto,
                                          BindingResult bindingResult,
                                          Model model,
                                          HttpSession session) {

        if (bindingResult.hasErrors()) {
            return "savings-feature-form";
        }

        User user;
        try {
            //todo admin only
            user = authenticationHelper.tryGetUser(session);

            SavingsFeature savingsFeature = savingsMapper.fromDto(savingsFeatureDto);
            savingsFeatureService.create(savingsFeature, user);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("savingsFeatureName", e.getMessage());
            return "savings-feature-form";
        }

        return "redirect:/savings/plans";
    }

    @GetMapping("/plans/{id}/update")
    public String updateSavingsFeatureView(@PathVariable int id,
                                           Model model,
                                           HttpSession session) {
        User user;

        try {
            //todo admin only
            user = authenticationHelper.tryGetUser(session);

            SavingsFeatureDto savingsFeatureDto = savingsMapper.toDto(savingsFeatureService.getById(id));
            model.addAttribute("savingsFeatureDto", savingsFeatureDto);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            return "savings-feature-form";
        }
        return "savings-feature-form";
    }

    @PostMapping("/plans/{id}/update")
    public String updateSavingsFeature(@PathVariable int id,
                                       @Valid @ModelAttribute("savingsFeatureDto") SavingsFeatureDto savingsFeatureDto,
                                       BindingResult bindingResult,
                                       Model model,
                                       HttpSession session) {

        if (bindingResult.hasErrors()) {
            //todo solve for checks
            return "savings-feature-form";
        }

        User user;
        try {
            //todo admin only
            user = authenticationHelper.tryGetUser(session);

            SavingsFeature savingsFeature = savingsMapper.fromDto(savingsFeatureDto, id);
            savingsFeatureService.update(savingsFeature, user);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("name", "savingsFeature.exists", e.getMessage());
            return "savings-feature-form";
        } catch (EntityNotFoundException e) {
            bindingResult.rejectValue("name", "savingsFeature.doesNotExist", e.getMessage());
            return "savings-feature-form";
        }

        return "redirect:/savings";
    }

}
