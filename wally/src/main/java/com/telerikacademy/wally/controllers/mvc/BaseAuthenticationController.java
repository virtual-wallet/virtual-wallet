package com.telerikacademy.wally.controllers.mvc;

import com.telerikacademy.wally.controllers.AuthenticationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import javax.servlet.http.HttpSession;

@Controller
public abstract class BaseAuthenticationController {

    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public BaseAuthenticationController(AuthenticationHelper authenticationHelper) {
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isAuthorized")
    public boolean populateIsAuthorized(HttpSession session) {
        return populateIsAuthenticated(session) && authenticationHelper.verifyAuthorization(session);
    }

}
