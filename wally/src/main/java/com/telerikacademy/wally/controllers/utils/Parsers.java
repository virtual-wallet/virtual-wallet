package com.telerikacademy.wally.controllers.utils;

public class Parsers {

    public static String parseCvvPresent(int cvv) {
        String parsedWalletId;
        if (cvv >= 100) {
            parsedWalletId = String.valueOf(cvv);
        } else {
            if (cvv >= 10) {
                parsedWalletId = "0" + (cvv);
            } else {
                parsedWalletId = "00" + (cvv);
            }
        }

        return parsedWalletId;
    }
}