package com.telerikacademy.wally.controllers.rest;

import com.telerikacademy.wally.controllers.AuthenticationHelper;
import com.telerikacademy.wally.exceptions.DuplicateEntityException;
import com.telerikacademy.wally.exceptions.EntityNotFoundException;
import com.telerikacademy.wally.exceptions.UnauthorizedOperationException;
import com.telerikacademy.wally.models.User;
import com.telerikacademy.wally.models.dtos.UserDto;
import com.telerikacademy.wally.models.dtos.UserFilterSearchSortDto;
import com.telerikacademy.wally.models.params.UserSearchParams;
import com.telerikacademy.wally.services.contracts.UserService;
import com.telerikacademy.wally.utils.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService service;
    private final UserMapper userMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UserController(UserService service, UserMapper userMapper, AuthenticationHelper authenticationHelper) {
        this.service = service;
        this.userMapper = userMapper;
        this.authenticationHelper = authenticationHelper;
    }


    @GetMapping
    public List<User> getAll(@RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        return service.getAll(user);

    }

    @GetMapping("/{id}")
    public User getById(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        return service.getById(id, user);
    }

    @PostMapping
    public User create(@Valid @RequestBody UserDto userDto) {
        User user = userMapper.fromDto(userDto);
        service.create(user);
        return user;
    }

    @PutMapping("/{id}")
    public User update(@PathVariable int id, @Valid @RequestBody UserDto userDto, @RequestHeader HttpHeaders headers) {
        User userToAuthenticate = authenticationHelper.tryGetUser(headers);
        User user = userMapper.fromDto(userDto, id);
        service.update(user, userToAuthenticate);
        return user;
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        User userToAuthenticate = authenticationHelper.tryGetUser(headers);
        service.delete(id, userToAuthenticate);
    }

    @PutMapping("/block/{id}")
    public User blockUser(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        User userToAuthenticate = authenticationHelper.tryGetUser(headers);
        User user = service.getById(id, userToAuthenticate);
        user.setBlocked(!user.isBlocked());
        service.update(user, userToAuthenticate);
        return user;
    }

    @GetMapping("/search")
    public List<User> search(@RequestParam(required = false) Optional<String> sort,
                             @RequestParam(required = false) Optional<String> username,
                             @RequestParam(required = false) Optional<String> phone,
                             @RequestParam(required = false) Optional<String> email,
                             @RequestParam("all") Optional<String> all,
                             @RequestHeader HttpHeaders headers) {

        User userToAuthenticate = authenticationHelper.tryGetUser(headers);
        UserFilterSearchSortDto userFilterSearchSortDto = userMapper.toDto(sort, username, phone, email, all);
        UserSearchParams userSearchParams = userMapper.fromDto(userFilterSearchSortDto);
        return service.searchAndSort(userSearchParams, userToAuthenticate);
    }

}

