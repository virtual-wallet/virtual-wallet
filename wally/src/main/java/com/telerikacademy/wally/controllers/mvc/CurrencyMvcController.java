package com.telerikacademy.wally.controllers.mvc;

import com.telerikacademy.wally.controllers.AuthenticationHelper;
import com.telerikacademy.wally.exceptions.AuthenticationFailureException;
import com.telerikacademy.wally.exceptions.DuplicateEntityException;
import com.telerikacademy.wally.exceptions.EntityNotFoundException;
import com.telerikacademy.wally.models.Currency;
import com.telerikacademy.wally.models.User;
import com.telerikacademy.wally.models.dtos.CurrencyDto;
import com.telerikacademy.wally.services.contracts.CurrencyService;
import com.telerikacademy.wally.services.contracts.UserService;
import com.telerikacademy.wally.utils.mappers.CurrencyMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/currencies")
public class CurrencyMvcController extends BaseAuthenticationController{

    private final CurrencyService currencyService;
    private final CurrencyMapper currencyMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public CurrencyMvcController(CurrencyService currencyService,
                                 CurrencyMapper currencyMapper,
                                 AuthenticationHelper authenticationHelper) {
        super(authenticationHelper);
        this.currencyService = currencyService;
        this.currencyMapper = currencyMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("currencies")
    public List<Currency> populateCurrencies() {
        return currencyService.getAll();
    }

    @GetMapping
    public String viewAllCurrencies(Model model, HttpSession session){
        return "currencies";
    }

    @GetMapping("/new")
    public String setUpNewCurrency(Model model, HttpSession session){

        model.addAttribute("currencyDto", new CurrencyDto());
        return "currency-form";
    }

    @PostMapping("/new")
    public String createNewCurrency(@Valid @ModelAttribute("currencyDto")CurrencyDto currencyDto,
                                    BindingResult bindingResult,
                                    Model model,
                                    HttpSession session){

        if (bindingResult.hasErrors()) {
            return "currency-form";
        }

        User user;
        try {
            //todo admin only
            user = authenticationHelper.tryGetUser(session);

            Currency currency = currencyMapper.fromDto(currencyDto);
            currencyService.create(currency, user);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }catch (DuplicateEntityException e){
            bindingResult.rejectValue("currencyName", e.getMessage());
            return "currency-form";
        }

        return "redirect:/currencies";
    }

    @GetMapping("/{id}/update")
    public String updateCurrencyView(@PathVariable int id,
                                     Model model,
                                     HttpSession session){
        User user;

        try {
            //todo admin only
            user = authenticationHelper.tryGetUser(session);

            CurrencyDto currencyDto = currencyMapper.toDto(currencyService.getById(id));
            model.addAttribute("currencyDto", currencyDto);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }catch (EntityNotFoundException e){
            return "currency-form";
        }
        return "currency-form";
    }

    @PostMapping("/{id}/update")
    public String updateCurrency(@PathVariable int id,
                                 @Valid @ModelAttribute("currencyDto")CurrencyDto currencyDto,
                                 BindingResult bindingResult,
                                 Model model,
                                 HttpSession session){

        if (bindingResult.hasErrors()) {
            return "currency-form";
        }

        User user;
        try {
            //todo admin only
            user = authenticationHelper.tryGetUser(session);

            Currency currency = currencyMapper.fromDto(currencyDto,id);
            currencyService.update(currency, user);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }catch (DuplicateEntityException e){
            bindingResult.rejectValue("currencyName", "currency.exists", e.getMessage());
            return "currency-form";
        }catch (EntityNotFoundException e){
            bindingResult.rejectValue("currencyName", "currency.doesNotExist", e.getMessage());
            return "currency-form";
        }

        return "redirect:/currencies";
    }
}
