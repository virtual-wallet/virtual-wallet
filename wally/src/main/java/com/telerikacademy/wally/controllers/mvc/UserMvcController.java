package com.telerikacademy.wally.controllers.mvc;

import com.telerikacademy.wally.controllers.AuthenticationHelper;
import com.telerikacademy.wally.exceptions.AuthenticationFailureException;
import com.telerikacademy.wally.exceptions.DuplicateEntityException;
import com.telerikacademy.wally.exceptions.EntityNotFoundException;
import com.telerikacademy.wally.exceptions.UnauthorizedOperationException;
import com.telerikacademy.wally.models.Photo;
import com.telerikacademy.wally.models.User;
import com.telerikacademy.wally.models.dtos.PasswordDto;
import com.telerikacademy.wally.models.dtos.UpdateUserDto;
import com.telerikacademy.wally.models.dtos.UserFilterSearchSortDto;
import com.telerikacademy.wally.models.enums.UserSortOptions;
import com.telerikacademy.wally.models.params.UserSearchParams;
import com.telerikacademy.wally.services.contracts.PhotoService;
import com.telerikacademy.wally.services.contracts.UserService;
import com.telerikacademy.wally.utils.FileUploadUtil;
import com.telerikacademy.wally.utils.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import static com.telerikacademy.wally.controllers.utils.PaginationHelper.setPageNavigation;

@Controller
@RequestMapping("/users")
public class UserMvcController extends BaseAuthenticationController {
    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final UserMapper userMapper;
    private final PhotoService photoService;

    @Autowired
    public UserMvcController(UserService userService,
                             AuthenticationHelper authenticationHelper,
                             UserMapper userMapper, PhotoService photoService) {
        super(authenticationHelper);
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.userMapper = userMapper;
        this.photoService = photoService;
    }

    @ModelAttribute("userSortOptions")
    public UserSortOptions[] populateUserSortOptions() {
        return UserSortOptions.values();
    }

    @ModelAttribute("users")
    public List<User> populateUsers() {
        return userService.getAll();
    }


    @GetMapping
    public String showAllUsers(@RequestParam("sort") Optional<String> sort,
                               @RequestParam("username") Optional<String> username,
                               @RequestParam("phone") Optional<String> phone,
                               @RequestParam("email") Optional<String> email,
                               @RequestParam("all") Optional<String> all,
                               @RequestParam("page") Optional<Integer> page,
                               @RequestParam("size") Optional<Integer> size,
                               Model model,
                               HttpSession session) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(20);

        UserFilterSearchSortDto userFilterSearchSortDto = userMapper.toDto(sort, username, phone, email, all);
        UserSearchParams userSearchParams = userMapper.fromDto(userFilterSearchSortDto);

        try {
            User user = authenticationHelper.tryGetUser(session);

            var filtered = userService.findPaginated(
                    PageRequest.of(currentPage - 1, pageSize), userSearchParams,user);

            setPageNavigation(model, filtered.getTotalPages(), currentPage);

            model.addAttribute("userPage", filtered);
            model.addAttribute("userFilterSearchSortDto", userFilterSearchSortDto);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        return "users";
    }

    @GetMapping("/{id}/update")
    public String showEditUserPage(@PathVariable int id, Model model, HttpSession session) {
        User loggedUser;
        try {
            loggedUser=authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (authenticationHelper.tryGetUser(session).getUserId() == id) {
            try {
                User user = userService.getById(id);
                UpdateUserDto userDto = userMapper.updatedUserToDto(user);
                model.addAttribute("user", userDto);
                model.addAttribute("username", user.getUsername());
                return "user-update";
            } catch (EntityNotFoundException e) {
                model.addAttribute("error", e.getMessage());
                return "not-found";
            }
        } else return "access-denied";

    }

    @PostMapping("/{id}/update")
    public String updateUser(@PathVariable int id,
                             @Valid @ModelAttribute("user") UpdateUserDto userDto,
                             BindingResult errors,
                             Model model,
                             HttpSession session,
                             @RequestParam("image") MultipartFile multipartFile) throws IOException {

User loggedUser;
        try {
           loggedUser= authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "user-update";
        }
        try {
            User user = userMapper.fromDto(userDto, id);
            String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
            userService.update(user,loggedUser);

            if (!fileName.equals("")) {
                String uploadDir = "user-photos/" + user.getUserId();
                FileUploadUtil.saveFile(uploadDir, fileName, multipartFile);

                try {
                    Photo photo = photoService.getByField("user_id", user.getUserId());
                    photo.setPhoto(fileName);
                    photoService.update(photo);
                } catch (EntityNotFoundException e) {
                    Photo photo = new Photo();
                    photo.setUser(user);
                    photo.setPhoto(fileName);
                    photoService.create(photo);
                }
            }
            return "redirect:/home";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("email", "email.exists", e.getMessage());
            return "user-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }

    }

    @GetMapping("/{id}/update/pass")
    public String showChangePasswordPage(@PathVariable int id, Model model, HttpSession session) {
        User loggedUser;
        try {
            loggedUser=authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (authenticationHelper.tryGetUser(session).getUserId() == id) {
            try {

                User user = userService.getById(id);
                PasswordDto passwordDto = userMapper.passwordToDto(user);
                model.addAttribute("pass", passwordDto);
                return "password-update";
            } catch (EntityNotFoundException e) {
                model.addAttribute("error", e.getMessage());
                return "not-found";
            }
        } else return "access-denied";

    }

    @PostMapping("/{id}/update/pass")
    public String updateUser(@PathVariable int id,
                             @Valid @ModelAttribute("pass") PasswordDto passwordDto,
                             BindingResult errors,
                             Model model,
                             HttpSession session){
User loggedUser;
        try {
            loggedUser=authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "password-update";
        }

        if (!passwordDto.getPassword().equals(passwordDto.getPasswordConfirm())) {
            errors.rejectValue("passwordConfirm", "password_error", "Password confirmation should match password.");
            return "password-update";
        }

        try {
            User user = userMapper.fromDto(passwordDto, id);
            userService.update(user,loggedUser);

            return "home";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }

    }

    @PostMapping()
    public String searchInUsers(@ModelAttribute UserFilterSearchSortDto userFilterSearchSortDto,
                                @RequestParam("page") Optional<Integer> page,
                                @RequestParam("size") Optional<Integer> size,
                                Model model,
                                HttpSession session) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(20);

        try {
            var filtered = userService.findPaginated(
                    PageRequest.of(currentPage - 1, pageSize),
                    userMapper.fromDto(userFilterSearchSortDto),authenticationHelper.tryGetUser(session));

            setPageNavigation(model, filtered.getTotalPages(), currentPage);

            model.addAttribute("userPage", filtered);
            model.addAttribute("userFilterSearchSortDto", userFilterSearchSortDto);
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
        return "users";
    }
    @GetMapping("/block/{id}")
    public String activateOverdraft(@PathVariable int id, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        User userToBlock = userService.getById(id);
        userToBlock.setBlocked(!user.isBlocked());
        userService.update(userToBlock,user);
        return "redirect:/users";
    }
}
