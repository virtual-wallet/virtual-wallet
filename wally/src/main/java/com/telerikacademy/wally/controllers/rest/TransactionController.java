package com.telerikacademy.wally.controllers.rest;

import com.telerikacademy.wally.controllers.AuthenticationHelper;
import com.telerikacademy.wally.exceptions.DuplicateEntityException;
import com.telerikacademy.wally.exceptions.EntityNotFoundException;
import com.telerikacademy.wally.models.Transaction;
import com.telerikacademy.wally.models.User;
import com.telerikacademy.wally.models.dtos.TransactionDto;
import com.telerikacademy.wally.models.dtos.TransactionFilterSortDto;
import com.telerikacademy.wally.models.params.TransactionsFilterParams;
import com.telerikacademy.wally.services.contracts.*;
import com.telerikacademy.wally.utils.mappers.TransactionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/transactions")
public class TransactionController {
    private final TransactionService transactionService;
    private final TransactionMapper transactionMapper;
    private final AuthenticationHelper authenticationHelper;


    @Autowired
    public TransactionController(TransactionService transactionService, TransactionMapper transactionMapper, AuthenticationHelper authenticationHelper) {
        this.transactionService = transactionService;
        this.transactionMapper = transactionMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Transaction> getAll(@RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        return transactionService.getAll(user);
    }

    @GetMapping("/user/{id}")
    public List<Transaction> getAllUserTransactions(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        return transactionService.getAllByField("from_user_id", id, user);
    }

    @GetMapping("/{id}")
    public Transaction getById(@PathVariable int id, @RequestHeader HttpHeaders headers) {
            User user = authenticationHelper.tryGetUser(headers);
            return transactionService.getById(id, user);
    }

    @PostMapping
    public Transaction create(@Valid @RequestBody TransactionDto transactionDto, @RequestHeader HttpHeaders headers) {
        authenticationHelper.tryGetUser(headers);
        Transaction transaction = transactionMapper.fromDto(transactionDto);
        transactionService.create(transaction);
        return transaction;
    }

    @GetMapping("/filter")
    public List<Transaction> search(@RequestParam("sender-username") Optional<String> senderUsername,
                                    @RequestParam("recipient-username") Optional<String> recipientUsername,
                                    @RequestParam("date-from") Optional<String> dateFrom,
                                    @RequestParam("date-to") Optional<String> dateTo,
                                    @RequestParam("sort") Optional<String> sort,
                                    @RequestParam("from-wallet") Optional<String> fromWalletId,
                                    @RequestParam("to-wallet") Optional<String> toWalletId,
                                    @RequestParam("is-credited") Optional<String> isCredited,
                                    @RequestParam("category") Optional<String> category,
                                    @RequestParam("status") Optional<String> status) {

        TransactionFilterSortDto transactionFilterSortDto = transactionMapper.toDto(senderUsername,
                recipientUsername,
                dateFrom,
                dateTo,
                sort,
                fromWalletId,
                toWalletId,
                isCredited,
                category,
                status);

        TransactionsFilterParams filterParams = transactionMapper.fromDto(transactionFilterSortDto);
        return transactionService.searchAndSort(filterParams);
    }

}
