package com.telerikacademy.wally.controllers.mvc;

import com.telerikacademy.wally.controllers.AuthenticationHelper;
import com.telerikacademy.wally.exceptions.AuthenticationFailureException;
import com.telerikacademy.wally.exceptions.DuplicateEntityException;
import com.telerikacademy.wally.exceptions.EntityNotFoundException;
import com.telerikacademy.wally.models.User;
import com.telerikacademy.wally.models.dtos.LoginDto;
import com.telerikacademy.wally.models.dtos.RegisterDto;
import com.telerikacademy.wally.services.contracts.PhotoService;
import com.telerikacademy.wally.services.contracts.UserService;
import com.telerikacademy.wally.utils.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;


@Controller
@RequestMapping("/auth")
public class AuthenticationController extends BaseAuthenticationController {

    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final UserMapper userMapper;
    private final PhotoService photoService;

    @Autowired
    public AuthenticationController(UserService userService,
                                    AuthenticationHelper authenticationHelper,
                                    UserMapper userMapper, PhotoService photoService) {
        super(authenticationHelper);
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.userMapper = userMapper;
        this.photoService = photoService;
    }


    @GetMapping("/login")
    public String showLoginPage(Model model) {
        model.addAttribute("login", new LoginDto());
        return "login";
    }

    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("login") LoginDto login,
                              BindingResult bindingResult,
                              HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "login";
        }

        try {
            authenticationHelper.verifyAuthentication(login.getUsername(), login.getPassword());
            session.setAttribute("currentUser", login.getUsername());
            session.setAttribute("currentUserId", authenticationHelper.tryGetUser(session).getUserId());
            try {
                String photo = photoService.getByField("user_id", authenticationHelper.tryGetUser(session).getUserId()).getPhoto();
                session.setAttribute("photo", photo);
            } catch (EntityNotFoundException e) {
                return "redirect:/home";
            }
            return "redirect:/home";

        } catch (AuthenticationFailureException e) {
            bindingResult.rejectValue("username", "auth_error", e.getMessage());
            return "login";
        }
    }

    @GetMapping("/logout")
    public String handleLogout(HttpSession session) {
        session.removeAttribute("currentUser");
        session.removeAttribute("currentUserId");
        session.removeAttribute("photo");
        return "redirect:/home";
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("register", new RegisterDto());
        return "register";
    }

    @PostMapping("/register")
    public String handleRegister(@Valid @ModelAttribute("register") RegisterDto register,
                                 BindingResult bindingResult1, HttpServletRequest request) throws MessagingException, UnsupportedEncodingException {

        if (bindingResult1.hasErrors()) {
            return "register";
        }

        if (!register.getPassword().equals(register.getPasswordConfirm())) {
            bindingResult1.rejectValue("passwordConfirm",
                    "password_error",
                    "Password confirmation should match password.");
            return "register";
        }

        try {
            User user = userMapper.fromDto(register);
            userService.create(user);
            String siteURL = request.getRequestURL().toString().replace(request.getServletPath(), "");
            userService.sendVerificationEmail(user, siteURL);

            return "register-success";
        } catch (DuplicateEntityException e) {
            if (e.getMessage().contains("phone")) {
                bindingResult1.rejectValue("phone", "phone.exist", e.getMessage());
            }
            if (e.getMessage().contains("username")) {
                bindingResult1.rejectValue("username", "username.exist", e.getMessage());
            }
            if (e.getMessage().contains("email")) {
                bindingResult1.rejectValue("email", "email.exist", e.getMessage());
            }
            return "register";
        }
    }

    @GetMapping("/verify")
    public String verifyAccount(@Param("code") String code) {
        if (userService.verify(code)) {
            return "verification-success";
        } else {
            return "verification-fail";
        }
    }
}


