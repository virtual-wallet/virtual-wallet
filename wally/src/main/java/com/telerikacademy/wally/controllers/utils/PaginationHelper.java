package com.telerikacademy.wally.controllers.utils;

import org.springframework.ui.Model;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class PaginationHelper {

    public static void setPageNavigation(Model model, int totalPages, int currentPage) {
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());

            if (pageNumbers.size() > 7) {
                if (currentPage > 3) {
                    int pagesTillEnd = pageNumbers.size() - currentPage;
                    int pagesTillEndFromCurrent = Math.min(3, pagesTillEnd);
                    pageNumbers = pageNumbers.subList(
                            currentPage - (7 - pagesTillEndFromCurrent), currentPage + pagesTillEndFromCurrent);
                } else pageNumbers = pageNumbers.subList(0, 7);
            }

            model.addAttribute("pageNumbers", pageNumbers);
        }
    }
}
