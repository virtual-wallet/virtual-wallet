package com.telerikacademy.wally.controllers.utils;

public class Constants {

    public static final int DEFAULT_PAGE_SIZE = 20;
    public static final int DEFAULT_MAIN_PAGE = 1;
    public static final int TRANSACTIONS_PAST_MONTHS_NUM = 1;
    public static final int STATUS_PENDING = 3;
    public static final int STATUS_COMPLETED = 2;
    public static final int STATUS_DECLINED = 1;
    public static final int CATEGORY_SAVINGS = 15;
    public static final int STATUS_TERMINATED = 4;
}
