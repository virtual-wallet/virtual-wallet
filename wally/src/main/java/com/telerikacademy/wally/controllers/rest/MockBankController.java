package com.telerikacademy.wally.controllers.rest;

import com.telerikacademy.wally.models.dtos.MockBankDto;
import com.telerikacademy.wally.models.enums.TransactionStatus;
import org.apache.tomcat.util.http.parser.Authorization;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@Controller
@RequestMapping("/api/mock-bank")
public class MockBankController {

    private static int currentResponse = 0;

    //todo add valid
    @PostMapping
    public ResponseEntity<?> getTransferPermission(@RequestBody MockBankDto mockBankDto,
                                                   @RequestHeader String authorization) {

        if (!authorization.contains("Bearer")) return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

        int i = currentResponse == 5 ? currentResponse = 0 : currentResponse++;

        switch (currentResponse) {
            case 0:
            case 1:
            case 2:
                return new ResponseEntity<>(TransactionStatus.APPROVED.getPreview(), HttpStatus.OK);
            case 3:
                throw new ResponseStatusException(HttpStatus.REQUEST_TIMEOUT);
            case 4:
                return new ResponseEntity<>(TransactionStatus.DECLINED.getPreview(), HttpStatus.OK);
            default:
                throw new ResponseStatusException(HttpStatus.ACCEPTED);
        }
    }

    @PostMapping("/check-status")
    public ResponseEntity<?> checkPreviousTransactionStatus(@RequestBody MockBankDto mockBankDto,
                                                            @RequestHeader String authorization) {
        return new ResponseEntity<>(TransactionStatus.APPROVED.getPreview(), HttpStatus.OK);
    }
}


