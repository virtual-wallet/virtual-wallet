package com.telerikacademy.wally.exceptions;

import java.io.IOException;

public class PaymentIssueException extends RuntimeException {

    public PaymentIssueException(String message) {
        super(message);
    }

}
