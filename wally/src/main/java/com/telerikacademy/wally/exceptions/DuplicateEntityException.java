package com.telerikacademy.wally.exceptions;

public class DuplicateEntityException extends RuntimeException {

    public <T> DuplicateEntityException(String object, String attribute, T value) {
        super(String.format("%s with %s %s already exists.", object, attribute, value));
    }

    public <T> DuplicateEntityException(String object) {
        super(String.format("%s already exists.", object));
    }

}
