package com.telerikacademy.wally.exceptions;

public class InvalidDataInput extends RuntimeException{

    public InvalidDataInput(String object) {
        super(String.format("Incorrect value for %s", object));
    }

    public InvalidDataInput(String object, String comparison, String anotherObject) {
        super(String.format("%s cannot be %s %s", object, comparison, anotherObject));
    }
}
