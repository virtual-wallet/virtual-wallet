package com.telerikacademy.wally.exceptions.config;

import com.telerikacademy.wally.exceptions.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.text.ParseException;

@ControllerAdvice
public class ApiExceptionHandler {

    @ExceptionHandler(value = {EntityNotFoundException.class})
    public ResponseEntity<Object> handleEntityNotFoundException(EntityNotFoundException e) {
        HttpStatus notFound = HttpStatus.NOT_FOUND;
        ApiException apiException = new ApiException(e.getMessage(), notFound);
        return new ResponseEntity<>(apiException, notFound);
    }

    @ExceptionHandler(value = {DuplicateEntityException.class})
    public ResponseEntity<Object> handleDuplicateEntityException(DuplicateEntityException e) {
        HttpStatus duplicate = HttpStatus.CONFLICT;
        ApiException apiException = new ApiException(e.getMessage(), duplicate);
        return new ResponseEntity<>(apiException, duplicate);
    }

    @ExceptionHandler(value = {AuthenticationFailureException.class})
    public ResponseEntity<Object> handleAuthenticationFailureException(AuthenticationFailureException e) {
        HttpStatus invalid = HttpStatus.BAD_REQUEST;
        ApiException apiException = new ApiException(e.getMessage(), invalid);
        return new ResponseEntity<>(apiException, invalid);
    }

    @ExceptionHandler(value = {InvalidDataInput.class})
    public ResponseEntity<Object> handleInvalidDataInput(InvalidDataInput e) {
        HttpStatus invalid = HttpStatus.BAD_REQUEST;
        ApiException apiException = new ApiException(e.getMessage(), invalid);
        return new ResponseEntity<>(apiException, invalid);
    }

    @ExceptionHandler(value = {PaymentIssueException.class})
    public ResponseEntity<Object> handlePaymentIssueException(PaymentIssueException e) {
        HttpStatus invalid = HttpStatus.BAD_REQUEST;
        ApiException apiException = new ApiException(e.getMessage(), invalid);
        return new ResponseEntity<>(apiException, invalid);
    }

    @ExceptionHandler(value = {UnauthorizedOperationException.class})
    public ResponseEntity<Object> handleUnauthorizedOperationException(UnauthorizedOperationException e) {
        HttpStatus invalid = HttpStatus.UNAUTHORIZED;
        ApiException apiException = new ApiException(e.getMessage(), invalid);
        return new ResponseEntity<>(apiException, invalid);
    }

    @ExceptionHandler(value = {ParseException.class})
    public ResponseEntity<Object> handleParseException(ParseException e) {
        HttpStatus invalid = HttpStatus.BAD_REQUEST;
        ApiException apiException = new ApiException(e.getMessage(), invalid);
        return new ResponseEntity<>(apiException, invalid);
    }
}
