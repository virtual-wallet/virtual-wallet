package com.telerikacademy.wally.exceptions;

public class EntityNotFoundException extends RuntimeException{

    public EntityNotFoundException(String type, int id) {
        this(type, "id", String.valueOf(id));
    }

    public EntityNotFoundException(String type, int from, int to) {
        super(String.format("%s in the range between %s and %s not found", type, from, to));
    }

    public EntityNotFoundException(String object) {
        super(String.format("No items of type '%s' found", object));

    }

    public <T> EntityNotFoundException(String type, String fieldName,T id) {
        super(String.format("%s with %s %s not found",type, fieldName, String.valueOf(id)));
    }
}
