create table categories
(
    id   int auto_increment,
    name varchar(50) not null,
    constraint categories_id_uindex
        unique (id),
    constraint categories_name_uindex
        unique (name)
);

alter table categories
    add primary key (id);

create table currencies
(
    id            int auto_increment,
    name          varchar(50)   not null,
    iso           varchar(5)    not null,
    symbol        varchar(5)    not null,
    exchange_rate double(18, 5) not null,
    constraint currencies_id_uindex
        unique (id),
    constraint currencies_name_uindex
        unique (name)
);

alter table currencies
    add primary key (id);


create table overdraft_features
(
    id      int auto_increment
        primary key,
    name    varchar(50)   not null,
    max_mth int           not null,
    apr     double(18, 5) not null,
    constraint overdraft_features_id_uindex
        unique (id),
    constraint overdraft_features_name_uindex
        unique (name)
);

create table rewards
(
    id               int auto_increment,
    name             varchar(50)   not null,
    amount           int           not null,
    currency_id      int           not null,
    reward_end_date  date          not null,
    max_reward_times int default 1 not null,
    constraint rewards_id_uindex
        unique (id),
    constraint rewards_name_uindex
        unique (name),
    constraint rewards_currencies_id_fk
        foreign key (currency_id) references currencies (id) on delete cascade
);

alter table rewards
    add primary key (id);

create table roles
(
    id   int auto_increment,
    name varchar(50) not null,
    constraint roles_id_uindex
        unique (id)
);

alter table roles
    add primary key (id);

create table savings_features
(
    id           int auto_increment,
    name         varchar(50)   not null,
    duration_mth int           not null,
    aer          double(18, 5) not null,
    constraint savings_features_id_uindex
        unique (id),
    constraint savings_features_name_uindex
        unique (name)
);

alter table savings_features
    add primary key (id);

create table statuses
(
    id   int auto_increment,
    name varchar(50) not null,
    constraint statuses_id_uindex
        unique (id),
    constraint statuses_name_uindex
        unique (name)
);

alter table statuses
    add primary key (id);

create table users
(
    id                int auto_increment,
    username          varchar(20)          not null,
    password          varchar(50)          not null,
    email             varchar(50)          not null,
    phone             varchar(20)          not null,
    enabled           tinyint(1)           not null,
    blocked           tinyint(1) default 0 not null,
    verification_code varchar(64)          not null,
    constraint User_email_uindex
        unique (email),
    constraint User_id_uindex
        unique (id),
    constraint User_phone_uindex
        unique (phone),
    constraint User_username_uindex
        unique (username)
);

alter table users
    add primary key (id);

create table approvals_queue
(
    id          int auto_increment,
    user_id     int         not null,
    user_photo  varchar(65) not null,
    user_doc_id varchar(65) not null,
    constraint approvals_queue_id_uindex
        unique (id),
    constraint approvals_queue_users_id_fk
        foreign key (user_id) references users (id) on delete cascade
);

alter table approvals_queue
    add primary key (id);

create table card_types
(
    id   int auto_increment,
    name varchar(50) not null,
    constraint card_types_id_uindex
        unique (id)
);

alter table card_types
    add primary key (id);

create table cards
(
    id           int auto_increment,
    card_number  varchar(16)   not null,
    cardholder   varchar(50)   not null,
    currency_id  int default 1 not null,
    cvv          int           not null,
    user_id      int           not null,
    card_type_id int           not null,
    exp_date     date          not null,
    constraint cards_card_number_uindex
        unique (card_number),
    constraint cards_id_uindex
        unique (id),
    constraint cards_card_types_id_fk
        foreign key (card_type_id) references card_types (id)
            on delete cascade,
    constraint cards_currencies_id_fk
        foreign key (currency_id) references currencies (id)
            on delete cascade,
    constraint cards_users_id_fk
        foreign key (user_id) references users (id)
            on delete cascade
);

alter table cards
    add primary key (id);


create table contacts
(
    contact_owner_id int not null,
    contact_id       int not null,
    constraint contacts_users_id_fk
        foreign key (contact_owner_id) references users (id) on delete cascade,
    constraint contacts_users_id_fk_2
        foreign key (contact_id) references users (id) on delete cascade
);

create table referrals
(
    id            int auto_increment,
    user_id       int           not null,
    referee_email varchar(50)   not null,
    is_accepted   int default 0 not null,
    reward_id     int           not null,
    constraint referrals_id_uindex
        unique (id),
    constraint referrals_referee_email_uindex
        unique (referee_email),
    constraint referrals_rewards_id_fk
        foreign key (reward_id) references rewards (id) on delete cascade,
    constraint referrals_users_id_fk
        foreign key (user_id) references users (id) on delete cascade
);

alter table referrals
    add primary key (id);

create table reward_user_tries
(
    user_id    int not null,
    reward_id  int not null,
    tries_left int not null,
    constraint reward_user_tries_rewards_id_fk
        foreign key (reward_id) references rewards (id) on delete cascade,
    constraint reward_user_tries_users_id_fk
        foreign key (user_id) references users (id) on delete cascade
);
create table users_categories
(
    user_id     int not null,
    category_id int not null,
    constraint user_categories_categories_id_fk
        foreign key (category_id) references categories (id),
    constraint user_categories_users_id_fk
        foreign key (user_id) references users (id)
);

create table users_roles
(
    user_id int not null,
    role_id int not null,
    constraint users_roles_roles_id_fk
        foreign key (role_id) references roles (id) on delete cascade,
    constraint users_roles_user_id_fk
        foreign key (user_id) references users (id) on delete cascade
);

create table wallets
(
    id            int auto_increment,
    owner_id      int                        not null,
    currency_id   int           default 1    not null,
    balance       double(18, 2) default 0.00 not null,
    has_overdraft tinyint(1)    default 0    not null,
    constraint wallets_id_uindex
        unique (id),
    constraint wallets_currencies_id_fk
        foreign key (currency_id) references currencies (id)
            on delete cascade,
    constraint wallets_users_id_fk
        foreign key (owner_id) references users (id)
            on delete cascade
);

alter table wallets
    add primary key (id);


create table overdraft_records
(
    id           int auto_increment,
    wallet_id    int                          not null,
    overdraft_id int                          not null,
    mth_unpaid   int                          not null,
    date_started date       default curdate() not null,
    is_paid      tinyint(1) default 0         not null,
    constraint overdraft_records_id_uindex
        unique (id),
    constraint overdraft_records_overdraft_features_id_fk
        foreign key (overdraft_id) references overdraft_features (id) on delete cascade,
    constraint overdraft_records_wallets_id_fk
        foreign key (wallet_id) references wallets (id) on delete cascade
);

alter table overdraft_records
    add primary key (id);


create table savings_records
(
    id              int auto_increment,
    wallet_id       int                    not null,
    savings_type_id int                    not null,
    currency_id     int                    not null,
    base_amount     double(18, 5)          not null,
    duration_mth    int                    not null,
    start_date      date default curdate() null,
    end_date        date                   not null,
    aer             double(18, 5)          not null,
    constraint savings_records_id_uindex
        unique (id),
    constraint savings_records_currencies_id_fk
        foreign key (currency_id) references currencies (id),
    constraint savings_records_savings_features_id_fk
        foreign key (savings_type_id) references savings_features (id)
            on delete cascade,
    constraint savings_records_wallets_id_fk
        foreign key (wallet_id) references wallets (id)
            on delete cascade
);

alter table savings_records
    add primary key (id);

create table transactions
(
    id               int auto_increment,
    from_user_id     int                                   not null,
    from_wallet_id   int                                   not null,
    to_wallet_id     int                                   not null,
    category_id      int                                   not null,
    status_id        int                                   not null,
    transfer_value   double(18, 5)                         not null,
    exchange_rate    double(18, 5)                         not null,
    transaction_time timestamp default current_timestamp() not null on update current_timestamp(),
    constraint transactions_id_uindex
        unique (id),
    constraint transactions_categories_category_id_fk
        foreign key (category_id) references categories (id)
            on delete cascade,
    constraint transactions_statuses_id_fk
        foreign key (status_id) references statuses (id)
            on delete cascade,
    constraint transactions_users_id_fk
        foreign key (from_user_id) references users (id)
            on delete cascade,
    constraint transactions_wallets_id_fk
        foreign key (from_wallet_id) references wallets (id)
            on delete cascade,
    constraint transactions_wallets_id_fk_2
        foreign key (to_wallet_id) references wallets (id)
            on delete cascade
);

alter table transactions
    add primary key (id);



create table recurring_transactions
(
    id               int auto_increment,
    transaction_id   int                    not null,
    frequency_wks    int  default 0         not null,
    frequency_mth    int  default 0         not null,
    date_started     date default curdate() not null,
    transaction_date int  default 1         not null,
    constraint recurring_transactions_id_uindex
        unique (id),
    constraint recurring_transactions_transactions_id_fk
        foreign key (transaction_id) references transactions (id) on delete cascade
);

alter table recurring_transactions
    add primary key (id);

create table user_wallet_access
(
    user_id    int                  not null,
    wallet_id  int                  not null,
    has_access tinyint(1) default 0 not null,
    constraint user_wallet_has_access_users_id_fk
        foreign key (user_id) references users (id) on delete cascade,
    constraint user_wallet_has_access_wallets_id_fk
        foreign key (wallet_id) references wallets (id) on delete cascade
);

create table wallets_cards
(
    wallet_id int not null,
    card_id   int not null
);

create table default_wallets
(
    user_id   int not null,
    wallet_id int not null,
    constraint default_wallets_user_id_uindex
        unique (user_id),
    constraint default_wallets_wallet_id_uindex
        unique (wallet_id),
    constraint default_wallets_users_id_fk
        foreign key (user_id) references users (id)
            on delete cascade,
    constraint default_wallets_wallets_id_fk
        foreign key (wallet_id) references wallets (id)
            on delete cascade
);


create table photos
(
    photo_id int auto_increment,
    photo    varchar(60) not null,
    user_id  int         not null,
    constraint photos_photo_id_uindex
        unique (photo_id),
    constraint photos_user_id_uindex
        unique (user_id),
    constraint photos_users_id_fk
        foreign key (user_id) references users (id)
            on delete cascade
);

alter table photos
    add primary key (photo_id);